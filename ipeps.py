import pyUni10 as uni10
import copy
import basic
import basic_H
import itebd
import Fullupdate
###################### Initialize parameters ###########################
Model="Heisenberg"
#Model="Heisenberg_Z2"
#Model="Heisenberg_U1"
#Model="Heisenberg_U1Z2"

D=[3]
chi=[15]
# #d_phys=[8]
d_phys=[8]
# d_phys=[3]

#D=[2,2]
#chi=[10,10]
#d_phys=[1,1]

#D=[1,2,1]
#chi=[1,1,5,5,5,1,1]
#d_phys=[1,1]
#d_phys=[1,1,1]
#d_phys=[1,3,3,1]

#D=[2,2,2,2,2,2]
#chi=[10,10,10,10,10,10]
#d_phys=[1,1]

method="SVD_QR"            #SVD, Grad, SVD_mpo, SVD_QR, SVD_mix,SVD_mg
Inv_method='SVD'         #SVD, CG
Grad_method="ST"        # CG,ST
Gauge='Fixed'
Corner_method='CTMFull'   #CTMRG, CTMFull
check_step='off'            #on, off
fixbond_itebd='off'            #on, off
itebd_method='short'            #long, short
start_itebd=1.400
division_itebd=4
iteration_per_step=1
#############################
grid_val=+0.0
Num_iter=1
J_kagome=1.0
J_Chiral=0.50
h_z=0.0
J_staggered=1.0
#############################

N_grad=450
N_env=[50,1.00e-7]
N_svd=[10,1.00e-9,20]
N_iteritebd=40
N_iterFull=200
Acc_E=1.00e-7
distance_val=20
Arnoldi_bond=100


###################################################################

q_D, q_chi, q_phys=basic.full_make_bond(Model, D, chi, d_phys)
#print q_phys
Gamma_a,Gamma_b,Gamma_c,Gamma_d,Landa_1,Landa_2,Landa_3,Landa_4,Landa_5, Landa_6, Landa_7,Landa_8=basic.produce_GammaLanda(q_D, q_phys)
#Gamma_a,Gamma_b,Gamma_c,Gamma_d,Landa_1,Landa_2,Landa_3,Landa_4,Landa_5, Landa_6, Landa_7,Landa_8=basic.produce_GammaLanda_manual(q_D, q_phys)

Gamma=[Gamma_a,Gamma_b,Gamma_c,Gamma_d]
Landa=[Landa_1,Landa_2,Landa_3,Landa_4,Landa_5,Landa_6,Landa_7,Landa_8]
basic.Initialize_function(Gamma,Landa)
a_u,b_u,c_u,d_u=basic.produce_abcd_gamma(Landa, Gamma_a,Gamma_b,Gamma_c,Gamma_d)

Env=basic.produce_env_init(q_chi,q_D)
Env1=basic.produce_env_init1(q_chi,q_D)
Env2=basic.Rand_env_total(Env)
Env3=basic.produce_env_init1(q_chi,q_D)

Env4=basic.produce_env_init2(q_chi,q_D)
Env5=basic.produce_env_init3(q_chi,q_D)
Env6=basic.produce_env_init2(q_chi,q_D)
Env7=basic.produce_env_init3(q_chi,q_D)

#Env5=basic.Rand_env_total(Env)
#Env7=basic.Rand_env_total(Env)


Env8=basic.produce_env_init(q_chi,q_D)
Env9=basic.produce_env_init1(q_chi,q_D)
Env10=basic.Rand_env_total(Env)
Env11=basic.produce_env_init1(q_chi,q_D)


Env12=basic.produce_env_init2(q_chi,q_D)
Env13=basic.produce_env_init3(q_chi,q_D)
Env14=basic.produce_env_init2(q_chi,q_D)
Env15=basic.produce_env_init3(q_chi,q_D)



#print a_u.printDiagram()
##
#a_u,b_u,c_u,d_u=basic.Reload_Full()
##a_u,b_u,c_u,d_u,a,b,c,d=basic.Reload_Full_previous(a_u, b_u, c_u, d_u)
##print a_u
##a_u,b_u,c_u,d_u,a,b,c,d=basic.slighty_random(a_u,b_u,c_u,d_u,a,b,c,d)
#Landa=[Landa_1,Landa_2,Landa_3,Landa_4,Landa_5,Landa_6,Landa_7,Landa_8]
#Gamma_a,Gamma_b,Gamma_c,Gamma_d,Landa_1,Landa_2,Landa_3,Landa_4,Landa_5,Landa_6,Landa_7,Landa_8=basic.produce_gamma_abcd(a_u,b_u,c_u,d_u,Gamma_a,Gamma_b,Gamma_c,Gamma_d,Landa_1,Landa_2,Landa_3,Landa_4,Landa_5,Landa_6,Landa_7,Landa_8)
###

fileEnergyf = open("Data/Energy.txt", "w")
fileMf = open("Data/M.txt", "w")
filechiralf = open("Data/chiral.txt", "w")
fileDxf = open("Data/Dx.txt", "w")
fileDyf = open("Data/Dy.txt", "w")
fileDrf = open("Data/Dr.txt", "w")
fileLength = open("Data/Length.txt", "w")

fileCorrH = open("Data/CorrelationH.txt", "w")
fileCorrV = open("Data/CorrelationV.txt", "w")
fileCorrLength = open("Data/CorrelationLength.txt", "w")
fileSpectrum = open("Data/Spectrum.txt", "w")
fileSpectrum2 = open("Data/Spectrum2.txt", "w")
fileSpectrum4 = open("Data/Spectrum4.txt", "w")
fileSpectrum5 = open("Data/Spectrum5.txt", "w") 
fileSpectrum6 = open("Data/Spectrum6.txt", "w")
fileSpectrum8 = open("Data/Spectrum8.txt", "w")
fileSpectrum10 = open("Data/Spectrum10.txt", "w")

fileSpectrum0 = open("Data/Spectrum0.txt", "w")
fileSpectrum1 = open("Data/Spectrum1.txt", "w")
fileSpectrum3 = open("Data/Spectrum3.txt", "w")

fileSpectrumC4 = open("Data/SpectrumC4.txt", "w")
fileSpectrumC6 = open("Data/SpectrumC6.txt", "w")
fileSpectrumC8 = open("Data/SpectrumC8.txt", "w")


fileCorrHH = open("Data/CorrelationHH.txt", "w")
fileCorrVV = open("Data/CorrelationVV.txt", "w")
fileCorrLength = open("Data/CorrelationLength.txt", "w")

fileCorrHH1 = open("Data/CorrelationHH1.txt", "w")
fileCorrVV1 = open("Data/CorrelationVV1.txt", "w")
fileCorrLength1 = open("Data/CorrelationLength1.txt", "w")


E_list=[]
M_list=[]
h_list=[]
Dx_list=[]
Dy_list=[]
Dr_list=[]
len_list=[]
h_coupling=[0,0,h_z,J_staggered]

chiral_list=[]
for i in xrange(Num_iter):

###############Remove#######################
# N_iteritebd=20+i*10
# D=[4+i]
# chi=[int(((4+i)**2)+(((4+i)**2)/3))]
# print D, chi, i
# q_D, q_chi, q_phys=basic.full_make_bond(Model, D, chi, d_phys)
# Env=basic.produce_env_init(q_chi,q_D)
# Env1=basic.produce_env_init1(q_chi,q_D)
# Env2=basic.Rand_env_total(Env)
# Env3=basic.produce_env_init1(q_chi,q_D)
# Env4=basic.produce_env_init2(q_chi,q_D)
# Env5=basic.produce_env_init3(q_chi,q_D)
# Env6=basic.produce_env_init2(q_chi,q_D)
# Env7=basic.produce_env_init3(q_chi,q_D)
# Env8=basic.produce_env_init(q_chi,q_D)
# Env9=basic.produce_env_init1(q_chi,q_D)
# Env10=basic.Rand_env_total(Env)
# Env11=basic.produce_env_init1(q_chi,q_D)
# Env12=basic.produce_env_init2(q_chi,q_D)
# Env13=basic.produce_env_init3(q_chi,q_D)
# Env14=basic.produce_env_init2(q_chi,q_D)
# Env15=basic.produce_env_init3(q_chi,q_D)
# 
####################################################################

 h_coupling[0]=J_kagome
 h_coupling[1]=J_Chiral+i*grid_val
 h_list.append(J_Chiral+i*grid_val)
 print J_Chiral+i*grid_val, h_coupling
 
 ###############################iTEBD################################
 #Gamma_a,Gamma_b,Gamma_c,Gamma_d,Landa_1,Landa_2,Landa_3,Landa_4,Landa_5, Landa_6, Landa_7,Landa_8=basic.Reload_itebd()

 Gamma_a,Gamma_b,Gamma_c,Gamma_d,Landa_1,Landa_2,Landa_3,Landa_4,Landa_5,Landa_6,Landa_7,Landa_8=itebd.itebd_eff(Gamma_a,Gamma_b,Gamma_c,Gamma_d,Landa_1,Landa_2,Landa_3,Landa_4,Landa_5,Landa_6,Landa_7,Landa_8,chi,q_phys,D,N_iteritebd,h_coupling,Model,q_D,fixbond_itebd,start_itebd, division_itebd,itebd_method)

 basic.Store_itebd(Gamma_a,Gamma_b,Gamma_c,Gamma_d,Landa_1,Landa_2,Landa_3,Landa_4,Landa_5, Landa_6, Landa_7,Landa_8)

#print Landa_1, '\n', Landa_2, '\n',Landa_3 ,'\n',Landa_4 ,'\n',Landa_5, '\n',Landa_6, '\n',Landa_7, '\n', Landa_8 
#print Gamma_a

 Landa=[Landa_1,Landa_2,Landa_3,Landa_4,Landa_5,Landa_6,Landa_7,Landa_8]
 a_u,b_u,c_u,d_u=basic.produce_abcd_gamma(Landa, Gamma_a,Gamma_b,Gamma_c,Gamma_d)

 a_u,b_u,c_u,d_u=basic.increase_norm(a_u,b_u,c_u,d_u, 6.00)
 a_u,b_u,c_u,d_u=basic.make_equall_dis(a_u,b_u,c_u,d_u)

 #a_u,b_u,c_u,d_u=basic.Reload_Full()
 print a_u.printDiagram(),b_u.printDiagram(),c_u.printDiagram(),d_u.printDiagram(), Env[0].printDiagram()
 #basic.Reload_EnvEnv(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15)
 #a_u,b_u,c_u,d_u,a,b,c,d=basic.total_random(a_u,b_u,c_u,d_u,a,b,c,d)
#

#E_value=basic.E_total(a_u,b_u,c_u,d_u,a,b,c,d,Env,Env1,Env2,Env3,D,h_coupling,q_phys,chi,Corner_method,Model,N_env)
#print 'E_toal=', E_value
#basic.Store_EnvEnv(Env,Env1,Env2,Env3)

 #a_u,b_u,c_u,d_u=basic.total_random(a_u,b_u,c_u,d_u)
 #a_u,b_u,c_u,d_u=basic.total_random1(a_u,b_u,c_u,d_u)



 E_value,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.E_total_product(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,h_coupling,q_phys,chi,Corner_method,Model,N_env)
 print 'E_toal=', E_value
 basic.Store_EnvEnv(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15)



 a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=Fullupdate.Full_Update(a_u,b_u,c_u,d_u,chi,q_phys,D,h_coupling,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,Gauge,Corner_method,N_iterFull,Acc_E,Model,N_grad, Grad_method,Inv_method,N_svd,N_env,method,check_step,iteration_per_step)


 E_value,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.E_total_product(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,h_coupling,q_phys,chi,Corner_method,Model,N_env)
 print 'E_toal=', E_value
 E_list.append(E_value)
 basic.Store_EnvEnv(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15)


 M_value=basic.M_total(a_u,b_u,c_u,d_u,Env,Env2,Env8,Env10,D,h_coupling,q_phys,chi,Corner_method,Model)
 print 'M_s=', M_value
 M_list.append(M_value)

 D_x, D_y, D_R=basic.Translational_sym(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,h_coupling,q_phys,chi,Corner_method,Model,N_env)
 print  "D_xyr", D_x, D_y, D_R

 Dx_list.append(D_x)
 Dy_list.append(D_y)
 Dr_list.append(D_R)


 chiral_val=basic.chiral(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,q_phys,chi,Corner_method,Model,N_env)
 chiral_list.append(chiral_val)
 print "chiral_val", chiral_val

 #gap_val=basic_H.Spectrum_Arnoldi_right0(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,fileSpectrum,Arnoldi_bond)

 #basic_H.central_charge(Env8,Env10,fileSpectrum3,Arnoldi_bond)

 basic_H.ES_spectrum4(Env,Env1,Env8,Env10,fileSpectrum1,Arnoldi_bond,fileSpectrumC4)
 basic_H.ES_spectrum6(Env,Env1,Env8,Env10,fileSpectrum2,Arnoldi_bond,fileSpectrumC6)
 basic_H.ES_spectrum8(Env,Env1,Env8,Env10,fileSpectrum3,Arnoldi_bond,fileSpectrumC8)

 gap_val,gap_val1=basic_H.Spectrum_Arnoldi_right_Eff(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,fileSpectrum,Arnoldi_bond)


 #gap_val=1.0
 len_list.append(1.0/gap_val)
 print "length, length1", 1.0/gap_val,1.0/gap_val1


 #basic_H.CorrelationH(a_u,b_u,Env,Env4,Env8,D,h_coupling,q_phys,chi,Corner_method,Model,distance_val,fileCorrH,Arnoldi_bond)
 #basic_H.CorrelationH(c_u,d_u,Env2,Env6,Env10,D,h_coupling,q_phys,chi,Corner_method,Model,distance_val,fileCorrV,Arnoldi_bond)


 basic.Store(h_list,E_list, M_list, Dx_list,Dy_list,Dr_list,chiral_list,len_list,fileEnergyf,fileMf,filechiralf,fileDxf,fileDyf,fileDrf,fileLength,i)



