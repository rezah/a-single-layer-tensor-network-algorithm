import pyUni10 as uni10
import copy
import time
import basic
import cmath

#@profile
def Var_ab(a_u, b_u,Env,Env4,Env8,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step):

 
 t0=time.time()
 N_u, l_u, r_u, q_u, qq_u = Qr_lQ_decom(Env,Env4,Env8,U,a_u,b_u)
 print time.time()-t0, "Qr_lQ_decom"

# t0=time.time()
# if Gauge is 'Fixed':

 t0=time.time()
 N_u=N_Positiv(N_u)
 print time.time()-t0, "Positiv"


# #print time.time() - t0, "Positive"
 
 l_up=copy.copy(l_u)
 r_up=copy.copy(r_u)

# l_up,r_up=svd_init(l_up,r_up,U)

 t0=time.time()
 if method is "SVD_QR":
  l_up, r_up=Do_optimization_Full(N_u, l_u, r_u, l_up, r_up, U,N_svd)
# l_up, r_up=Do_optimization_grad(N_u, l_u, r_u, l_up, r_up, U)
  a_up, b_up =reproduce_ab(r_up, l_up, q_u, qq_u)
 print time.time() - t0, "QR"



 if method is "Grad":
  a_up=copy.copy(a_u)
  b_up=copy.copy(b_u)
  a_up, b_up=Do_optimization_Grad_com(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, a_up, b_up, U,N_grad, Opt_method,Gauge)



 if method is "SVD":
  a_up=copy.copy(a_u)
  b_up=copy.copy(b_u)
  a_up, b_up=Do_optimization_svd(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, a_up, b_up, U,N_grad, Opt_method, Inv_method, N_svd,Gauge, check_step)



 a_up, b_up=equall_dis(a_up,b_up) 
 a_up, b_up=equall_dis(a_up,b_up) 

 #Dis_val=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_up,b_up)
 ##print "Dis_final", Dis_val

 a_up=basic.max_ten(a_up)
 b_up=basic.max_ten(b_up)

 Maxa=basic.MaxAbs(a_up)
 Maxb=basic.MaxAbs(b_up)
 #print Maxa, Maxb


 ap=basic.make_ab(a_up)
 bp=basic.make_ab(b_up)

 return  a_up, b_up 


def Var_ca(c_u, a_u,Env2,Env1,Env,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step):


 t0=time.time()
 N_u, l_u, r_u, q_u, qq_u = Qr_lQ_decom_1(Env2,Env1,Env, U,c_u,a_u)
 print time.time() - t0, "Qr_lQ_decom"

 

# if Gauge is 'Fixed':
 t0=time.time()
 N_u=N_Positiv_1(N_u)
 print time.time() - t0, "Qr_lQ_decom"
 
 l_up=copy.copy(l_u)
 r_up=copy.copy(r_u)
 
 #l_up,r_up=svd_init_1(l_up,r_up,U)

 
# Dis_val=Dis_fQR_1(N_u, l_u, r_u, l_up, r_up, U )
# #print "Dis", Dis_val
 t0=time.time()

 if method is "SVD_QR":
  l_up, r_up=Do_optimization_Full_1(N_u, l_u, r_u, l_up, r_up, U,N_svd)
#  l_up, r_up=Do_optimization_grad_1(N_u, l_u, r_u, l_up, r_up, U)
  c_up,a_up = reproduce_ca(r_up, l_up, q_u, qq_u)
 print time.time() - t0, "Qr_lQ_decom"


 if method is "Grad":
  a_up=copy.copy(a_u)
  c_up=copy.copy(c_u)
  a_up, c_up=Do_optimization_Grad_com1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, c_u, a_up, c_up, U,N_grad, Opt_method,Gauge)



 if method is "SVD":
  a_up=copy.copy(a_u)
  c_up=copy.copy(c_u)
  a_up, c_up=Do_optimization_svd1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, c_u, a_up, c_up, U,N_grad, Opt_method, Inv_method, N_svd,Gauge, check_step)





# Dis_val=Dis_fQR_1(N_u, l_u, r_u, l_up, r_up, U )
# #print "DisFFF", Dis_val

 c_up, a_up=equall_dis_1(c_up,a_up) 
 c_up, a_up=equall_dis_1(c_up,a_up) 
 
 #Dis_val=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u,a_u,c_up,a_up)
 ##print "Dis_final", Dis_val


 c_up=basic.max_ten(c_up)
 a_up=basic.max_ten(a_up)

 Maxa=basic.MaxAbs(a_up)
 Maxc=basic.MaxAbs(c_up)
 #print Maxa, Maxc

 
 cp=basic.make_ab(c_up)
 ap=basic.make_ab(a_up)

 return  c_up, a_up


def produce_Env(a,b,c,d,c1, c2,c3,c4,Ta1, Tb1,Ta2, Tb2,Ta3, Tb3,Ta4, Tb4,D,d_phys):

 c1.setLabel([0,1])
 Tb1.setLabel([1,2,-2,3])
 E1=c1*Tb1
 E1.permute([0,2,-2,3],2)
 E1.setLabel([1,2,-2,3])
 E1.permute([1,2,-2,3],3)
 
 c2.setLabel([1,0])
 Ta1.setLabel([3,2,-2,1])
 E2=c2*Ta1
 E2.permute([0,2,-2,3],2)
 E2.setLabel([5,4,-4,3])
 E2.permute([3,4,-4,5],4)
 
 E3=copy.copy(Ta2)
 E3.setLabel([7,6,-6,5])
 E3.permute([5,6,-6,7],3)
 

 c3.setLabel([8,7])
 Tb2.setLabel([7,15,-15,22])
 E4=(c3*Tb2)
 E4.permute([8,15,-15,22],2)
 E4.setLabel([9,8,-8,7])
 E4.permute([7,8,-8,9],3)

 E5=copy.copy(Tb3)
 E5.setLabel([11,10,-10,9])
 E5.permute([9,10,-10,11],2)
 
 c4.setLabel([11,10])
 Ta3.setLabel([10,12,-12,13])
 E6=c4*Ta3
 E6.permute([11,12,-12,13],1)
 E6.setLabel([13,12,-12,11])
 E6.permute([11,12,-12,13],1)

 E7=copy.copy(Ta4)
 E7.setLabel([13,14,-14,15])
 E7.permute([13,14,-14,15],1)

 E8=copy.copy(Tb4)
 E8.setLabel([15,16,-16,1])
 E8.permute([15,16,-16,1],1)
 b.setLabel([18,-18,20,-20,6,-6,4,-4])
 c.setLabel([14,-14,12,-12,19,-19,17,-17])
 a.setLabel([16,-16,17,-17,18,-18,2,-2])
 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 Norm=(((((E1*E8)*(a))*((E7*E6)*(c))))*(((E2*E3)*(b))))*((E4*E5)*d)
 if Norm[0].real < 0: E1=-1.0*E1;

 while abs(Norm[0])<1.0e-2: 
  E1, E2, E3, E4, E5, E6, E7, E8,a, b, c, d=checking_norm(E1, E2, E3, E4, E5, E6, E7, E8,a, b, c, d)
  b.setLabel([18,-18,20,-20,6,-6,4,-4])
  c.setLabel([14,-14,12,-12,19,-19,17,-17])
  a.setLabel([16,-16,17,-17,18,-18,2,-2])
  d.setLabel([19,-19,10,-10,8,-8,20,-20])
  Norm=(((((E1*E8)*(a))*((E7*E6)*(c))))*(((E2*E3)*(b))))*((E4*E5)*d)
  ##print Norm[0]


 while abs(Norm[0])>1.0e+5: 
  E1, E2, E3, E4, E5, E6, E7, E8,a, b, c, d=checking_norm(E1, E2, E3, E4, E5, E6, E7, E8,a, b, c, d)
  b.setLabel([18,-18,20,-20,6,-6,4,-4])
  c.setLabel([14,-14,12,-12,19,-19,17,-17])
  a.setLabel([16,-16,17,-17,18,-18,2,-2])
  d.setLabel([19,-19,10,-10,8,-8,20,-20])
  Norm=(((((E1*E8)*(a))*((E7*E6)*(c))))*(((E2*E3)*(b))))*((E4*E5)*d)
  ##print Norm[0]
 ##print "Final_Norm", Norm[0]

 return E1, E2, E3, E4, E5, E6, E7, E8


def checking_norm(E1, E2, E3, E4, E5, E6, E7, E8,a, b, c, d):
  
 b.setLabel([18,-18,20,-20,6,-6,4,-4])
 c.setLabel([14,-14,12,-12,19,-19,17,-17])
 a.setLabel([16,-16,17,-17,18,-18,2,-2])
 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 Norm=(((((E1*E8)*(a))*((E7*E6)*(c))))*(((E2*E3)*(b))))*((E4*E5)*d)
 if Norm[0] < 0: E1=-1.0*E1;
 if Norm[0] < 1.0e-2:
  ##print "Norm[0] < 1.0e+1 ", Norm[0]
  E1*=1.5
  E8*=1.5
  E2*=1.5
  E3*=1.5
  E4*=1.5
  E6*=1.5
  E7*=1.5
#  a_u*=5.0
#  b_u*=5.0
#  c_u*=5.0
#  d_u*=5.0
#  a=basic.make_ab(a_u)
#  b=basic.make_ab(b_u)
#  c=basic.make_ab(c_u)
#  d=basic.make_ab(d_u)
 elif Norm[0] > 1.e+5:
  ##print "Norm[0] > 1.e+8 ", Norm[0]
  E1*=0.5
  E8*=0.5
  E2*=0.5
  E3*=0.5
  E4*=0.5
  E6*=0.5
  E7*=0.5
#  a_u*=0.20
#  b_u*=0.20
#  c_u*=0.20
#  d_u*=0.20
#  a=basic.make_ab(a_u)
#  b=basic.make_ab(b_u)
#  c=basic.make_ab(c_u)
#  d=basic.make_ab(d_u)
 return E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d 


def Energy_ab_time(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_u_f,b_u_f):

 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 c.setLabel([14,-14,12,-12,19,-19,17,-17])


 a_u.setLabel([53,16,17,18,2])
 a_d=copy.copy(a_u_f)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,53,-16,-17])


 b_u.setLabel([54,18,20,6,4])
 b_d=copy.copy(b_u_f)
 b_d.cTranspose()
 b_d.setLabel([-6,-4,54,-18,-20])



 Val=((((E1*E8)*(a_u*a_d))*((E7*E6)*(c))))*(((E4*E5)*d)*((E2*E3)*(b_u*b_d)))
 Norm_f=Val

 ##print Norm_f[0]
 U.setLabel([51,52,53,54])
 #U.identity()

 ##print U
 a_u.setLabel([53,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,51,-16,-17])


 b_u.setLabel([54,18,20,6,4])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([-6,-4,52,-18,-20])


 Val=((((E1*E8)*(a_u*a_d))*((E7*E6)*(c)))*U)*(((E4*E5)*d)*((E2*E3)*(b_u*b_d)))
 ##print Norm_f[0], Val[0],cmath.phase(Norm_f[0]),cmath.phase(Val[0])
 return Val[0]/Norm_f[0]

def  Energy_ab(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u):

 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 c.setLabel([14,-14,12,-12,19,-19,17,-17])

 a_u.setLabel([53,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,53,-16,-17])

 b_u.setLabel([54,18,20,6,4])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([-6,-4,54,-18,-20])

 Val=((((E1*E8)*(a_u*a_d))*((E7*E6)*(c))))*(((E4*E5)*d)*((E2*E3)*(b_u*b_d)))
 Norm_f=Val

 ##print Norm_f[0]
 U.setLabel([51,52,53,54])
 #U.identity()

 ##print U
 a_u.setLabel([53,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,51,-16,-17])


 b_u.setLabel([54,18,20,6,4])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([-6,-4,52,-18,-20])


 Val=((((E1*E8)*(a_u*a_d))*((E7*E6)*(c)))*U)*(((E4*E5)*d)*((E2*E3)*(b_u*b_d)))
 ##print Norm_f,Norm_f[0], Val[0],cmath.phase(Norm_f[0]),cmath.phase(Val[0])
 
 return Val[0]/Norm_f[0]



##@profile 
def  Energy_ab_product(a_up,b_up,Env,Env4,Env8, U):
 #print "Hi"
 bdi2 = uni10.Bond(uni10.BD_IN, 2)
 bdi4 = uni10.Bond(uni10.BD_IN, 4)

 bdo2 = uni10.Bond(uni10.BD_OUT, 2)
 bdo4 = uni10.Bond(uni10.BD_OUT, 4)

 a_u=uni10.UniTensor(uni10.CTYPE,[bdi4,bdi2,a_up.bond(1),a_up.bond(2),a_up.bond(3),a_up.bond(4)])
 a_u.putBlock(a_up.getBlock())

 b_u=uni10.UniTensor(uni10.CTYPE,[bdi4,bdi2,b_up.bond(1),b_up.bond(2),b_up.bond(3),b_up.bond(4)])
 b_u.putBlock(b_up.getBlock())


 a_u.setLabel([101,53,16,15,13,4])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([20,2,101,53,18,19])

 b_u.setLabel([54,201,13,14,11,8])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([22,6,54,201,20,21])

 Env[0].setLabel([0,1])
 Env[1].setLabel([1,2,3])
 Env[2].setLabel([3,4,5])
 Env[11].setLabel([17,16,0])
 Env[10].setLabel([29,18,17])
 Env[9].setLabel([28,29])
 Env[8].setLabel([27,19,28])
 Env[7].setLabel([26,15,27])

 Env4[2].setLabel([5,6,7])
 Env4[7].setLabel([25,21,26])

 Env8[2].setLabel([7,8,9])
 Env8[7].setLabel([24,14,25])
 Env8[3].setLabel([9,10])
 Env8[4].setLabel([10,11,12])
 Env8[5].setLabel([12,22,23])
 Env8[6].setLabel([23,24])



 Val=(((((((((Env8[3]*Env8[4])*Env8[5])*Env8[6])*Env8[2])*b_u)*Env8[7])*Env4[7])*b_d)*Env4[2])*(((((((((Env[0]*Env[11])*Env[10])*Env[9])*Env[8])*a_d)*Env[1])*Env[2])*a_u)*Env[7])

 #print "Hello", Val.printDiagram()
 Norm_f=Val

 ##print Norm_f[0]
 U.setLabel([51,52,53,54])
 #U.identity()




 a_u.setLabel([101,53,16,15,13,4])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([20,2,101,51,18,19])

 b_u.setLabel([54,201,13,14,11,8])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([22,6,52,201,20,21])





# ##print U.#printDiagram()
# a_u.setLabel([101,53,16,17,18,2])
# a_d=copy.copy(a_u)
# a_d.cTranspose()
# a_d.setLabel([-18,-2,101,51,-16,-17])


# b_u.setLabel([54,201,18,20,6,4])
# b_d=copy.copy(b_u)
# b_d.cTranspose()
# b_d.setLabel([-6,-4,52,201,-18,-20])


 Val1=((((((((((Env8[3]*Env8[4])*Env8[5])*Env8[6])*Env8[2])*b_u)*Env8[7])*Env4[7])*b_d)*Env4[2])*U)*(((((((((Env[0]*Env[11])*Env[10])*Env[9])*Env[8])*a_d)*Env[1])*Env[2])*a_u)*Env[7])

 #print Val1,Val1[0]/Norm_f[0]
 
 return Val1[0]/Norm_f[0]


def  Energy_ca(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u,a_u):

 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 b.setLabel([18,-18,20,-20,6,-6,4,-4])

 a_u.setLabel([54,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,54,-16,-17])

 c_u.setLabel([53,14,12,19,17])
 c_d=copy.copy(c_u)
 c_d.cTranspose()
 c_d.setLabel([-19,-17,53,-14,-12])

 Val=((((E1*E8)*(a_d*a_u))*((E7*E6)*(c_u*c_d))))*(((E4*E5)*(d))*((E2*E3)*(b)))
 Norm_f=Val



 U.setLabel([51,52,53,54])
 a_u.setLabel([54,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,52,-16,-17])

 c_u.setLabel([53,14,12,19,17])
 c_d=copy.copy(c_u)
 c_d.cTranspose()
 c_d.setLabel([-19,-17,51,-14,-12])


 Val=((((E1*E8)*(a_d*a_u)*U)*((E7*E6)*(c_u*c_d))))*(((E4*E5)*(d))*((E2*E3)*(b)))

 return Val[0]/Norm_f[0]


##@profile 

def  Energy_ca_product(c_up,a_up,Env,Env1,Env2,U):


 bdi2 = uni10.Bond(uni10.BD_IN, 2)
 bdi4 = uni10.Bond(uni10.BD_IN, 4)

 bdo2 = uni10.Bond(uni10.BD_OUT, 2)
 bdo4 = uni10.Bond(uni10.BD_OUT, 4)

 a_u=uni10.UniTensor(uni10.CTYPE,[bdi2,bdi4,a_up.bond(1),a_up.bond(2),a_up.bond(3),a_up.bond(4)])
 a_u.putBlock(a_up.getBlock())

 c_u=uni10.UniTensor(uni10.CTYPE,[bdi2,bdi4,c_up.bond(1),c_up.bond(2),c_up.bond(3),c_up.bond(4)])
 c_u.putBlock(c_up.getBlock())




 a_u.setLabel([100,54,9,8,7,4])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([13,2,100,54,11,12])

 c_u.setLabel([53,200,18,17,16,8])
 c_d=copy.copy(c_u)
 c_d.cTranspose()
 c_d.setLabel([23,12,53,200,21,22])

 Env[0].setLabel([0,1])
 Env[1].setLabel([1,2,3])
 Env[2].setLabel([3,4,5])
 Env[11].setLabel([10,9,0])
 Env[10].setLabel([19,11,10])
 Env[3].setLabel([5,6])
 Env[4].setLabel([6,7,14])
 Env[5].setLabel([14,13,15])

 Env1[5].setLabel([15,16,24])
 Env1[10].setLabel([20,18,19])

 Env2[5].setLabel([24,23,25])
 Env2[10].setLabel([29,21,20])
 Env2[6].setLabel([25,26])
 Env2[7].setLabel([26,17,27])
 Env2[8].setLabel([27,22,28])
 Env2[9].setLabel([28,29])



 Val=(((((((((Env2[6]*Env2[7])*Env2[8])*Env2[9])*Env2[10])*c_d)*Env2[5])*Env1[5])*c_u)*Env1[10])*(((((((((Env[0]*Env[1])*Env[2])*Env[3])*Env[4])*a_u)*Env[11])*Env[10])*a_d)*Env[5])
 #print  Val



 U.setLabel([51,52,53,54])


 a_u.setLabel([100,54,9,8,7,4])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([13,2,100,52,11,12])

 c_u.setLabel([53,200,18,17,16,8])
 c_d=copy.copy(c_u)
 c_d.cTranspose()
 c_d.setLabel([23,12,51,200,21,22])





 Val1=(((((((((Env2[6]*Env2[7])*Env2[8])*Env2[9])*Env2[10])*c_d)*Env2[5])*Env1[5])*c_u)*Env1[10])*((((((((((Env[0]*Env[1])*Env[2])*Env[3])*Env[4])*a_u)*Env[11])*Env[10])*a_d)*Env[5])*U)
 #print  Val1
 
 return Val1[0]/Val[0]



def Qr_lQ_decom(Env,Env4,Env8,U,a_u,b_u):
# a_u.setLabel([101,53,16,15,13,4])
# a_d=copy.copy(a_u)
# a_d.cTranspose()
# a_d.setLabel([20,2,101,53,18,19])

# b_u.setLabel([54,201,13,14,11,8])
# b_d=copy.copy(b_u)
# b_d.cTranspose()
# b_d.setLabel([22,6,54,201,20,21])

 Env[0].setLabel([0,1])
 Env[1].setLabel([1,2,3])
 Env[2].setLabel([3,4,5])
 Env[11].setLabel([17,16,0])
 Env[10].setLabel([29,18,17])
 Env[9].setLabel([28,29])
 Env[8].setLabel([27,19,28])
 Env[7].setLabel([26,15,27])

 Env4[2].setLabel([5,6,7])
 Env4[7].setLabel([25,21,26])

 Env8[2].setLabel([7,8,9])
 Env8[7].setLabel([24,14,25])
 Env8[3].setLabel([9,10])
 Env8[4].setLabel([10,11,12])
 Env8[5].setLabel([12,22,23])
 Env8[6].setLabel([23,24])


 A=copy.copy(a_u)
 A.setLabel([53,16,15,13,4])
 A.permute([16,15,4,53,13],3)
 
 q,r=qr_parity(A) 
 
 q.setLabel([16,15,4,82,83])
 r.setLabel([82,83,53,13])
 q.permute([16,15,82,83,4],2)
 r.permute([82,83,53,13],3)
 
 q_d=copy.copy(q)
 q_d.cTranspose()
 q_d.setLabel([-82,-83,2,18,19])
########################################## 
 A=copy.copy(b_u)
 A.setLabel([54,13,14,11,8])
 A.permute([54,13,14,11,8],2)
 
 l, qq=lq_parity(A) 
 
 l.setLabel([54,13,80,81])
 qq.setLabel([80,81,14,11,8])
 l.permute([54,13,80,81],2)
 qq.permute([80,81,14,11,8],3)
 
 qq_d=copy.copy(qq)
 qq_d.cTranspose()
 qq_d.setLabel([22,6,-80,-81,21])


 N=(((((((((Env8[3]*Env8[4])*Env8[5])*Env8[6])*Env8[2])*qq)*Env8[7])*Env4[7])*qq_d)*Env4[2])*(((((((((Env[0]*Env[11])*Env[10])*Env[9])*Env[8])*q_d)*Env[1])*Env[2])*q)*Env[7])

 N.permute([80,81,-82,-83,-80,-81,82,83],4)
 #print N.printDiagram()
 ##print r.#printDiagram(), l.#printDiagram() 
 ###testing####


 r.setLabel([82,83,53,18])
 l.setLabel([54,18,80,81])



# r_d=copy.copy(r)
# r_d.cTranspose()
# l_d=copy.copy(l)
# l_d.cTranspose()
# 
# 
# r_d.setLabel([-18,-82,-83,53])
# l_d.setLabel([-80,-81,54,-18])
# Norm=((N*(r*l))*(r_d*l_d))
# #print "Norm-QR", Norm[0]

 return N, l, r, q, qq


def Qr_lQ_decom_1(Env2,Env1,Env, U,c_u,a_u):
 Env[0].setLabel([0,1])
 Env[1].setLabel([1,2,3])
 Env[2].setLabel([3,4,5])
 Env[11].setLabel([10,9,0])
 Env[10].setLabel([19,11,10])
 Env[3].setLabel([5,6])
 Env[4].setLabel([6,7,14])
 Env[5].setLabel([14,13,15])

 Env1[5].setLabel([15,16,24])
 Env1[10].setLabel([20,18,19])

 Env2[5].setLabel([24,23,25])
 Env2[10].setLabel([29,21,20])
 Env2[6].setLabel([25,26])
 Env2[7].setLabel([26,17,27])
 Env2[8].setLabel([27,22,28])
 Env2[9].setLabel([28,29])


 A=copy.copy(a_u)
 A.setLabel([54,9,8,7,4])
 A.permute([54,8,9,4,7],2)
 
 l, qq=lq_parity(A) 
 
 l.setLabel([54,8,82,83])
 qq.setLabel([82,83,9,4,7])
 l.permute([54,8,82,83],2)
 qq.permute([9,82,83,7,4],3)
 
 qq_d=copy.copy(qq)
 qq_d.cTranspose()
 qq_d.setLabel([13,2,11,-82,-83])

###############################################################################
 A=copy.copy(c_u)
 A.setLabel([53,18,17,16,8])
 A.permute([18,17,16,53,8],3)
 
 
 q,r=qr_parity(A)
 
 q.setLabel([18,17,16,80,81])
 r.setLabel([80,81,53,8])
 q.permute([18,17,16,80,81],2)
 r.permute([80,81,53,8],3)
 
 q_d=copy.copy(q)
 q_d.cTranspose()
 q_d.setLabel([23,-80,-81,21,22])
 
 
 N=(((((((((Env2[6]*Env2[7])*Env2[8])*Env2[9])*Env2[10])*q_d)*Env2[5])*Env1[5])*q)*Env1[10])*(((((((((Env[0]*Env[1])*Env[2])*Env[3])*Env[4])*qq)*Env[11])*Env[10])*qq_d)*Env[5])
 #print  Val
 N.permute([-80,-81,82,83,80,81,-82,-83],4)
 ##print N.#printDiagram() 

 r.setLabel([80,81,53,17])
 l.setLabel([54,17,82,83])
 q.setLabel([18,100,16,80,81])

## ###testing####
# r_d=copy.copy(r)
# r_d.cTranspose()
# l_d=copy.copy(l)
# l_d.cTranspose()
# ##############
# 
# r_d.setLabel([-17,-80,-81,53])
# l_d.setLabel([-82,-83,54,-17])

# Norm=((N*(r*l))*(r_d*l_d))
# #print "Norm-QR", Norm[0]
 return N, l, r, q, qq



#def Energy_ab_positive(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U, a_u, b_u):
# N, l, r, q, qq=Qr_lQ_decom(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u)
# N=N_Positiv(N)
# U.setLabel([51,52,53,54])
# Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
# Iden.identity()
# Iden.setLabel([51,52,53,54])

# r_d=copy.copy(r)
# l_d=copy.copy(l)
# l_d.cTranspose()
# r_d.cTranspose()
# r_d.setLabel([-18,-82,-83,51])
# l_d.setLabel([-80,-81,52,-18])

# Val=((N*(r*l))*(r_d*l_d)*U)
# #print Val
# Val1=((N*(r*l))*(r_d*l_d)*Iden)
# #print Val1
# return Val[0]/Val1[0]
# 
#def Energy_ca_positive(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u,a_u):
# N, l, r, q, qq=Qr_lQ_decom_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u,a_u)
# N=N_Positiv_1(N)

# U.setLabel([51,52,53,54])
# Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
# Iden.identity()
# Iden.setLabel([51,52,53,54])

# r_d=copy.copy(r)
# l_d=copy.copy(l)
# l_d.cTranspose()
# r_d.cTranspose()
# 
# 
# r_d.setLabel([-17,-80,-81,51])
# l_d.setLabel([-82,-83,52,-17])


# Val=((N*(r*l))*(r_d*l_d)*U)
# #print Val
# Val1=((N*(r*l))*(r_d*l_d)*Iden)
# #print Val1
# return Val[0]/Val1[0]



 
 
def reproduce_ab(r_u, l_u, q_u, qq_u):

 a_up=q_u*r_u
 a_up.permute([53,16,15,18,4],3)


 b_up=qq_u*l_u
 b_up.permute([54,18,14,11,8],3)


 return a_up, b_up
 
def reproduce_ca(r_u, l_u, q_u, qq_u):

 c_up=q_u*r_u
 c_up.permute([53,18,100,16,17],3)


 a_up=qq_u*l_u
 a_up.permute([54,9,17,7,4],3)

 return c_up, a_up

######@profile
def Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, U, a_u, b_u, a_up, b_up):

 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 c.setLabel([14,-14,12,-12,19,-19,17,-17])
 a.setLabel([16,-16,17,-17,18,-18,2,-2])

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])

 H1=copy.copy(U)
 H1.cTranspose()
 H1.setLabel([-20,-40,51,52])

 U_dagger=copy.copy(H1)
 U_dagger.setLabel([51,52,53,54])

 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])
 U_dagger.setLabel([51,52,53,54])
 
 a_up.setLabel([53,16,17,18,2])
 a_dp=copy.copy(a_up)
 a_dp.cTranspose()
 a_dp.setLabel([-18,-2,51,-16,-17])

 a_u.setLabel([53,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,51,-16,-17])
 

 b_u.setLabel([54,18,20,6,4])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([-6,-4,52,-18,-20])

 
 b_up.setLabel([54,18,20,6,4])
 b_dp=copy.copy(b_up)
 b_dp.cTranspose()
 b_dp.setLabel([-6,-4,52,-18,-20])

 E_c=(E7*E6)*(c)
 E_d=((E4*E5)*d)
 Val=(((((E1*E8)*(a_u*a_d))*(E_c))*H)*(((E2*E3)*(b_u*b_d))))*((E4*E5)*d)
 ##print 'Val=',Val[0]
 Val1=(((((E1*E8)*(a_dp*a_up))*(E_c))*Iden)*((E2*E3)*(b_up*b_dp)))*(E_d)
 ##print 'Val1=',Val1[0]
 Val2=(((((E1*E8)*(a_u*a_dp))*(E_c))*U)*(((E2*E3)*(b_u*b_dp))))*(E_d)
 ##print 'Val2=',Val2[0]
 Val3=(((((E1*E8)*(a_up*a_d))*(E_c))*U_dagger)*(((E2*E3)*(b_up*b_d))))*(E_d)

 val_f=Val[0]+Val1[0]-Val2[0]-Val3[0]
# val_f=Val1[0]-Val2[0]-Val3[0]

 return  abs(val_f) 
#####@profile
def  Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U, c_u, a_u, c_up, a_up):

 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 b.setLabel([18,-18,20,-20,6,-6,4,-4])


 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])

 H1=copy.copy(U)
 H1.cTranspose()

 U_dagger=copy.copy(H1)
 U_dagger.setLabel([51,52,53,54])


 H1.setLabel([-20,-40,51,52])
 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])

 a_up.setLabel([54,16,17,18,2])
 a_dp=copy.copy(a_up)
 a_dp.cTranspose()
 a_dp.setLabel([-18,-2,52,-16,-17])

 a_u.setLabel([54,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,52,-16,-17])
  
 c_up.setLabel([53,14,12,19,17])
 c_dp=copy.copy(c_up)
 c_dp.cTranspose()
 c_dp.setLabel([-19,-17,51,-14,-12])
 
 c_u.setLabel([53,14,12,19,17])
 c_d=copy.copy(c_u)
 c_d.cTranspose()
 c_d.setLabel([-19,-17,51,-14,-12])
 
 
 E_b=(E2*E3)*(b)
 E_d=((E4*E5)*d)
 
 Val=(((((E1*E8)*(a_u*a_d)*H)*((E7*E6)*(c_u*c_d))))*((E_b)))*(E_d)
 ##print 'Val=',Val[0]
 Val1=(((((E1*E8)*(a_dp*a_up))*((E7*E6)*(c_up*c_dp)*Iden)))*(E_b))*((E_d))
 ##print 'Val1=',Val1[0]
 Val2=(((((E1*E8)*(a_u*a_dp))*((E7*E6)*(c_u*c_dp)*U)))*((E_b)))*(E_d)
 ##print 'Val2=',Val2[0]
 Val3=(((((E1*E8)*(a_up*a_d))*((E7*E6)*(c_up*c_d)*U_dagger)))*((E_b)))*(E_d)
 ##print 'Val3=',Val3[0]
 
 val_f=Val[0]+Val1[0]-Val2[0]-Val3[0]
# val_f=Val[0]-2.00*Val2[0]#-Val3[0]

 return  abs(val_f) 


#@profile
def Dis_fQR(N, l, r, lp, rp, U ):
 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])

 H1=copy.copy(U)
 H1.cTranspose()

 U_dagger=copy.copy(U)
 U_dagger.cTranspose()
 U_dagger.setLabel([51,52,53,54])


 H1.setLabel([-20,-40,51,52])
 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])

 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 
 r_d.setLabel([-18,-82,-83,51])
 l_d.setLabel([-80,-81,52,-18])

 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 
 r_dp.setLabel([-18,-82,-83,51])
 l_dp.setLabel([-80,-81,52,-18])

 ##print N.#printDiagram(), H.#printDiagram(), r.#printDiagram() 
 Val=((N*(r*l))*(r_d*l_d)*H)
 ##print Val[0]
 Val1=((N*(rp*lp))*(r_dp*l_dp)*Iden)
 ##print Val1[0]
 Val2=((N*(rp*lp))*(r_d*l_d)*U_dagger)
 ##print Val2[0]
 Val3=((N*(r*l))*(r_dp*l_dp)*U)
 ##print Val3[0]
 Val_f=Val[0]+Val1[0]-Val2[0]-Val3[0]
 
 return abs(Val_f)


def Dis_fQR_1(N, l, r, lp, rp, U ):
 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])

 H1=copy.copy(U)
 H1.cTranspose()


 U_dagger=copy.copy(U)
 U_dagger.cTranspose()
 U_dagger.setLabel([51,52,53,54])


 H1.setLabel([-20,-40,51,52])
 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])

# r.permute([82,83,53,18],3)
# l.permute([80,81,54,20],3)

 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 
 r_d.setLabel([-17,-80,-81,51])
 l_d.setLabel([-82,-83,52,-17])

 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 
 r_dp.setLabel([-17,-80,-81,51])
 l_dp.setLabel([-82,-83,52,-17])


 Val=((N*(r*l))*(r_d*l_d)*H)
 ##print Val
 Val1=((N*(rp*lp))*(r_dp*l_dp)*Iden)
 ##print Val1
 Val2=((N*(rp*lp))*(r_d*l_d)*U_dagger)
 ##print Val2
 Val3=((N*(r*l))*(r_dp*l_dp)*U)
 ##print Val3
 Val_f=Val[0]+Val1[0]-Val2[0]-Val3[0]

 return abs(Val_f)



#@profile
def Do_optimization_Full(N_u, l_u, r_u, l_up, r_up, U,N_svd):

 r_up_first=copy.copy(r_up)
 l_up_first=copy.copy(l_up)
 checking_val=0
 
 Res=10
 Res1=20
 count=0
 #Distance_val=Dis_fQR(N_u, l_u, r_u, l_up, r_up, U)
 for q in xrange(N_svd[0]):
  ##print "\n", "\n"
  Distance_val=Dis_fQR(N_u, l_u, r_u, l_up, r_up, U)
  #print 'Dis0', Distance_val, abs(Res1-Res) / abs(Res), q
  r_up=optimum_0(N_u, l_u, r_u, l_up, r_up, U)
  l_up=optimum_1(N_u, l_u, r_u, l_up, r_up, U)


  Res=Res1
  Res1=Distance_val

  #if (q>1) and (Res1 > Res) and ((abs(Res1-Res) / abs(Res)) > 1.00e-4): checking_val=1;

  count+=1
  if count > 60: print 'Num_Opt > 60'; break;
  if abs(Res) > 1.00e-10:
   if (abs(Distance_val) < 1.00e-8) or ((abs(Res1-Res) / abs(Res)) < N_svd[1]): 
    print 'break, Dis', Distance_val, (abs(Res1-Res) / abs(Res)), count
    break
  else:
    if (abs(Distance_val[0]) < 1.00e-8) or (  abs(Res1-Res) < 1.00e-11  ): 
     print 'break, Dis', Distance_val[0], abs(Res1-Res)
     break
 Distance_val=Dis_fQR(N_u, l_u, r_u, l_up, r_up, U)
 ##print 'Dis', Distance_val, abs(Res1-Res) / abs(Res), q


 #if checking_val != 0:
  #r_up=copy.copy(r_up_first)
  #l_up=copy.copy(l_up_first)
  #l_up, r_up=Do_optimization_grad(N_u, l_u, r_u, l_up, r_up, U)


 return l_up, r_up

def Do_optimization_Full_1(N_u, l_u, r_u, l_up, r_up, U,N_svd):
 r_up_first=copy.copy(r_up)
 l_up_first=copy.copy(l_up)
 checking_val=0
 
 
 Res=10
 Res1=20
 count=0
 #Distance_val=Dis_fQR_1(N_u, l_u, r_u, l_up, r_up, U)
 for q in xrange(N_svd[0]):
  ##print "\n", "\n"
  Distance_val=Dis_fQR_1(N_u, l_u, r_u, l_up, r_up, U)
  #print 'Dis1', Distance_val, abs(Res1-Res) / abs(Res), q
  r_up=optimum_00(N_u, l_u, r_u, l_up, r_up, U)
  l_up=optimum_11(N_u, l_u, r_u, l_up, r_up, U)


  Res=Res1
  Res1=Distance_val

  #if (q>1) and (Res1 > Res) and ((abs(Res1-Res) / abs(Res)) > 1.00e-4): checking_val=1;

  count+=1
  if count > 30: print 'Num_Opt > 30'; break;
  if abs(Res) > 1.00e-10:
   if (abs(Distance_val) < 1.00e-7) or ((abs(Res1-Res) / abs(Res)) < N_svd[1]): 
    print 'break, Dis', Distance_val, (abs(Res1-Res) / abs(Res)), count
    break
  else:
    if (abs(Distance_val[0]) < 1.00e-7) or (  abs(Res1-Res) < 1.00e-11  ): 
     print 'break, Dis', Distance_val[0], abs(Res1-Res)
     break
 Distance_val=Dis_fQR_1(N_u, l_u, r_u, l_up, r_up, U)
 ##print 'Dis', Distance_val, abs(Res1-Res) / abs(Res), q


 #if checking_val != 0:
  #r_up=copy.copy(r_up_first)
  #l_up=copy.copy(l_up_first)
  #l_up, r_up=Do_optimization_grad_1(N_u, l_u, r_u, l_up, r_up, U)

 return l_up, r_up



#@profile
def optimum_0(N, l, r, lp, rp, U):
 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 U_dagger=copy.copy(U)
 U_dagger.cTranspose()
 U_dagger.setLabel([51,52,53,54])

 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 
 r_d.setLabel([-18,-82,-83,51])
 l_d.setLabel([-80,-81,52,-18])

 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 
 r_dp.setLabel([-18,-82,-83,51])
 l_dp.setLabel([-80,-81,52,-18])

 A2=(((lp*l_dp)*N)*Iden)
 A2.permute([-82,-83,51,-18,82,83,53,18],4)

 A2_trans=copy.copy(A2)
 A2_trans.cTranspose()
 A2=A2+A2_trans
 
 A2.setLabel([-82,-83,51,-18,82,83,53,18])
 
 A3=((r)*U*(l*l_dp))*N
 A3.permute([-82,-83,51,-18],0)
 A3p=((r_d)*U_dagger*(l_d*lp))*N
 A3p.permute([82,83,53,18],4)
 A3p.cTranspose()

 A3=A3+A3p
 
 A3.setLabel([-82,-83,51,-18])
 
 U, S, V=svd_parity(A2)
 U.cTranspose()
 V.cTranspose()
 S=inverse(S)
 
 U.setLabel([8,9,10,11,12,13,14,15])
 S.setLabel([4,5,6,7,8,9,10,11])
 V.setLabel([0,1,2,3,4,5,6,7])


 A2_inv=V*S*U
 A2_inv.permute([0,1,2,3,12,13,14,15],4)
 A2_inv.setLabel([82,83,53,18,-82,-83,51,-18])
 
 #distance_iden_val=distance_iden(A2_mat,A2_inv)
 ##print 'distance1=', distance_iden_val
 ##print A2.getBlock()*A2_inv


 A=A2_inv*A3
 A.setLabel([82,83,53,18])
 A.permute([82,83,53,18],3)

 rf=copy.copy(A)

 return rf



def Obtain_grad_r(N, l, r, lp, rp, U):

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 U_dagger=copy.copy(U)
 U_dagger.cTranspose()
 U_dagger.setLabel([51,52,53,54])

 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 
 r_d.setLabel([-18,-82,-83,51])
 l_d.setLabel([-80,-81,52,-18])

 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 
 r_dp.setLabel([-18,-82,-83,51])
 l_dp.setLabel([-80,-81,52,-18])

 A2=(((lp*l_dp)*N)*Iden)*r_dp

 A2.permute([82,83,53,18],4)
 D_r=copy.copy(A2)

 A2=(((lp*l_dp)*N)*Iden)*rp
 A2.permute([-82,-83,51,-18],0)
 A2.cTranspose()
 D_r=D_r+A2
 
 
 A3=((r)*U*(l*l_dp))*N
 A3.permute([-82,-83,51,-18],0)
 A3.cTranspose() 
 D_r=D_r+(-1.00)*A3


 A3p=((r_d)*U_dagger*(l_d*lp))*N
 A3p.permute([82,83,53,18],4)
 D_r=D_r+(-1.00)*A3p

 D_r.cTranspose()
 D_r.permute([82,83,53,18],3)
 return D_r


def Obtain_grad_l(N, l, r, lp, rp, U):

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 U_dagger=copy.copy(U)
 U_dagger.cTranspose()
 U_dagger.setLabel([51,52,53,54])
 
 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 r_d.setLabel([-18,-82,-83,51])
 l_d.setLabel([-80,-81,52,-18])
 
 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 r_dp.setLabel([-18,-82,-83,51])
 l_dp.setLabel([-80,-81,52,-18])
 
 A2=(((rp*r_dp)*N)*Iden)*l_dp
 A2.permute([54,18,80,81],4)
 D_l=copy.copy(A2)

 A2=(((rp*r_dp)*N)*Iden)*lp
 A2.permute([52,-18,-80,-81],0)
 A2.cTranspose()
 D_l=D_l+A2


 A3=((l)*U*(r*r_dp))*N
 A3.permute([52,-18,-80,-81],0)
 A3.cTranspose()
 D_l=D_l+(-1.0)*A3

 A3p=((l_d)*U_dagger*(rp*r_d))*N
 A3p.permute([54,18,80,81],4)
 D_l=D_l+(-1.0)*A3p

 D_l.cTranspose()

 D_l.permute([54,18,80,81],2)


 return D_l

#@profile
def optimum_1(N, l, r, lp, rp, U):

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 U_dagger=copy.copy(U)
 U_dagger.cTranspose()
 U_dagger.setLabel([51,52,53,54])
 
 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 r_d.setLabel([-18,-82,-83,51])
 l_d.setLabel([-80,-81,52,-18])
 
 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 r_dp.setLabel([-18,-82,-83,51])
 l_dp.setLabel([-80,-81,52,-18])
 
 A2=(((rp*r_dp)*N)*Iden)
 A2.permute([52,-18,-80,-81,54,18,80,81],4)
 A2_trans=copy.copy(A2)
 A2_trans.cTranspose()
 A2=A2+A2_trans
 A2.setLabel([52,-18,-80,-81,54,18,80,81])
 
 A3=((l)*U*(r*r_dp))*N
 A3.permute([52,-18,-80,-81],0)
 A3p=((l_d)*U_dagger*(rp*r_d))*N
 A3p.permute([54,18,80,81],4)
 A3p.cTranspose()
 A3=A3+A3p
 A3.setLabel([52,-18,-80,-81])
 
 U, S, V=svd_parity(A2)

 U.cTranspose()
 V.cTranspose()
 S=inverse(S)

 U.setLabel([8,9,10,11,12,13,14,15])
 S.setLabel([4,5,6,7,8,9,10,11])
 V.setLabel([0,1,2,3,4,5,6,7])

 A2_inv=V*S*U
 A2_inv.permute([0,1,2,3,12,13,14,15],4)
 A2_inv.setLabel([54,18,80,81,52,-18,-80,-81])

 A=A3*A2_inv
 A.setLabel([54,18,80,81])
 A.permute([54,18,80,81],2)

 lf=copy.copy(A)

 return lf



def optimum_00(N, l, r, lp, rp, U):
 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 U_dagger=copy.copy(U)
 U_dagger.cTranspose()
 U_dagger.setLabel([51,52,53,54])

 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 
 r_d.setLabel([-17,-80,-81,51])
 l_d.setLabel([-82,-83,52,-17])

 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 
 r_dp.setLabel([-17,-80,-81,51])
 l_dp.setLabel([-82,-83,52,-17])

 A2=(((lp*l_dp)*N)*Iden)
 ##print A2.#printDiagram(), r.#printDiagram(),l.#printDiagram()
 A2.permute([-80,-81,51,-17,80,81,53,17],4)

 A2_trans=copy.copy(A2)
 A2_trans.cTranspose()
 A2=A2+A2_trans
 
 A2.setLabel([-80,-81,51,-17,80,81,53,17])
 
 A3=((r)*U*(l*l_dp))*N
 A3.permute([-80,-81,51,-17],0)
 A3p=((r_d)*U_dagger*(l_d*lp))*N
 A3p.permute([80,81,53,17],4)
 A3p.cTranspose()
 #A3=addition_symmetric(A3,A3p)

 A3=A3+A3p
 
 A3.setLabel([-80,-81,51,-17])
 
 U, S, V=svd_parity(A2)
 ##print U.#printDiagram()
 U.cTranspose()
 V.cTranspose()
 S=inverse(S)
 
 U.setLabel([8,9,10,11,12,13,14,15])
 S.setLabel([4,5,6,7,8,9,10,11])
 V.setLabel([0,1,2,3,4,5,6,7])


 A2_inv=V*S*U
 A2_inv.permute([0,1,2,3,12,13,14,15],4)
 A2_inv.setLabel([80,81,53,17,-80,-81,51,-17])
 
 #distance_iden_val=distance_iden(A2_mat,A2_inv)
 ##print 'distance1=', distance_iden_val
 ##print A2.getBlock()*A2_inv

 A=A2_inv*A3
 A.setLabel([80,81,53,17])
 A.permute([80,81,53,17],3)

 rf=copy.copy(A)

 return rf





def Obtain_grad_r_1(N, l, r, lp, rp, U):

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 U_dagger=copy.copy(U)
 U_dagger.cTranspose()
 U_dagger.setLabel([51,52,53,54])

 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 
 r_d.setLabel([-17,-80,-81,51])
 l_d.setLabel([-82,-83,52,-17])

 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 
 r_dp.setLabel([-17,-80,-81,51])
 l_dp.setLabel([-82,-83,52,-17])

 A2=(((lp*l_dp)*N)*Iden)*r_dp
 #A2.permute([-80,-81,51,-17,80,81,53,17],4)

 A2.permute([80,81,53,17],4)
 D_r=copy.copy(A2)

 A2=(((lp*l_dp)*N)*Iden)*rp
 A2.permute([-80,-81,51,-17],0)
 A2.cTranspose()
 D_r=D_r+A2
 
 A3=((r)*U*(l*l_dp))*N
 A3.permute([-80,-81,51,-17],0)
 A3.cTranspose() 
 D_r=D_r+(-1.00)*A3


 A3p=((r_d)*U_dagger*(l_d*lp))*N
 A3p.permute([80,81,53,17],4)
 D_r=D_r+(-1.00)*A3p

 D_r.cTranspose()
 D_r.permute([80,81,53,17],3)
 return D_r


def Obtain_grad_l_1(N, l, r, lp, rp, U):

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])

 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 
 r_d.setLabel([-17,-80,-81,51])
 l_d.setLabel([-82,-83,52,-17])

 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 
 r_dp.setLabel([-17,-80,-81,51])
 l_dp.setLabel([-82,-83,52,-17])

 A2=(((rp*r_dp)*N)*Iden)*l_dp
 #A2.permute([52,-17,-82,-83,54,17,82,83],4)


 A2.permute([54,17,82,83],4)
 D_l=copy.copy(A2)

 A2=(((rp*r_dp)*N)*Iden)*lp
 A2.permute([52,-17,-82,-83],0)
 A2.cTranspose()
 D_l=D_l+A2


 A3=((l)*U*(r*r_dp))*N
 A3.permute([52,-17,-82,-83],0)
 A3.cTranspose()
 D_l=D_l+(-1.0)*A3

 A3p=((l_d)*U*(rp*r_d))*N
 A3p.permute([54,17,82,83],4)
 D_l=D_l+(-1.0)*A3p

 D_l.cTranspose()

 D_l.permute([54,17,82,83],2)
 return D_l



def optimum_11(N, l, r, lp, rp, U):
 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 U_dagger=copy.copy(U)
 U_dagger.cTranspose()
 U_dagger.setLabel([51,52,53,54])

 r_d=copy.copy(r)
 l_d=copy.copy(l)
 l_d.cTranspose()
 r_d.cTranspose()
 
 
 r_d.setLabel([-17,-80,-81,51])
 l_d.setLabel([-82,-83,52,-17])

 r_dp=copy.copy(rp)
 l_dp=copy.copy(lp)
 l_dp.cTranspose()
 r_dp.cTranspose()
 
 
 r_dp.setLabel([-17,-80,-81,51])
 l_dp.setLabel([-82,-83,52,-17])

 A2=(((rp*r_dp)*N)*Iden)
 A2.permute([52,-17,-82,-83,54,17,82,83],4)
 A2_trans=copy.copy(A2)
 A2_trans.cTranspose()
 A2=A2+A2_trans
 A2.setLabel([52,-17,-82,-83,54,17,82,83])


 A3=((l)*U*(r*r_dp))*N
 A3.permute([52,-17,-82,-83],0)
 A3p=((l_d)*U_dagger*(rp*r_d))*N
 A3p.permute([54,17,82,83],4)
 A3p.cTranspose()
 A3=A3+A3p
 A3.setLabel([52,-17,-82,-83])
 
 
 U, S, V=svd_parity(A2)

 U.cTranspose()
 V.cTranspose()
 S=inverse(S)
 
 U.setLabel([8,9,10,11,12,13,14,15])
 S.setLabel([4,5,6,7,8,9,10,11])
 V.setLabel([0,1,2,3,4,5,6,7])


 A2_inv=V*S*U
 A2_inv.permute([0,1,2,3,12,13,14,15],4)
 A2_inv.setLabel([54,17,82,83,52,-17,-82,-83])




 A=A3*A2_inv
 A.setLabel([54,17,82,83])
 A.permute([54,17,82,83],2)

 lf=copy.copy(A)

 return lf



def Do_optimization_grad(N_u, l_u, r_u, l_up, r_up, U):
  Es=Dis_fQR(N_u, l_u, r_u, l_up, r_up, U)
  Ef=0
  E2_val=0
  r_u_first=copy.copy(r_up)
  l_u_first=copy.copy(l_up)
  ##print '\n', '\n', '\n', '\n'
  Gamma=1.0
  E_previous=0
  count=0
  for i in xrange(150):
   count+=1
   E1_val=Dis_fQR(N_u, l_u, r_u, l_up, r_up, U)
   Ef=E1_val

   #print 'E=', E1_val, count
   D_r=Obtain_grad_r(N_u, l_u, r_u, l_up, r_up, U)
   D_l=Obtain_grad_l(N_u, l_u, r_u, l_up, r_up, U)
   D_r=D_r*(-1.00)
   D_l=D_l*(-1.00)
   
   A=D_r*D_r
   B=D_l*D_l


   Norm_Z=A[0]+B[0]
   
   #print 'Norm', Norm_Z
   if (E1_val<E_previous) or (i is 0):
    if (abs(E1_val) > 1.0e-10):
     if abs((E_previous-E1_val)/E1_val) < 1.0e-15:
      #print 'Differnance Satisfied!', E_previous, E1_val, abs((E_previous-E1_val)/E1_val), i
      break
     else: 
      if abs((E_previous-E1_val)) < 1.0e-15:
       #print 'Differnance Satisfied!', E_previous, E1_val, abs((E_previous-E1_val)), i
       break
      
   E_previous=E1_val
   
   if Norm_Z < 1.0e-16:
    #print 'Break Norm=', Norm_Z
    break
   Break_loop=1
   Gamma=1.0
   while Break_loop is 1:
    count+=1

    r_tem=r_up+(2.00)*Gamma*D_r
    l_tem=l_up+(2.00)*Gamma*D_l
    E2_val=Dis_fQR(N_u, l_u, r_u, l_tem, r_tem, U)
    if abs((0.5)*Norm_Z*Gamma) > 1.0e+15 or  abs(Gamma)>1.0e+15 :
     #print "break", E1_val, E2_val, Gamma
     Gamma=0
     break

    if E1_val-E2_val >=(Norm_Z*Gamma):
     Gamma*=2.00
    else:
     Break_loop=0
   
   Break_loop=1
   while Break_loop is 1:
    count+=1
    r_tem=r_up+(1.00)*Gamma*D_r
    l_tem=l_up+(1.00)*Gamma*D_l
    E2_val=Dis_fQR(N_u, l_u, r_u, l_tem, r_tem, U)
    if abs((0.5)*Norm_Z*Gamma) <1.0e-15 or  abs(E1_val-E2_val)<1.0e-15 or abs(Gamma)<1.0e-15 :
     #print "break", E1_val, E2_val, Gamma
     break
     
    if E1_val-E2_val < (0.50)*Norm_Z*Gamma:
     Gamma*=0.5
    else:
     Break_loop=0


   r_up=r_up+(1.00)*Gamma*D_r
   l_up=l_up+(1.00)*Gamma*D_l

  if( Ef > Es):
   #print 'SD method, Fail, f<s', Ef, Es 
   r_up=r_u_first
   l_up=l_u_first

  return  l_up, r_up


def Do_optimization_grad_1(N_u, l_u, r_u, l_up, r_up, U):
  Es=Dis_fQR_1(N_u, l_u, r_u, l_up, r_up, U)
  Ef=0
  E2_val=0
  r_u_first=copy.copy(r_up)
  l_u_first=copy.copy(l_up)
  ##print '\n', '\n'
  Gamma=1.0
  E_previous=0
  count=0
  for i in xrange(150):
   count+=1
   E1_val=Dis_fQR_1(N_u, l_u, r_u, l_up, r_up, U)
   Ef=E1_val

   #print 'E=', E1_val, count
   D_r=Obtain_grad_r_1(N_u, l_u, r_u, l_up, r_up, U)
   D_l=Obtain_grad_l_1(N_u, l_u, r_u, l_up, r_up, U)
   D_r=D_r*(-1.00)
   D_l=D_l*(-1.00)
   
   A=D_r*D_r
   B=D_l*D_l


   Norm_Z=A[0]+B[0]
   
   #print 'Norm', Norm_Z
   if (E1_val<E_previous) or (i is 0):
    if (abs(E1_val) > 1.0e-10):
     if abs((E_previous-E1_val)/E1_val) < 1.0e-15:
      #print 'Differnance Satisfied!', E_previous, E1_val, abs((E_previous-E1_val)/E1_val), i
      break
     else: 
      if abs((E_previous-E1_val)) < 1.0e-15:
       #print 'Differnance Satisfied!', E_previous, E1_val, abs((E_previous-E1_val)), i
       break
      
   E_previous=E1_val
   
   if Norm_Z < 1.0e-16:
    #print 'Break Norm=', Norm_Z
    break
   Break_loop=1
   Gamma=1.0
   while Break_loop is 1:
    count+=1

    r_tem=r_up+(2.00)*Gamma*D_r
    l_tem=l_up+(2.00)*Gamma*D_l
    E2_val=Dis_fQR_1(N_u, l_u, r_u, l_tem, r_tem, U)
    if abs((0.5)*Norm_Z*Gamma) > 1.0e+15 or  abs(Gamma)>1.0e+15 :
     #print "break", E1_val, E2_val, Gamma
     Gamma=0
     break

    if E1_val-E2_val >=(Norm_Z*Gamma):
     Gamma*=2.00
    else:
     Break_loop=0
   
   Break_loop=1
   while Break_loop is 1:
    count+=1
    r_tem=r_up+(1.00)*Gamma*D_r
    l_tem=l_up+(1.00)*Gamma*D_l
    E2_val=Dis_fQR_1(N_u, l_u, r_u, l_tem, r_tem, U)
    if abs((0.5)*Norm_Z*Gamma) <1.0e-15 or  abs(E1_val-E2_val)<1.0e-15 or abs(Gamma)<1.0e-15 :
     #print "break", E1_val, E2_val, Gamma
     break
     
    if E1_val-E2_val < (0.50)*Norm_Z*Gamma:
     Gamma*=0.5
    else:
     Break_loop=0


   r_up=r_up+(1.00)*Gamma*D_r
   l_up=l_up+(1.00)*Gamma*D_l

  if( Ef > Es):
   #print 'SD method, Fail, f<s', Ef, Es 
   r_up=r_u_first
   l_up=l_u_first


  return  l_up, r_up





def svd_init(l_up,r_up,H):
 ##print l_up.#printDiagram(),  r_up.#printDiagram()
 H.setLabel([51,52,53,54])
 D_dim=int(r_up.bond(3).dim())
 ##print D_dim
 
 Teta=r_up*l_up*H
 Teta.permute([82,83,51,80,81,52],3)
 
 U, V, s=setTruncation(Teta,D_dim)
 U.setLabel([82,83,53,17])
 V.setLabel([18,80,81,54])
 s.setLabel([17,18])
 U=U*s
 U.permute([82,83,53,18],3)
 V.permute([54,18,80,81],2)

 ##print U.#printDiagram(), V.#printDiagram()

 return V, U

def svd_init_1(l_up,r_up,H):
 ##print l_up.#printDiagram(),  r_up.#printDiagram()
 H.setLabel([51,52,53,54])
 D_dim=int(r_up.bond(3).dim())
 ##print D_dim
 
 Teta=r_up*l_up*H
 Teta.permute([80,81,51,82,83,52],3)
 
 U, V, s=setTruncation(Teta,D_dim)
 U.setLabel([80,81,53,16])
 V.setLabel([17,82,83,54])
 s.setLabel([16,17])
 U=U*s
 U.permute([80,81,53,17],3)
 V.permute([54,17,82,83],2)

 ##print U.#printDiagram(), V.#printDiagram()

 return V, U





def equall_dis(a_up, b_up):

 A=copy.copy(b_up)
 A.setLabel([54,18,20,6,4])
 A.permute([18,20,6,4,54],1)
 
 l,qq=lq_parity1(A) 
 
 l.setLabel([18,80])
 qq.setLabel([80,20,6,4,54])
 


 A=copy.copy(a_up)
 A.setLabel([55,16,17,18,2])
 A.permute([2,16,17,55,18],4)
 
 
 
 q,r=qr_parity1(A) 
 
 
 q.setLabel([2,16,17,55,81])
 r.setLabel([81,18])


 Teta=l*r
 Teta.permute([81,80],1)
 U,s,V=svd_parity2(Teta)

 U.setLabel([81,18])
 s.setLabel([18,-18])
 V.setLabel([-18,80])

 s=Sqrt(s)
 
 U=U*s
 V=s*V

 U.permute([81,-18],1)
 U.setLabel([81,18])
 V.permute([18,80],1)
 
 b_up=qq*V
 a_up=q*U
 b_up.permute([54,18,20,6,4],3)
 a_up.permute([55,16,17,18,2],3)
 
 return a_up,b_up



def equall_dis_1(c_up,a_up):

 A=copy.copy(a_up)
 A.setLabel([55,16,17,18,2])
 A.permute([17,55,16,18,2],1)
 
 l,qq=lq_parity1(A) 
 
 l.setLabel([17,80])
 qq.setLabel([80,55,16,18,2])
 



 A=copy.copy(c_up)
 A.setLabel([53,14,12,19,17])
 A.permute([53,14,12,19,17],4)
 
 
 
 q,r=qr_parity1(A) 
 
 
 q.setLabel([53,14,12,19,81])
 r.setLabel([81,17])

 Teta=l*r
 Teta.permute([81,80],1)
 U,s,V=svd_parity2(Teta)

 U.setLabel([81,17])
 s.setLabel([17,-17])
 V.setLabel([-17,80])

 s=Sqrt(s)
 
 U=U*s
 V=s*V

 U.permute([81,-17],1)
 U.setLabel([81,17])
 V.permute([17,80],1)
 
 a_up=qq*V
 c_up=q*U
 c_up.permute([53,14,12,19,17],3)
 a_up.permute([55,16,17,18,2],3)



 
 
 return c_up, a_up





 





















def svd_parity(theta):

    bd1=uni10.Bond(uni10.BD_IN,theta.bond(4).Qlist())
    bd2=uni10.Bond(uni10.BD_IN,theta.bond(5).Qlist())
    bd3=uni10.Bond(uni10.BD_IN,theta.bond(6).Qlist())
    bd4=uni10.Bond(uni10.BD_IN,theta.bond(7).Qlist())

    GA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0),theta.bond(1),theta.bond(2),theta.bond(3),theta.bond(4),theta.bond(5),theta.bond(6),theta.bond(7)])
    LA=uni10.UniTensor(uni10.CTYPE,[bd1,bd2,bd3,bd4,theta.bond(4),theta.bond(5),theta.bond(6),theta.bond(7)])
    GB=uni10.UniTensor(uni10.CTYPE,[bd1,bd2,bd3,bd4,theta.bond(4),theta.bond(5),theta.bond(6),theta.bond(7)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])
        GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB

def inverse(Landa2):
 invLanda2=uni10.UniTensor(uni10.CTYPE,Landa2.bond())
 blk_qnums=Landa2.blockQnum()
 for qnum in blk_qnums:
  D=int(Landa2.getBlock(qnum).row())
  D1=int(Landa2.getBlock(qnum).col())
  invL2 = uni10.CMatrix(D, D1,True)
  invLt = uni10.CMatrix(D, D1,True)
  invLt=Landa2.getBlock(qnum,True)
  ##print invLt[0], invLt[1], invLt[2], invLt[3]
  for i in xrange(D):
      invL2[i] = 0 if ((invLt[i].real) < 1.0e-9) else (1.00 / (invLt[i].real))

  invLanda2.putBlock(qnum,invL2)
 return invLanda2

def svd_parity2(theta):

    LA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0), theta.bond(1)])
    GA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0), theta.bond(1)])
    GB=uni10.UniTensor(uni10.CTYPE,[theta.bond(0), theta.bond(1)])
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
    for qnum in blk_qnums:
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0])
        GB.putBlock(qnum, svd[2])
        LA.putBlock(qnum, svd[1])
#    #print LA
    return GA, LA,GB

def   Sqrt(Landa):
  Landa_cp=copy.copy(Landa)
  blk_qnums=Landa.blockQnum()
  for qnum in blk_qnums:
   D=int(Landa_cp.getBlock(qnum).col())
   Landa_cpm=Landa_cp.getBlock(qnum,True)
   Landam=Landa_cp.getBlock(qnum,True)
   ##print Landa_cpm[0], Landa_cpm[1], Landa_cpm[2], Landa_cpm[3]
   for i in xrange(D):
      if abs(Landam[i]) > 1.0e-12:
       Landa_cpm[i]=Landam[i]**(1.00/2.00)
      else:
       Landa_cpm[i]=0
   Landa_cp.putBlock(qnum,Landa_cpm)
  return Landa_cp 

def lq_parity1(theta):
    #bd1=copy.copy(theta.bond(0))
    #bd1.change(uni10.BD_OUT)
    bd1=uni10.Bond(uni10.BD_OUT,theta.bond(0).Qlist())

    
    LA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0),bd1])
    GA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0),theta.bond(1),theta.bond(2),theta.bond(3),theta.bond(4)])
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).lq()
        GA.putBlock(qnum, svds[qnum][1])
        LA.putBlock(qnum, svds[qnum][0])

#    #print LA
    return  LA, GA
    
def qr_parity1(theta):

    #bd1=copy.copy(theta.bond(3))
    #bd1.change(uni10.BD_IN)
    bd1=uni10.Bond(uni10.BD_IN,theta.bond(4).Qlist())

    GA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0),theta.bond(1),theta.bond(2),theta.bond(3),theta.bond(4)])
    LA=uni10.UniTensor(uni10.CTYPE,[bd1, theta.bond(4)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).qr()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])

#    #print LA
    return GA, LA
    
    
def lq_parity(theta):
#    bd1=copy.copy(theta.bond(0))
#    bd2=copy.copy(theta.bond(1))
#    bd1.change(uni10.BD_OUT)
#    bd2.change(uni10.BD_OUT)
    bd1=uni10.Bond(uni10.BD_OUT,theta.bond(0).Qlist())
    bd2=uni10.Bond(uni10.BD_OUT,theta.bond(1).Qlist())    
    
    LA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0),theta.bond(1),bd1,bd2])
    GA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0),theta.bond(1),theta.bond(2),theta.bond(3),theta.bond(4)])
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).lq()
        GA.putBlock(qnum, svds[qnum][1])
        LA.putBlock(qnum, svds[qnum][0])

#    #print LA
    return  LA, GA





 
def qr_parity(theta):

#    bd1=copy.copy(theta.bond(3))
#    bd2=copy.copy(theta.bond(4))
#    bd1.change(uni10.BD_IN)
#    bd2.change(uni10.BD_IN)
    
    bd1=uni10.Bond(uni10.BD_IN,theta.bond(3).Qlist())
    bd2=uni10.Bond(uni10.BD_IN,theta.bond(4).Qlist())
    
    GA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0),theta.bond(1),theta.bond(2),theta.bond(3),theta.bond(4)])
    LA=uni10.UniTensor(uni10.CTYPE,[bd1,bd2, theta.bond(3),theta.bond(4)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).qr()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])

#    #print LA
    return GA, LA


def sqrt_general(N2):
  N_init=copy.copy(N2)
  blk_qnums = N2.blockQnum()
  for qnum in blk_qnums:
   M=N2.getBlock(qnum)
   eig=M.eigh()
   
   e=Sqrt_mat(eig[0])
   U_trans=copy.copy(eig[1])
   U_trans.cTranspose()
   M=U_trans*e*eig[1]
   N_init.putBlock(qnum,M)
  return N_init
def Sqrt_mat(e):
 d=int(e.row())
 
 for q in xrange(d):
   ##print e[q] 
   if e[q] > 0:  
    e[q]=((e[q])**(1.00/2.00))
   else:  
    e[q]=0.0 
 return e  
 
def N_Positiv(N):
 N.setLabel([80,81,-82,-83,-80,-81,82,83])
 N.permute([-82,-83,-80,-81, 82,83,80,81], 4)
 N1=copy.copy(N)
 N1.cTranspose()
 N=(N+N1)*(1.00/2.00)
 N1=copy.copy(N)
 N1.setLabel([82,83,80,81,0,1,2,3])
 N=N*N1
 N.permute([-82,-83,-80,-81,0,1,2,3],4)
 N_final=sqrt_general(N)
 N_final.setLabel([-82,-83,-80,-81,82,83,80,81])
 N_final.permute([80,81,-82,-83,-80,-81,82,83], 4)
 return N_final              
def N_Positiv_1(N):
 N.setLabel([-80,-81,82,83,80,81,-82,-83])
 N.permute([-80,-81,-82,-83,80,81,82,83], 4)
 N1=copy.copy(N)
 N1.cTranspose()
 N=(N+N1)*(1.00/2.00)
 N1=copy.copy(N)
 N1.setLabel( [80,81,82,83,0,1,2,3 ] )
 N=N*N1
 N.permute([-80,-81,-82,-83,0,1,2,3],4)
 N_final=sqrt_general(N)
 N_final.setLabel([-80,-81,-82,-83,80,81,82,83])
 N_final.permute([-80,-81,82,83,80,81,-82,-83], 4)
 return N_final    
 
 
 
def setTruncation(theta, chi):
    LA=uni10.UniTensor(uni10.CTYPE,theta.bond())
    GA=uni10.UniTensor(uni10.CTYPE,theta.bond())
    GB=uni10.UniTensor(uni10.CTYPE,theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        M_tem=theta.getBlock(qnum)
        svds[qnum] = M_tem.svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_merge(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    ##print bdi_mid
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0), theta.bond(1),theta.bond(2), bdo_mid])
    GB.assign([bdi_mid, theta.bond(3), theta.bond(4),theta.bond(5)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
    return GA, GB, LA

def sv_merge(svs, bidxs, bidx, sv_mat, chi, len_qn):
    if(len(svs)):
        length = len(svs) + sv_mat.elemNum()
        length = length if length < chi else chi
        ori_svs = svs
        ori_bidxs = bidxs
        svs = [0] * length
        bidxs = [0] * length
        svs = []
        bidxs = []
        cnt  = 0
        cur1 = 0
        cur2 = 0
        while cnt < length:
            if(cur1 < len(ori_svs)) and cur2 < sv_mat.elemNum():
                if ori_svs[cur1] >= sv_mat[cur2]:
                    if (ori_svs[cur1] > 1.0e-12):
                     svs.append(ori_svs[cur1]) 
                     bidxs.append(ori_bidxs[cur1])
                    cur1 += 1
                else:
                    if (sv_mat[cur2] > 1.0e-12):
                     svs.append( sv_mat[cur2])
                     bidxs.append(bidx) 
                    cur2 += 1
            elif cur2 < sv_mat.elemNum() :
                for i in xrange(cnt, length):
                    if (sv_mat[cur2] > 1.0e-12):
                     svs.append(sv_mat[cur2]) 
                     bidxs.append(bidx) 
                    cur2 += 1
                break
            else:
                for i in xrange(cur1, len(ori_svs)):
                 svs.append(ori_svs[i])
                 bidxs.append(ori_bidxs[i]) 
                break
            cnt += 1
    else:
       if (len_qn is 1):
        bidxs = [bidx] * chi  
        svs = [sv_mat[i] for i in xrange(chi)]
       elif (sv_mat[0] > 1.0e-12):
        bidxs = [bidx] * sv_mat.elemNum()
        svs = [sv_mat[i] for i in xrange(sv_mat.elemNum())]
       else: bidxs = [bidx];  svs = [sv_mat[0]];  
    return svs, bidxs
            

def svd_parity4(theta):

    bd1=uni10.Bond(uni10.BD_IN,theta.bond(5).Qlist())
    bd2=uni10.Bond(uni10.BD_IN,theta.bond(6).Qlist())
    bd3=uni10.Bond(uni10.BD_IN,theta.bond(7).Qlist())
    bd4=uni10.Bond(uni10.BD_IN,theta.bond(8).Qlist())
    bd5=uni10.Bond(uni10.BD_IN,theta.bond(9).Qlist())

    GA=uni10.UniTensor(uni10.CTYPE,[theta.bond(0),theta.bond(1),theta.bond(2),theta.bond(3),theta.bond(4),theta.bond(5),theta.bond(6),theta.bond(7),theta.bond(8),theta.bond(9)])
    LA=uni10.UniTensor(uni10.CTYPE,[bd1,bd2,bd3,bd4,bd5,theta.bond(5),theta.bond(6),theta.bond(7),theta.bond(8),theta.bond(9)])
    GB=uni10.UniTensor(uni10.CTYPE,[bd1,bd2,bd3,bd4,bd5,theta.bond(5),theta.bond(6),theta.bond(7),theta.bond(8),theta.bond(9)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])
        GB.putBlock(qnum, svds[qnum][2])

#    #print LA
    return GA, LA, GB


######@profile
def Do_optimization_svd(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, a_up, b_up, U,N_grad, Opt_method,Inv_method,N_svd,Gauge,check_step):

 a_up_first=copy.copy(a_up) 
 b_up_first=copy.copy(b_up)

 a_up_first1=copy.copy(a_up) 
 b_up_first1=copy.copy(b_up)


 checking_val=0

 Distance_val=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_up,b_up)
 #print Distance_val
 Res=10
 Res1=Distance_val
 count=0
 test=0
 for q in xrange(N_svd[0]):
  t0=time.time()

  if check_step is "on":
   Dis1=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_up,b_up)
   a_up=opt_a(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,b_u,a_up,b_up,U,Inv_method,Gauge)
   Dis2=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_up,b_up)
   if Dis2 <= Dis1:
      a_up_first=copy.copy(a_up) 
   else: 
      #print "Fail0:a", Dis1, Dis2, Dis2 <= Dis1  
      test=1; break;
      a_up=copy.copy(a_up_first) 
   Dis1=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_up,b_up)
   b_up=opt_b(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,b_u,a_up,b_up,U,Inv_method,Gauge)
   Dis2=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_up,b_up)
   if Dis2 <= Dis1:
      b_up_first=copy.copy(b_up) 
   else: 
      #print "Fail0:b", Dis1, Dis2, Dis2 <= Dis1
      test=1; break;  
      b_up=copy.copy(b_up_first) 
  elif check_step is "off":
   a_up=opt_a(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,b_u,a_up,b_up,U,Inv_method,Gauge)
   b_up=opt_b(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,b_u,a_up,b_up,U,Inv_method,Gauge)

  Distance_val=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_up,b_up)
  #print 'Dis_svd1=', Distance_val, abs(Res1-Res) / abs(Res), q, time.time() - t0
#  #print test

  Res=Res1
  Res1=Distance_val
  ##print Res, Res1, Res1 < Res 
  #test=q

  count+=1
  if count > 100: print 'Num_Opt > 30'; break;
  if abs(Res) > 1.00e-10:
   if (abs(Distance_val) < 1.00e-8) or ((abs(Res1-Res) / abs(Res)) < N_svd[1]): 
    print 'break, Dis', Distance_val, (abs(Res1-Res) / abs(Res)), count
    break
  else:
    if (abs(Distance_val) < 1.00e-8) or (  abs(Res1-Res) < 1.00e-11  ): 
     print 'break, Dis', Distance_val[0], abs(Res1-Res)
     break

  if Res1 < Res:
    a_up_first=copy.copy(a_up) 
    b_up_first=copy.copy(b_up)
  else:
    a_up=copy.copy(a_up_first) 
    b_up=copy.copy(b_up_first)
    test=q+1
    break
 

 #print test
# if test != 0:
#  a_up=copy.copy(a_up_first1)
#  b_up=copy.copy(b_up_first1)
#  a_up, b_up, c_up, d_up=Do_optimization_Grad(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, c_u, d_u, a_up, b_up, c_up, d_up, U,N_grad, Opt_method,Gauge)
 return a_up, b_up




#####@profile

def Do_optimization_svd1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, c_u, a_up, c_up, U,N_grad, Opt_method, Inv_method, N_svd,Gauge, check_step):

 a_up_first=copy.copy(a_up) 
 c_up_first=copy.copy(c_up)

 a_up_first1=copy.copy(a_up) 
 c_up_first1=copy.copy(c_up)


 checking_val=0

 Distance_val=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u, a_u, c_up, a_up)
 #print Distance_val
 Res=10
 Res1=Distance_val
 count=0
 test=0
 for q in xrange(N_svd[0]):
  t0=time.time()

  if check_step is "on":
   Dis1=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u, a_u, c_up, a_up)
   a_up=opt_a1(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,c_u,a_up,c_up,U,Inv_method,Gauge)
   Dis2=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u, a_u, c_up, a_up)
   if Dis2 <= Dis1:
      a_up_first=copy.copy(a_up) 
   else: 
      #print "Fail0:a", Dis1, Dis2, Dis2 <= Dis1  
      test=1; break;
      a_up=copy.copy(a_up_first) 
   Dis1=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u, a_u, c_up, a_up)
   b_up=opt_c(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,c_u,a_up,c_up,U,Inv_method,Gauge)
   Dis2=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u, a_u, c_up, a_up)
   if Dis2 <= Dis1:
      c_up_first=copy.copy(c_up) 
   else: 
      #print "Fail0:b", Dis1, Dis2, Dis2 <= Dis1
      test=1; break;  
      c_up=copy.copy(c_up_first) 
  elif check_step is "off":
   a_up=opt_a1(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,c_u,a_up,c_up,U,Inv_method,Gauge)
   c_up=opt_c(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,c_u,a_up,c_up,U,Inv_method,Gauge)

  Distance_val=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u, a_u, c_up, a_up)
  #print 'Dis_svd2=', Distance_val, abs(Res1-Res) / abs(Res), q, time.time() - t0
#  #print test

  Res=Res1
  Res1=Distance_val
  ##print Res, Res1, Res1 < Res 
  #test=q

  count+=1
  if count > 100: print 'Num_Opt > 30'; break;
  if abs(Res) > 1.00e-10:
   if (abs(Distance_val) < 1.00e-8) or ((abs(Res1-Res) / abs(Res)) < N_svd[1]): 
    print 'break, Dis', Distance_val, (abs(Res1-Res) / abs(Res)), count
    break
  else:
    if (abs(Distance_val) < 1.00e-8) or (  abs(Res1-Res) < 1.00e-11  ): 
     print 'break, Dis', Distance_val[0], abs(Res1-Res)
     break

  if Res1 < Res:
    a_up_first=copy.copy(a_up) 
    c_up_first=copy.copy(c_up)
  else:
    a_up=copy.copy(a_up_first) 
    c_up=copy.copy(c_up_first)
    test=q+1
    break
 

#  #print test
#  if test != 0:
#   a_up=copy.copy(a_up_first1)
#   b_up=copy.copy(b_up_first1)
#   a_up, b_up, c_up, d_up=Do_optimization_Grad(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, c_u, d_u, a_up, b_up, c_up, d_up, U,N_grad, Opt_method,Gauge)
 return a_up, c_up

#####@profile
def opt_a1(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,c_u,a_up,c_up,U,Inv_method,Gauge):
 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 b.setLabel([18,-18,20,-20,6,-6,4,-4])

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 Iden1=copy.copy(Iden)
 Iden1.setLabel([53,-54,51,52])
 Idena=Iden*Iden1
 Idena.permute([-54,54],1)
 Idena.identity()

 H1=copy.copy(U)
 H1.cTranspose()
 H1.setLabel([-20,-40,51,52])

 U_dagger=copy.copy(H1)
 U_dagger.setLabel([51,52,53,54])

 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])
 U_dagger.setLabel([51,52,53,54])
 
 

 a_up.setLabel([54,16,17,18,2])
 a_dp=copy.copy(a_up)
 a_dp.cTranspose()
 a_dp.setLabel([-18,-2,52,-16,-17])

 a_u.setLabel([54,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,52,-16,-17])
  
 c_up.setLabel([53,14,12,19,17])
 c_dp=copy.copy(c_up)
 c_dp.cTranspose()
 c_dp.setLabel([-19,-17,51,-14,-12])
 
 c_u.setLabel([53,14,12,19,17])
 c_d=copy.copy(c_u)
 c_d.cTranspose()
 c_d.setLabel([-19,-17,51,-14,-12])
 
###########################################---a---########################
 E_b=(E2*E3)*(b)
 E_d=((E4*E5)*d)


 c_dp.setLabel([-19,-17,53,-14,-12])


 A=(((((E7*E6)*(c_up*c_dp))*E_d)*E_b)*(E1*E8))*(Idena)
 A.permute([-54,-16,-17,-18,-2,54,16,17,18,2],5)

 if Gauge is "Fixed": 
  A1=copy.copy(A)
  A1.cTranspose()
  A=A+A1
  
 c_dp.setLabel([-19,-17,51,-14,-12])
 Ap=((((((E7*E6)*(c_u*c_dp))*E_d)*E_b)*U)*(E1*E8)*a_u)
 Ap.permute([52,-16,-17,-18,-2],5)
 
 if Gauge is "Fixed": 
  Ap1=((((((E7*E6)*(c_up*c_d))*E_d)*E_b)*U_dagger)*(E1*E8)*a_d)
  Ap1.permute([54,16,17,18,2],0)
  Ap1.cTranspose()
  Ap=Ap+Ap1
 
 if Inv_method is "SVD":
   #A=N_Positiv(A)
   U, S, V=svd_parity4(A)
   U.cTranspose()
   V.cTranspose()
   S=inverse(S)
   
   U.setLabel([10,11,12,13,14,15,16,17,18,19])
   S.setLabel([5,6,7,8,9,10,11,12,13,14])
   V.setLabel([0,1,2,3,4,5,6,7,8,9])

   A_inv=V*S*U
   A_inv.permute([0,1,2,3,4,15,16,17,18,19],5)
   A_inv.setLabel([54,16,17,18,2,52,-16,-17,-18,-2])
   A=A_inv*Ap
   A.permute([54,16,17,18,2],3)

 elif Inv_method is "CG":
   A=solve_linear_eq(A,Ap)
   A.setLabel([54,16,17,18,2])
   A.permute([54,16,17,18,2],3)
 return A

#####@profile
def opt_c(E1,E2,E3,E4,E5,E6,E7,E8,a,b,c,d,a_u,c_u,a_up,c_up,U,Inv_method,Gauge):
 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 b.setLabel([18,-18,20,-20,6,-6,4,-4])

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 Iden1=copy.copy(Iden)
 Iden1.setLabel([-53,54,51,52])
 Idena=Iden*Iden1
 Idena.permute([-53,53],1)
 Idena.identity()

 H1=copy.copy(U)
 H1.cTranspose()
 H1.setLabel([-20,-40,51,52])

 U_dagger=copy.copy(H1)
 U_dagger.setLabel([51,52,53,54])

 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])
 U_dagger.setLabel([51,52,53,54])
 
 

 a_up.setLabel([54,16,17,18,2])
 a_dp=copy.copy(a_up)
 a_dp.cTranspose()
 a_dp.setLabel([-18,-2,52,-16,-17])

 a_u.setLabel([54,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,52,-16,-17])
  
 c_up.setLabel([53,14,12,19,17])
 c_dp=copy.copy(c_up)
 c_dp.cTranspose()
 c_dp.setLabel([-19,-17,51,-14,-12])
 
 c_u.setLabel([53,14,12,19,17])
 c_d=copy.copy(c_u)
 c_d.cTranspose()
 c_d.setLabel([-19,-17,51,-14,-12])
 
###########################################---c---########################
 E_b=(E2*E3)*(b)
 E_d=((E4*E5)*d)


 a_dp.setLabel([-18,-2,54,-16,-17])



 A=(((E_d*E_b)*((E1*E8)*(a_up*a_dp)))*(E7*E6))*Idena
 A.permute([-53,-14,-12,-19,-17,53,14,12,19,17],5)

 if Gauge is "Fixed": 
  A1=copy.copy(A)
  A1.cTranspose()
  A=A+A1
  
 a_dp.setLabel([-18,-2,52,-16,-17])
 
 Ap=((((E_d*E_b)*((E1*E8)*(a_u*a_dp)))*U)*(E7*E6)*c_u)
 ##print Ap.#printDiagram()
 Ap.permute([51,-14,-12,-19,-17],5)
 
 if Gauge is "Fixed": 
  Ap1=((((E_d*E_b)*((E1*E8)*(a_up*a_d)))*U_dagger)*(E7*E6)*c_d)
  Ap1.permute([53,14,12,19,17],0)
  Ap1.cTranspose()
  Ap=Ap+Ap1
 
 if Inv_method is "SVD":
   #A=N_Positiv(A)
   U, S, V=svd_parity4(A)
   U.cTranspose()
   V.cTranspose()
   S=inverse(S)
   
   U.setLabel([10,11,12,13,14,15,16,17,18,19])
   S.setLabel([5,6,7,8,9,10,11,12,13,14])
   V.setLabel([0,1,2,3,4,5,6,7,8,9])

   A_inv=V*S*U
   A_inv.permute([0,1,2,3,4,15,16,17,18,19],5)
   A_inv.setLabel([53,14,12,19,17,51,-14,-12,-19,-17])
   A=A_inv*Ap
   A.permute([53,14,12,19,17],3)

 elif Inv_method is "CG":
   A=solve_linear_eq(A,Ap)
   A.setLabel([53,14,12,19,17])
   A.permute([53,14,12,19,17],3)
 
 return A

######@profile
def opt_a(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, a_up, b_up, U,Inv_method,Gauge):

 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 c.setLabel([14,-14,12,-12,19,-19,17,-17])

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 Iden1=copy.copy(Iden)
 Iden1.setLabel([-53,54,51,52])
 Idena=Iden*Iden1
 Idena.permute([-53,53],1)
 Idena.identity()

 H1=copy.copy(U)
 H1.cTranspose()
 H1.setLabel([-20,-40,51,52])

 U_dagger=copy.copy(H1)
 U_dagger.setLabel([51,52,53,54])

 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])
 U_dagger.setLabel([51,52,53,54])
 
 a_up.setLabel([53,16,17,18,2])
 a_dp=copy.copy(a_up)
 a_dp.cTranspose()
 a_dp.setLabel([-18,-2,51,-16,-17])

 a_u.setLabel([53,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,51,-16,-17])
 

 b_u.setLabel([54,18,20,6,4])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([-6,-4,52,-18,-20])

 
 b_up.setLabel([54,18,20,6,4])
 b_dp=copy.copy(b_up)
 b_dp.cTranspose()
 b_dp.setLabel([-6,-4,52,-18,-20])
###########################################---a---########################
 b_up.setLabel([54,18,20,6,4])
 b_dp=copy.copy(b_up)
 b_dp.cTranspose()
 b_dp.setLabel([-6,-4,54,-18,-20])

 A=(((((E7*E6)*(c))*(((E4*E5)*d)))*((E2*E3)*(b_up*b_dp)))*((E1*E8)))*Idena
 ##print A.#printDiagram()
 A.permute([-53,-16,-17,-18,-2,53,16,17,18,2],5)

 if Gauge is "Fixed": 
  A1=copy.copy(A)
  A1.cTranspose()
  A=A+A1
 


 b_dp.setLabel([-6,-4,52,-18,-20])
 Ap=(((((E7*E6)*(c))*(((E4*E5)*d)))*(((E2*E3)*(b_u*b_dp))*U))*((E1*E8)*a_u))
 ##print Ap.#printDiagram()
 Ap.permute([51,-16,-17,-18,-2],5)
 
 if Gauge is "Fixed": 
  Ap1=(((((E7*E6)*(c))*(((E4*E5)*d)))*(((E2*E3)*(b_up*b_d))*U_dagger))*((E1*E8)*a_d))
  Ap1.permute([53,16,17,18,2],0)
  Ap1.cTranspose()
  Ap=Ap+Ap1
 
 if Inv_method is "SVD":
   #A=N_Positiv(A)
   U, S, V=svd_parity4(A)
   U.cTranspose()
   V.cTranspose()
   S=inverse(S)
   
   U.setLabel([10,11,12,13,14,15,16,17,18,19])
   S.setLabel([5,6,7,8,9,10,11,12,13,14])
   V.setLabel([0,1,2,3,4,5,6,7,8,9])

   A_inv=V*S*U
   A_inv.permute([0,1,2,3,4,15,16,17,18,19],5)
   A_inv.setLabel([53,16,17,18,2,51,-16,-17,-18,-2])
   A=A_inv*Ap
   A.permute([53,16,17,18,2],3)

 elif Inv_method is "CG":
   A=solve_linear_eq(A,Ap)
   A.setLabel([53,16,17,18,2])
   A.permute([53,16,17,18,2],3)
 
 return A


######@profile

def opt_b(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, a_up, b_up, U,Inv_method,Gauge):

 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 c.setLabel([14,-14,12,-12,19,-19,17,-17])

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 Iden1=copy.copy(Iden)
 Iden1.setLabel([53,-54,51,52])
 Idenb=Iden*Iden1
 Idenb.permute([-54,54],1)
 Idenb.identity()

 H1=copy.copy(U)
 H1.cTranspose()
 H1.setLabel([-20,-40,51,52])

 U_dagger=copy.copy(H1)
 U_dagger.setLabel([51,52,53,54])

 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])
 U_dagger.setLabel([51,52,53,54])
 
 a_up.setLabel([53,16,17,18,2])
 a_dp=copy.copy(a_up)
 a_dp.cTranspose()
 a_dp.setLabel([-18,-2,51,-16,-17])

 a_u.setLabel([53,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,51,-16,-17])
 

 b_u.setLabel([54,18,20,6,4])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([-6,-4,52,-18,-20])

 
 b_up.setLabel([54,18,20,6,4])
 b_dp=copy.copy(b_up)
 b_dp.cTranspose()
 b_dp.setLabel([-6,-4,52,-18,-20])
###########################################---a---########################
 a_up.setLabel([53,16,17,18,2])
 a_dp=copy.copy(a_up)
 a_dp.cTranspose()
 a_dp.setLabel([-18,-2,53,-16,-17])

 A=(((((E7*E6)*(c))*(((E4*E5)*d)))*(((E1*E8))*(a_up*a_dp)))*((E2*E3)))*Idenb
 ##print A.#printDiagram()
 A.permute([-54,-18,-20,-6,-4,54,18,20,6,4],5)

 if Gauge is "Fixed": 
  A1=copy.copy(A)
  A1.cTranspose()
  A=A+A1

 a_dp.setLabel([-18,-2,51,-16,-17])
 Ap=(((((E7*E6)*(c))*(((E4*E5)*d)))*(((E1*E8)*(a_u*a_dp))*U))*((E2*E3)*b_u))
 ##print Ap.#printDiagram()
 Ap.permute([52,-18,-20,-6,-4],5)
 
 if Gauge is "Fixed": 
  Ap1=(((((E7*E6)*(c))*(((E4*E5)*d)))*(((E1*E8)*(a_up*a_d))*U_dagger))*((E2*E3)*b_d))
  ##print Ap1.#printDiagram()
  Ap1.permute([54,18,20,6,4],0)
  Ap1.cTranspose()
  Ap=Ap+Ap1
 
 if Inv_method is "SVD":
   #A=N_Positiv(A)

   U, S, V=svd_parity4(A)
   U.cTranspose()
   V.cTranspose()
   S=inverse(S)
   
   U.setLabel([10,11,12,13,14,15,16,17,18,19])
   S.setLabel([5,6,7,8,9,10,11,12,13,14])
   V.setLabel([0,1,2,3,4,5,6,7,8,9])


   A_inv=V*S*U
   A_inv.permute([0,1,2,3,4,15,16,17,18,19],5)
   A_inv.setLabel([54,18,20,6,4,52,-18,-20,-6,-4])

   A=A_inv*Ap
   A.permute([54,18,20,6,4],3)
 elif Inv_method is "CG":
   A=solve_linear_eq(A,Ap)
   A.setLabel([54,18,20,6,4])
   A.permute([54,18,20,6,4],3)
 
 return A

######@profile
def Do_optimization_Grad_com(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, a_up, b_up, U, N_grad, Opt_method, Gauge):


  a_um=copy.copy(a_up) 
  b_um=copy.copy(b_up) 

  time_val=0
  Es=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_up,b_up)
  Ef=0
  E2_val=0
  ##print '\n', '\n', '\n', '\n'
  Gamma=1.0
  E_previous=1.e+18
  count=0
  D_list=[0+0j]*2
  H_list=[0+0j]*2
  H_a=0+0j; H_b=0+0j;
  for i in xrange(N_grad):
   count+=1
   t0=time.time()

   E1_val=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_up,b_up)
   Ef=E1_val
   #print 'E1=', E1_val, abs((E_previous-E1_val)/E1_val), i, count, time_val


   D_a,D_b=Obtain_grad_four(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, a_up, b_up, U,Gauge)
   D_a=(-1.0)*D_a
   D_b=(-1.0)*D_b

   if i is 0:
    H_a=D_a
    H_b=D_b
   else:
    Z_a=D_a+(-1.0)*D_list[0]
    Z_b=D_b+(-1.0)*D_list[1]
    Z_b_tr=copy.copy(Z_b)
    Z_b_tr.cTranspose()
    Z_a_tr=copy.copy(Z_a)
    Z_a_tr.cTranspose()
    A=Z_a_tr*D_a
    B=Z_b_tr*D_b
    D_list0=copy.copy(D_list[0])
    D_list0.cTranspose()
    D_list1=copy.copy(D_list[1])
    D_list1.cTranspose()
    A1=D_list0*D_list[0]
    A2=D_list1*D_list[1]
    Gamma_grad=(A[0]+B[0]) / (A1[0]+A2[0])
    if Opt_method is 'ST':Gamma_grad=0+0j;
    ##print abs(Gamma_grad)
    H_a=D_a+(abs(Gamma_grad))*H_list[0]
    H_b=D_b+(abs(Gamma_grad))*H_list[1]

#    A=D_a*D_list[0]
#    B=D_b*D_list[1]
#    C=D_c*D_list[2]
#    D=D_d*D_list[3]
#    check=A[0]+B[0]+C[0]+D[0] 
#    #print "check", check 

   D_list[0]=copy.copy(D_a)
   D_list[1]=copy.copy(D_b)

   H_list[0]=copy.copy(H_a)
   H_list[1]=copy.copy(H_b)

   H_b_tr=copy.copy(H_b)
   H_b_tr.cTranspose()
   H_a_tr=copy.copy(H_a)
   H_a_tr.cTranspose()

   A=D_a*H_a_tr
   B=D_b*H_b_tr
   
   Norm_Z=(0.5)*abs(A[0]+B[0])
   
   ##print 'Norm', Norm_Z
   if (E1_val<E_previous) or (i is 0):
    if (abs(E1_val) > 1.0e-10):
     if abs((E_previous-E1_val)/E1_val) < 1.0e-12:
      #print 'Differnance Satisfied!', E_previous, E1_val, abs((E_previous-E1_val)/E1_val), i
      break
     else: 
      if abs((E_previous-E1_val)) < 1.0e-15:
       #print 'Differnance Satisfied!', E_previous, E1_val, abs((E_previous-E1_val)), i
       break

   ##print E1_val>E_previous  
   if (E1_val>E_previous):
    #print "break, not satisfied", E1_val, E_previous
    a_up=copy.copy(a_um) 
    b_up=copy.copy(b_um) 
    break
   else:
    a_um=copy.copy(a_up) 
    b_um=copy.copy(b_up) 

   E_previous=E1_val
   
   if abs(Norm_Z) < 1.0e-11:
    #print 'Break Norm=', Norm_Z
    break
   Break_loop=1
   if (i%15)==0: 
    Gamma=1
   else: 
    if Gamma >= 1: Gamma*=(1.00/100)
    if Gamma < 1: Gamma*=100
   #Gamma=1
   ##print "Gamma", Gamma
   while Break_loop is 1:
    count+=1
    a_ut=a_up+(2.00)*Gamma*H_a
    b_ut=b_up+(2.00)*Gamma*H_b
    E2_val=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_ut,b_ut)
    if abs((0.5)*Norm_Z*Gamma) > 1.0e+12 or  abs(Gamma)>1.0e+12 :
     #print "break1", E1_val, abs((0.5)*Norm_Z*Gamma), E2_val, Gamma
     Gamma=1
     break
    if E1_val-E2_val >=(Norm_Z*Gamma):
     Gamma*=2.00
    else:
     Break_loop=0

   Break_loop=1
   while Break_loop is 1:
    count+=1
    a_ut=a_up+(1.00)*Gamma*H_a
    b_ut=b_up+(1.00)*Gamma*H_b
    E2_val=Dis_f(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,a_u,b_u,a_ut,b_ut)
    if abs((0.5)*Norm_Z*Gamma) <1.0e-16 or  (abs((E1_val-E2_val)/E2_val))<1.0e-16 or abs(Gamma)<1.0e-16 :
     #print "break2", E1_val, E2_val, Gamma, abs((0.5)*Norm_Z*Gamma), (abs((E1_val-E2_val)/E2_val))
     break
     
    if E1_val-E2_val < (0.50)*abs(Norm_Z)*Gamma:
     Gamma*=0.5
    else:
     Break_loop=0


   a_up=a_up+(1.00)*Gamma*H_a
   b_up=b_up+(1.00)*Gamma*H_b
   time_val=time.time() - t0

  return a_up, b_up
####@profile
def Do_optimization_Grad_com1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, c_u, a_up, c_up, U, N_grad, Opt_method, Gauge):

  a_um=copy.copy(a_up)
  c_um=copy.copy(c_up)

  time_val=0
  Es=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u,a_u,c_up,a_up)
  Ef=0
  E2_val=0
  ##print '\n', '\n', '\n', '\n'
  Gamma=1.0
  E_previous=1.e+18
  count=0
  D_list=[0+0j]*2
  H_list=[0+0j]*2
  H_a=0+0j; H_b=0+0j;
  for i in xrange(N_grad):
   count+=1
   t0=time.time()

   E1_val=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u,a_u,c_up,a_up)
   Ef=E1_val
   #print 'E1=', E1_val, abs((E_previous-E1_val)/E1_val), i, count, time_val


   D_a, D_c=Obtain_grad_four_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, c_u, a_up, c_up, U,Gauge)
   D_a=(-1.0)*D_a
   D_c=(-1.0)*D_c

   if i is 0:
    H_a=D_a
    H_c=D_c
   else:
    Z_a=D_a+(-1.0)*D_list[0]
    Z_c=D_c+(-1.0)*D_list[1]
    Z_c_tr=copy.copy(Z_c)
    Z_c_tr.cTranspose()
    Z_a_tr=copy.copy(Z_a)
    Z_a_tr.cTranspose()
    A=Z_a_tr*D_a
    c=Z_c_tr*D_c
    D_list0=copy.copy(D_list[0])
    D_list0.cTranspose()
    D_list1=copy.copy(D_list[1])
    D_list1.cTranspose()
    A1=D_list0*D_list[0]
    A2=D_list1*D_list[1]
    Gamma_grad=(A[0]+C[0]) / (A1[0]+A2[0])
    if Opt_method is 'ST':Gamma_grad=0+0j;
    ##print abs(Gamma_grad)
    H_a=D_a+(abs(Gamma_grad))*H_list[0]
    H_c=D_c+(abs(Gamma_grad))*H_list[1]

#    A=D_a*D_list[0]
#    B=D_b*D_list[1]
#    C=D_c*D_list[2]
#    D=D_d*D_list[3]
#    check=A[0]+B[0]+C[0]+D[0] 
#    #print "check", check 

   D_list[0]=copy.copy(D_a)
   D_list[1]=copy.copy(D_c)

   H_list[0]=copy.copy(H_a)
   H_list[1]=copy.copy(H_c)

   H_c_tr=copy.copy(H_c)
   H_c_tr.cTranspose()
   H_a_tr=copy.copy(H_a)
   H_a_tr.cTranspose()

   A=D_a*H_a_tr
   C=D_c*H_c_tr
   
   Norm_Z=(0.5)*abs(A[0]+C[0])
   
   ##print 'Norm', Norm_Z
   if (E1_val<E_previous) or (i is 0):
    if (abs(E1_val) > 1.0e-10):
     if abs((E_previous-E1_val)/E1_val) < 1.0e-12:
      #print 'Differnance Satisfied!', E_previous, E1_val, abs((E_previous-E1_val)/E1_val), i
      break
     else: 
      if abs((E_previous-E1_val)) < 1.0e-15:
       #print 'Differnance Satisfied!', E_previous, E1_val, abs((E_previous-E1_val)), i
       break

   ##print E1_val>E_previous  
   if (E1_val>E_previous):
    #print "break, not satisfied", E1_val, E_previous
    a_up=copy.copy(a_um) 
    c_up=copy.copy(c_um) 
    break
   else:
    a_um=copy.copy(a_up) 
    c_um=copy.copy(c_up) 

   E_previous=E1_val
   
   if abs(Norm_Z) < 1.0e-11:
    #print 'Break Norm=', Norm_Z
    break
   Break_loop=1
   if (i%15)==0: 
    Gamma=1
   else: 
    if Gamma >= 1: Gamma*=(1.00/100)
    if Gamma < 1: Gamma*=100
   #Gamma=1
   ##print "Gamma", Gamma
   while Break_loop is 1:
    count+=1
    a_ut=a_up+(2.00)*Gamma*H_a
    c_ut=c_up+(2.00)*Gamma*H_c
    E2_val=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u,a_u,c_ut,a_ut)
    if abs((0.5)*Norm_Z*Gamma) > 1.0e+12 or  abs(Gamma)>1.0e+12 :
     #print "break1", E1_val, abs((0.5)*Norm_Z*Gamma), E2_val, Gamma
     Gamma=1
     break
    if E1_val-E2_val >=(Norm_Z*Gamma):
     Gamma*=2.00
    else:
     Break_loop=0

   Break_loop=1
   while Break_loop is 1:
    count+=1
    a_ut=a_up+(1.00)*Gamma*H_a
    c_ut=c_up+(1.00)*Gamma*H_c
    E2_val=Dis_f_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, U,c_u,a_u,c_ut,a_ut)
    if abs((0.5)*Norm_Z*Gamma) <1.0e-16 or  (abs((E1_val-E2_val)/E2_val))<1.0e-16 or abs(Gamma)<1.0e-16 :
     #print "break2", E1_val, E2_val, Gamma, abs((0.5)*Norm_Z*Gamma), (abs((E1_val-E2_val)/E2_val))
     break
     
    if E1_val-E2_val < (0.50)*abs(Norm_Z)*Gamma:
     Gamma*=0.5
    else:
     Break_loop=0

   a_up=a_up+(1.00)*Gamma*H_a
   c_up=c_up+(1.00)*Gamma*H_c
   time_val=time.time() - t0

  return a_up, c_up
####@profile
def Obtain_grad_four_1(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, c_u, a_up, c_up, U, Gauge):
 Gauge=copy.copy(Gauge)
 Gauge='Fixed'
 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 b.setLabel([18,-18,20,-20,6,-6,4,-4])

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 Iden1=copy.copy(Iden)
 Iden1.setLabel([53,-54,51,52])
 Idena=Iden*Iden1
 Idena.permute([-54,54],1)
 Idena.identity()

 H1=copy.copy(U)
 H1.cTranspose()
 H1.setLabel([-20,-40,51,52])

 U_dagger=copy.copy(H1)
 U_dagger.setLabel([51,52,53,54])

 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])
 U_dagger.setLabel([51,52,53,54])
 
 a_up.setLabel([54,16,17,18,2])
 a_dp=copy.copy(a_up)
 a_dp.cTranspose()
 a_dp.setLabel([-18,-2,52,-16,-17])

 a_u.setLabel([54,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,52,-16,-17])
  
 c_up.setLabel([53,14,12,19,17])
 c_dp=copy.copy(c_up)
 c_dp.cTranspose()
 c_dp.setLabel([-19,-17,51,-14,-12])
 
 c_u.setLabel([53,14,12,19,17])
 c_d=copy.copy(c_u)
 c_d.cTranspose()
 c_d.setLabel([-19,-17,51,-14,-12])
 
###########################################---a---########################
 E_b=(E2*E3)*(b)
 E_d=((E4*E5)*d)

 c_dp.setLabel([-19,-17,53,-14,-12])
 a_dp.setLabel([-18,-2,54,-16,-17])

 A=(((((E7*E6)*(c_up*c_dp))*E_d)*E_b))*((E1*E8)*a_dp)
 A.permute([54,16,17,18,2],3)
 D_a=A


 if Gauge is "Fixed":
  A=(((((E7*E6)*(c_up*c_dp))*E_d)*E_b))*((E1*E8)*a_up)
  A.permute([-18,-2,54,-16,-17],2)
  A.cTranspose()
  D_a=D_a+A

 A=(((((E7*E6)*(c_up*c_d))*E_d)*E_b)*U_dagger)*((E1*E8)*a_d)
 A.permute([54,16,17,18,2],3)
 D_a=D_a+(-1.0)*A

 c_dp.setLabel([-19,-17,51,-14,-12])

 if Gauge is "Fixed":
  A=(((((E7*E6)*(c_u*c_dp))*E_d)*E_b)*U_dagger)*((E1*E8)*a_u)
  A.permute([-18,-2,52,-16,-17],2)
  A.cTranspose()
  D_a=D_a+(-1.0)*A
#  D_a.cTranspose()
#  D_a.permute([55,16,17,18,2],3)


 D_a.cTranspose()
 D_a.permute([54,16,17,18,2],3)
 
#####################################----c----#############################################
 c_dp.setLabel([-19,-17,53,-14,-12])
 a_dp.setLabel([-18,-2,54,-16,-17])


 A=(((E_d*E_b)*((E1*E8)*(a_up*a_dp)))*(E7*E6)*c_dp)
 A.permute([53,14,12,19,17],3)
 D_c=A

 if Gauge is "Fixed":
  A=(((E_d*E_b)*((E1*E8)*(a_up*a_dp)))*(E7*E6)*c_up)
  A.permute([-19,-17,53,-14,-12],2)
  A.cTranspose()
  D_c=D_c+A


 A=(((E_d*E_b)*((E1*E8)*(a_up*a_d))*U_dagger)*((E7*E6)*c_d))
 
 A.permute([53,14,12,19,17],3)
 D_c=D_c+(-1.0)*A

 a_dp.setLabel([-18,-2,52,-16,-17])

 if Gauge is "Fixed":
  A=(((E_d*E_b)*((E1*E8)*(a_u*a_dp))*U)*((E7*E6)*c_u))
  A.permute([-19,-17,51,-14,-12],2)
  A.cTranspose()
  D_c=D_c+(-1.0)*A
#  D_b.cTranspose()
#  D_b.permute([56,18,20,6,4],3)

 D_c.cTranspose()
 D_c.permute([53,14,12,19,17],3)

 return D_a, D_c


def Obtain_grad_four(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c, d, a_u, b_u, a_up, b_up, U, Gauge):
 Gauge=copy.copy(Gauge)
 Gauge='Fixed'
 d.setLabel([19,-19,10,-10,8,-8,20,-20])
 c.setLabel([14,-14,12,-12,19,-19,17,-17])

 U.setLabel([51,52,53,54])
 Iden=uni10.UniTensor(uni10.CTYPE,U.bond())
 Iden.identity()
 Iden.setLabel([51,52,53,54])
 Iden1=copy.copy(Iden)
 Iden1.setLabel([53,-54,51,52])
 Idenb=Iden*Iden1
 Idenb.permute([-54,54],1)
 Idenb.identity()


 H1=copy.copy(U)
 H1.cTranspose()
 H1.setLabel([-20,-40,51,52])

 U_dagger=copy.copy(H1)
 U_dagger.setLabel([51,52,53,54])

 H=U*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([51,52,53,54])
 U_dagger.setLabel([51,52,53,54])
 
 a_up.setLabel([53,16,17,18,2])
 a_dp=copy.copy(a_up)
 a_dp.cTranspose()
 a_dp.setLabel([-18,-2,51,-16,-17])

 a_u.setLabel([53,16,17,18,2])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([-18,-2,51,-16,-17])
 

 b_u.setLabel([54,18,20,6,4])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([-6,-4,52,-18,-20])

 
 b_up.setLabel([54,18,20,6,4])
 b_dp=copy.copy(b_up)
 b_dp.cTranspose()
 b_dp.setLabel([-6,-4,52,-18,-20])
##########################---a---####################################
 a_dp.setLabel([-18,-2,53,-16,-17])
 b_dp.setLabel([-6,-4,54,-18,-20])
 E_c=(E7*E6)*(c)
 E_d=((E4*E5)*d)

 A=((((E_c)*(E_d))*((E2*E3)*(b_up*b_dp)))*((E1*E8)*a_dp))
 A.permute([53,16,17,18,2],3)
 D_a=A


 if Gauge is "Fixed":
  A=((((E_c))*((E_d))*((E2*E3)*(b_up*b_dp)))*((E1*E8)*a_up))
  A.permute([-18,-2,53,-16,-17],2)
  A.cTranspose()
  D_a=D_a+A


 A=((((E_c))*((E_d))*(((E2*E3)*(b_up*b_d))*U_dagger))*((E1*E8)*a_d))
 A.permute([53,16,17,18,2],3)
 D_a=D_a+(-1.0)*A

 b_dp.setLabel([-6,-4,52,-18,-20])

 if Gauge is "Fixed":
  A=((((E_c)*((E_d)))*(((E2*E3)*(b_u*b_dp))*U))*((E1*E8)*a_u))
  A.permute([-18,-2,51,-16,-17],2)
  A.cTranspose()
  D_a=D_a+(-1.0)*A
#  D_a.cTranspose()
#  D_a.permute([55,16,17,18,2],3)


 D_a.cTranspose()
 D_a.permute([53,16,17,18,2],3)
 
#####################################----b----#############################################

 a_dp.setLabel([-18,-2,53,-16,-17])
 b_dp.setLabel([-6,-4,54,-18,-20]) 

 A=((((E_c)*((E_d)))*(((E1*E8))*(a_up*a_dp)))*((E2*E3)*b_dp))
 
 A.permute([54,18,20,6,4],3)
 D_b=A

 if Gauge is "Fixed":
  A=((((E_c)*((E_d)))*(((E1*E8))*(a_up*a_dp)))*((E2*E3)*b_up))
  A.permute([-6,-4,54,-18,-20],2)
  A.cTranspose()
  D_b=D_b+A


 A=((((E_c)*((E_d)))*(((E1*E8)*(a_up*a_d))*U_dagger))*((E2*E3)*b_d))
 
 A.permute([54,18,20,6,4],3)
 D_b=D_b+(-1.0)*A

 a_dp.setLabel([-18,-2,51,-16,-17])


 if Gauge is "Fixed":
  A=((((E_c)*((E_d)))*(((E1*E8)*(a_u*a_dp))*U_dagger))*((E2*E3)*b_u))
  A.permute([-6,-4,52,-18,-20],2)
  A.cTranspose()
  D_b=D_b+(-1.0)*A
#  D_b.cTranspose()
#  D_b.permute([56,18,20,6,4],3)

 D_b.cTranspose()
 D_b.permute([54,18,20,6,4],3)

 return D_a, D_b


