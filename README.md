# A single-layer tensor-network algorithm

An iPEPS ansatz with an efficient optimization algorithm in the framework of the single-layer tensor network based on the corner-transfer matrix technique. 

How to run: python ipeps.py
