import pyUni10 as uni10
#import matplotlib.pyplot as plt
#import matplotlib
#import pylab
import math
import copy
import time
import Move
import MoveCorboz
import MoveFull
import basicA
import basicB
import basicC
import TruncateU
import numpy as np
from numpy import linalg as LA
import cmath
import math
from heapq import nsmallest
def norm_comp(complex_mat):
 D=int(complex_mat.row())
 D1=int(complex_mat.col())
 list_z=[]
 for i in xrange(D):
  for j in xrange(D1):
    list_z.append(abs(complex_mat[i*D1+j]))
  
 #print list_z
 return max(list_z)
def MaxAbs(c):
 blk_qnums = c.blockQnum()
 max_list=[]
 for qnum in blk_qnums:
    c_mat=c.getBlock(qnum)
    max_list.append(norm_comp(c_mat))
 #sv_mat = uni10.Matrix( len(max_list), len(max_list), max_list, True)
 #return sv_mat.absMax()
 max_list_f=[abs(x) for x in max_list]
 #print max_list_f, max(max_list_f)
 return max(max_list_f)


def max_ten(a):
 if ( MaxAbs(a) < 0.50e-1) or (MaxAbs(a) > 0.50e+1)   :
  a=a*(1.00/MaxAbs(a));
 else: a=a;
 
 return a


def matSx():
  spin = 0.5
  dim = int(spin * 2 + 1)
  Mat=uni10.CMatrix(dim, dim)
  Mat[0]=0
  Mat[1]=0.5
  Mat[2]=0.5
  Mat[3]=0
  return Mat 

def matSz():
  spin = 0.5
  dim = int(spin * 2 + 1)
  Mat=uni10.CMatrix(dim, dim);
  Mat[0]=0.5
  Mat[1]=0
  Mat[2]=0
  Mat[3]=-0.5
  return Mat 

def matSy():
  spin = 0.5
  dim = int(spin * 2 + 1)
  Mat=uni10.CMatrix(dim, dim);
  Mat[0]=0.0
  Mat[1]=-0.5j
  Mat[2]=0.5j
  Mat[3]=0
  return Mat 


def matIden():
    spin_t=0.5
    dimT = int(2*spin_t + 1)
    Mat=uni10.CMatrix(dimT, dimT)
    Mat[0]=1.
    Mat[1]=0
    Mat[2]=0
    Mat[3]=1.
    return Mat


#cx_mat X1(3,3);  X1.zeros();         X1(0,1)=1; X1(1,0)=1; X1(1,2)=1,X1(2,1)=1;
#cx_mat Z1(3,3);  Z1.zeros();         Z1(0,0)=1; Z1(1,1)=0; Z1(2,2)=-1;
#cx_mat Y1(3,3);  Y1.zeros();         Y1(0,1)=-1; Y1(1,0)=1; Y1(1,2)=-1,Y1(2,1)=1;


#def matSx():
#    spin_t=1
#    dimT = int(2*spin_t + 1)
#    Mat=(1.0/(2.0**(0.5)))*uni10.CMatrix(dimT, dimT,[0, 1.0, 0 ,1.0,0, 1.0,0,1.0,0])
#    return Mat 
#    
#def matSy():
#    spin_t=1
#    dimT = int(2*spin_t + 1)
#    Mat=(1.0/(2.0**(0.5)))*uni10.CMatrix(dimT, dimT,[0,-1.0,0,1.0,0,-1.0,0,1.0,0])
#    return Mat 


#def matSz():
#    spin_t=1
#    dimT = int(2*spin_t + 1)
#    Mat=uni10.CMatrix(dimT, dimT,[1,0,0,0,0,0,0,0,-1])
#    return Mat

#def matIden():
#    spin_t=1
#    dimT = int(2*spin_t + 1)
#    Mat=uni10.CMatrix(dimT, dimT,[1,0,0,0,1,0,0,0,1])
#    return Mat



################################################################################
def  central_charge(Env8,Env10,fileSpectrum,Arnoldi_bond):


 Env8[11].setLabel([4,2,5])
 Env8[10].setLabel([6,-2,4])
 Env10[11].setLabel([7,3,6])
 Env10[10].setLabel([9,-3,7])
 Eleft=(Env8[11]*Env8[10])
 Eleft1=(Env10[11]*Env10[10])
 Eleft.permute([5,-2,2,6],2)
 Eleft1.permute([6,-3,3,9],2)
 Mpo=Eleft

####
 Mpo1=Eleft*Eleft1
 Mpo1.permute([5,-2,-3,2,3,9],3)

 bdi = uni10.Bond(uni10.BD_IN, int(Mpo1.bond(1).dim())*int(Mpo1.bond(1).dim()))
 bdo = uni10.Bond(uni10.BD_OUT, int(Mpo1.bond(1).dim())*int(Mpo1.bond(1).dim()))
 Mpo_f=uni10.UniTensor(uni10.CTYPE,[Mpo1.bond(0),bdi, bdo,Mpo1.bond(5)])
 #print bdi, Mpo1.printDiagram(), Mpo_f.printDiagram()
 Mpo_f.putBlock(Mpo1.getBlock())
 Mpo_f.setLabel([5,-2,2,6])
 Mpo.setLabel([5,-2,2,6])
 #print Mpo.printDiagram(), Mpo_f.printDiagram()
###


 bdi = uni10.Bond(uni10.BD_IN, 20)
 bdo = uni10.Bond(uni10.BD_OUT, 20)

 Gamma=uni10.UniTensor(uni10.CTYPE,[bdi, Mpo.bond(1),bdo])
 Landa=uni10.UniTensor(uni10.CTYPE,[bdi,bdo])
 Gamma.randomize()
 Landa.randomize()
 print Gamma.printDiagram(), Landa.printDiagram()
 
 Gamma.setLabel([1,2,3])
 #Landa.setLabel([])

 
 theta=Gamma*Mpo
 theta.permute([1,5,-2,3,6],3)
 bdi = uni10.Bond(uni10.BD_IN, int(theta.bond(0).dim())*int(theta.bond(1).dim()))
 bdo = uni10.Bond(uni10.BD_OUT, int(theta.bond(3).dim())*int(theta.bond(4).dim()))

 theta_f=uni10.UniTensor(uni10.CTYPE,[bdi, theta.bond(2),bdo])
 theta_f.putBlock(theta.getBlock())
 
 print theta_f.printDiagram()
 theta_f.setLabel([1,2,3])
 vec_right=uni10.UniTensor(uni10.CTYPE,[Eright.bond(2),Eright1.bond(2),Eright.bond(2),Eright1.bond(2)], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([1,2,3,4])
 eig_list, eig_phase=ED_Spect4(Eleft,Eright,Eleft1,Eright1,vec_right,Arnoldi_bond)
 #eig_list1=ED_Len4(Eleft,Eright,vec_right,Arnoldi_bond)
 maxabs=np.abs(eig_list).max()
 eig_new_list = [math.log(abs(i * (1.0/maxabs))) for i in eig_list]
 eig_new_list_abs=[abs(i * (1.0/maxabs)) for i in eig_list]

 for i in xrange(len(eig_phase)):
  fileSpectrum.write(str(eig_phase[i])+" "+str(eig_new_list[i]) + "\n")
  fileSpectrum.flush()
 #len1=nsmallest(2, abslog_ten)[-1]



################################################################################
def  ES_spectrum4(Env,Env1,Env8,Env10,fileSpectrum,Arnoldi_bond,fileSpectrumC):

 Env8[11].setLabel([4,2,5])
 Env8[10].setLabel([6,-2,4])
 Env10[11].setLabel([7,3,6])
 Env10[10].setLabel([9,-3,7])
 Eleft=(Env8[11]*Env8[10])
 Eleft1=(Env10[11]*Env10[10])
 Eleft.permute([5,-2,2,6],4)
 Eleft1.permute([6,-3,3,9],4)

 Eleft*=3
 Eleft1*=3

 Env[4].setLabel([5,2,4])
 Env[5].setLabel([4,-2,6])
 Env1[4].setLabel([6,3,7])
 Env1[5].setLabel([7,-3,9])
 Eright=(Env[4]*Env[5])
 Eright1=(Env1[4]*Env1[5])
 Eright.permute([5,-2,2,6],4)
 Eright1.permute([6,-3,3,9],4)

 Eright*=3
 Eright1*=3

 vec_right=uni10.UniTensor(uni10.CTYPE,[Eright.bond(2),Eright1.bond(2),Eright.bond(2),Eright1.bond(2)], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([1,2,3,4])
 eig_list, eig_phase, Vec_uni_f=ED_Spect4(Eleft,Eright,Eleft1,Eright1,vec_right,Arnoldi_bond)
 #eig_list1=ED_Len4(Eleft,Eright,vec_right,Arnoldi_bond)
 maxabs=np.abs(eig_list).max()
 eig_new_list = [math.log(abs(i * (1.0/maxabs))) for i in eig_list]
 eig_new_list_abs=[abs(i * (1.0/maxabs)) for i in eig_list]

 for i in xrange(len(eig_phase)):
  fileSpectrum.write(str(eig_phase[i])+" "+str(eig_new_list[i]) + "\n")
  fileSpectrum.flush()
 #len1=nsmallest(2, abslog_ten)[-1]

 mat_m=Vec_uni_f.getBlock()
 chi=int(mat_m.col())
 #chi=2
 Chi_list=[]
 Entropy_list=[]
 print "chi", chi
 for q in xrange(5,chi,1):
  U, V, S=TruncateU.setTruncation3(Vec_uni_f,q)
  S_mat=S.getBlock(True)
  #print q
  sum_val=0
  for i in xrange(q):
    #print S_mat[i]
    if abs(S_mat[i]) > 10e-12:
     sum_val=sum_val+(-1.0)*math.log(abs(S_mat[i]),2)*abs(S_mat[i])
  Chi_list.append(q)
  Entropy_list.append(sum_val)
  

 for i in xrange(len(Chi_list)):
  fileSpectrumC.write(str(Chi_list[i])+" "+str(Entropy_list[i]) + "\n")
  fileSpectrumC.flush()

 print "W=4","chi", "Entropy", Chi_list , Entropy_list

###################################################################################
def ED_Spect4(Eleft,Eright,Eleft1,Eright1,Vec_uni,Arnoldi_bond):
  Vec_uni_f=0
  q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  #print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()

  for j in xrange(m):
   #print j, m
   if (j%50)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_ES24(Vec_uni,Eleft,Eright,Eleft1,Eright1,q0_even)
   #r_midle=Multi_ES2(Vec_uni,Eleft,Eright)
   #Vec_uni.putBlock(q0_even,r_midle)
   #print "hi"
   #r=Multi_full_right(Vec_uni,c, d)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace(uni10.CTYPE)
    #r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
    r_tem=Multy(q_vec[i], (-1.0+0.0j)*h[i*m+j])
    r=r+r_tem
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np, D_eig=eig_np_full(h)
  #Lambda, index, Lambda_comp =find_maxindex(D_eig[0])


  #print "D_eig", D_eig
  eig_list=[]
  eig_phase=[]

  d=np.size(e_np,0)
  #print "Hi0", d,e_np
  for q in xrange(int(d/2)+50): 
   if abs(e_np[q]) > 1.e-10:
    #print  e_np[q], abs(e_np[q]),q#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)
    eig_list.append(e_np[q])
    eigvec=return_vec(D_eig[1],q)
    Q=make_Q(q_vec)
    Q.resize(D,m)
    #Vec_F=Q*eigvec
    Vec_FL=Q*eigvec
    Vec_uni_new=uni10.UniTensor(uni10.CTYPE,list(Vec_uni.bond()))
    Vec_uni_new.putBlock(q0_even,Vec_FL)
    #print eigvec.row(), eigvec.col()#,D_eig[1] #, Vec_uni 
    Vec_uni_new.setLabel([2,-2,3,-3])
    if q==0:
      Vec_uni_f=copy.copy(Vec_uni_new)
      Vec_uni_f.permute([2,-2,3,-3],2)
    #Vec_uni_new_trans=copy.copy(Vec_uni_new)
    #Vec_uni_new_trans.cTranspose()
    #norm=Vec_uni_new_trans*Vec_uni_new
    #print norm[0] 
    #Vec_uni_new=Vec_uni_new*(1/((norm[0])**(0.5)))
    Vec_uni1=copy.copy(Vec_uni_new)
    #Vec_uni1.permute([3,-3,2,-2],4)
#    Vec_uni1.setLabel([5,-5,2,-2,3,-3,4,-4])
    Vec_uni1.setLabel([-3,2,-2,3])

    Vec_uni1_mat=Vec_uni1.getBlock()
    Vec_uni1_mat.conj()
    Vec_uni1.putBlock(Vec_uni1_mat)
    #Vec_uni1.cTranspose()
    Result_dat=Vec_uni_new*Vec_uni1
    #print  cmath.phase(Result_dat[0]) #Result_dat[0]
    eig_phase.append(cmath.phase(Result_dat[0]))

  return eig_list, eig_phase,Vec_uni_f


#@profile
def Multi_ES24(Vec_uni,Eleft,Eright,Eleft1,Eright1,q0_even):
  #print "hi"
  Vec_uni.setLabel([2,3,4,5])

  Eright.setLabel([1,-2,2,200])
  #print "hi",Vec_uni.printDiagram(), Eright.printDiagram()
  Vec_uni=Vec_uni*Eright

  Eright1.setLabel([200,-3,3,100])
  #print "hi",Vec_uni.printDiagram(), Eright.printDiagram()
  Vec_uni=Vec_uni*Eright1


  #Eright1=copy.copy(Eright)
  Eright.setLabel([100,-4,4,20])
  #print Vec_uni.printDiagram(),Eright1.printDiagram()
  Vec_uni=Vec_uni*Eright

  #Eright1=copy.copy(Eright)
  Eright1.setLabel([20,-5,5,1])
  #print Vec_uni.printDiagram(),Eright1.printDiagram()
  Vec_uni=Vec_uni*Eright1


#  Eright4=copy.copy(Eright2)
#  Eright4.setLabel([400,-10,-11,10,11,1])
#  Vec_uni=Vec_uni*Eright3


  Eleft.setLabel([1,-2,22,200])
  Vec_uni=Vec_uni*Eleft

  Eleft1.setLabel([200,-3,33,100])
  Vec_uni=Vec_uni*Eleft1


  #Eleft1=copy.copy(Eleft)
  Eleft.setLabel([100,-4,44,20])
  Vec_uni=Vec_uni*Eleft
  #Eleft2=copy.copy(Eleft1)

  #Eleft1=copy.copy(Eleft)
  Eleft1.setLabel([20,-5,55,1])
  Vec_uni=Vec_uni*Eleft1
  #Eleft2=copy.copy(Eleft1)

 
#  Eleft4=copy.copy(Eleft1)
#  Eleft4.setLabel([400,-10,-11,101,111,1])
#  Vec_uni=Vec_uni*Eleft4 

  #print  Eleft.printDiagram(), Eright.printDiagram() 
  #vec=Eleft*(Eright*Vec_uni)
  Vec_uni.permute([22,33,44,55],4) 
  Vec_M=Vec_uni.getBlock(q0_even)
  return Vec_M

#######################################################


################################################################################
def  ES_spectrum6(Env,Env1,Env8,Env10,fileSpectrum,Arnoldi_bond,fileSpectrumC):

 Env8[11].setLabel([4,2,5])
 Env8[10].setLabel([6,-2,4])
 Env10[11].setLabel([7,3,6])
 Env10[10].setLabel([9,-3,7])
 Eleft=(Env8[11]*Env8[10])
 Eleft1=(Env10[11]*Env10[10])
 Eleft.permute([5,-2,2,6],4)
 Eleft1.permute([6,-3,3,9],4)

 Eleft*=3
 Eleft1*=3

 Env[4].setLabel([5,2,4])
 Env[5].setLabel([4,-2,6])
 Env1[4].setLabel([6,3,7])
 Env1[5].setLabel([7,-3,9])
 Eright=(Env[4]*Env[5])
 Eright1=(Env1[4]*Env1[5])
 Eright.permute([5,-2,2,6],4)
 Eright1.permute([6,-3,3,9],4)

 Eright*=3
 Eright1*=3

 vec_right=uni10.UniTensor(uni10.CTYPE,[Eright.bond(2),Eright1.bond(2),Eright.bond(2),Eright1.bond(2),Eright.bond(2),Eright1.bond(2)], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([1,2,3,4,5,6])
 eig_list, eig_phase,Vec_uni_f=ED_Spect6(Eleft,Eright,Eleft1,Eright1,vec_right,Arnoldi_bond)
 #eig_list1=ED_Len4(Eleft,Eright,vec_right,Arnoldi_bond)
 maxabs=np.abs(eig_list).max()
 eig_new_list = [math.log(abs(i * (1.0/maxabs))) for i in eig_list]
 eig_new_list_abs=[abs(i * (1.0/maxabs)) for i in eig_list]

 for i in xrange(len(eig_phase)):
  fileSpectrum.write(str(eig_phase[i])+" "+str(eig_new_list[i]) + "\n")
  fileSpectrum.flush()
 #len1=nsmallest(2, abslog_ten)[-1]


 mat_m=Vec_uni_f.getBlock()
 chi=int(mat_m.col())
 #chi=2
 Chi_list=[]
 Entropy_list=[]
 print "chi", chi
 for q in xrange(10,chi,1):
  #print q
  U, V, S=TruncateU.setTruncation(Vec_uni_f,q)
  S_mat=S.getBlock(True)
  sum_val=0
  for i in xrange(q):
    #print S_mat[i]
    if abs(S_mat[i]) > 10e-12:
     sum_val=sum_val+(-1.0)*math.log(abs(S_mat[i]),2)*abs(S_mat[i])
  Chi_list.append(q)
  Entropy_list.append(sum_val)
  

 for i in xrange(len(Chi_list)):
  fileSpectrumC.write(str(Chi_list[i])+" "+str(Entropy_list[i]) + "\n")
  fileSpectrumC.flush()

  
 print "W=6","chi", "Entropy", Chi_list , Entropy_list



###################################################################################
def ED_Spect6(Eleft,Eright,Eleft1,Eright1,Vec_uni,Arnoldi_bond):
  Vec_uni_f=10
  q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  #print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()

  for j in xrange(m):
   #print j, m
   if (j%50)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_ES26(Vec_uni,Eleft,Eright,Eleft1,Eright1,q0_even)
   #r_midle=Multi_ES2(Vec_uni,Eleft,Eright)
   #Vec_uni.putBlock(q0_even,r_midle)
   #print "hi"
   #r=Multi_full_right(Vec_uni,c, d)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace(uni10.CTYPE)
    #r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
    r_tem=Multy(q_vec[i], (-1.0+0.0j)*h[i*m+j])
    r=r+r_tem
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np, D_eig=eig_np_full(h)
  #Lambda, index, Lambda_comp =find_maxindex(D_eig[0])


  #print "D_eig", D_eig
  eig_list=[]
  eig_phase=[]

  d=np.size(e_np,0)
  #print "Hi0", d,e_np
  for q in xrange(int(d/2)+60): 
   if abs(e_np[q]) > 1.e-10:
    #print  e_np[q], abs(e_np[q]),q#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)
    eig_list.append(e_np[q])
    eigvec=return_vec(D_eig[1],q)
    Q=make_Q(q_vec)
    Q.resize(D,m)
    #Vec_F=Q*eigvec
    Vec_FL=Q*eigvec
    Vec_uni_new=uni10.UniTensor(uni10.CTYPE,list(Vec_uni.bond()))
    Vec_uni_new.putBlock(q0_even,Vec_FL)
    #print eigvec.row(), eigvec.col()#,D_eig[1] #, Vec_uni 
    Vec_uni_new.setLabel([2,-2,3,-3,4,-4])

    if q==0:
      Vec_uni_f=copy.copy(Vec_uni_new)
      Vec_uni_f.permute([2,-2,3,-3,4,-4],3)

    #Vec_uni_new_trans=copy.copy(Vec_uni_new)
    #Vec_uni_new_trans.cTranspose()
    #norm=Vec_uni_new_trans*Vec_uni_new
    #print norm[0] 
    #Vec_uni_new=Vec_uni_new*(1/((norm[0])**(0.5)))
    Vec_uni1=copy.copy(Vec_uni_new)
    #Vec_uni1.permute([3,-3,2,-2],4)
#    Vec_uni1.setLabel([5,-5,2,-2,3,-3,4,-4])
    Vec_uni1.setLabel([-4,2,-2,3,-3,4])

    Vec_uni1_mat=Vec_uni1.getBlock()
    Vec_uni1_mat.conj()
    Vec_uni1.putBlock(Vec_uni1_mat)
    #Vec_uni1.cTranspose()
    Result_dat=Vec_uni_new*Vec_uni1
    #print  cmath.phase(Result_dat[0]) #Result_dat[0]
    eig_phase.append(cmath.phase(Result_dat[0]))

  return eig_list, eig_phase,Vec_uni_f



#@profile
def Multi_ES26(Vec_uni,Eleft,Eright,Eleft1,Eright1,q0_even):
  #print "hi"
  Vec_uni.setLabel([2,3,4,5,6,7])

  Eright.setLabel([1,-2,2,200])
  Vec_uni=Vec_uni*Eright

  Eright1.setLabel([200,-3,3,100])
  Vec_uni=Vec_uni*Eright1

  Eright.setLabel([100,-4,4,20])
  Vec_uni=Vec_uni*Eright

  Eright1.setLabel([20,-5,5,40])
  Vec_uni=Vec_uni*Eright1

  Eright.setLabel([40,-6,6,60])
  Vec_uni=Vec_uni*Eright

  Eright1.setLabel([60,-7,7,1])
  Vec_uni=Vec_uni*Eright1




  Eleft.setLabel([1,-2,22,200])
  Vec_uni=Vec_uni*Eleft

  Eleft1.setLabel([200,-3,33,100])
  Vec_uni=Vec_uni*Eleft1

  Eleft.setLabel([100,-4,44,20])
  Vec_uni=Vec_uni*Eleft

  Eleft1.setLabel([20,-5,55,100])
  Vec_uni=Vec_uni*Eleft1

  Eleft.setLabel([100,-6,66,20])
  Vec_uni=Vec_uni*Eleft

  Eleft1.setLabel([20,-7,77,1])
  Vec_uni=Vec_uni*Eleft1




  Vec_uni.permute([22,33,44,55,66,77],6) 
  Vec_M=Vec_uni.getBlock(q0_even)
  return Vec_M

#######################################################



################################################################################
def  ES_spectrum8(Env,Env1,Env8,Env10,fileSpectrum,Arnoldi_bond,fileSpectrumC):

 Env8[11].setLabel([4,2,5])
 Env8[10].setLabel([6,-2,4])
 Env10[11].setLabel([7,3,6])
 Env10[10].setLabel([9,-3,7])
 Eleft=(Env8[11]*Env8[10])
 Eleft1=(Env10[11]*Env10[10])
 Eleft.permute([5,-2,2,6],4)
 Eleft1.permute([6,-3,3,9],4)

 Eleft*=3
 Eleft1*=3

 Env[4].setLabel([5,2,4])
 Env[5].setLabel([4,-2,6])
 Env1[4].setLabel([6,3,7])
 Env1[5].setLabel([7,-3,9])
 Eright=(Env[4]*Env[5])
 Eright1=(Env1[4]*Env1[5])
 Eright.permute([5,-2,2,6],4)
 Eright1.permute([6,-3,3,9],4)

 Eright*=3
 Eright1*=3

 vec_right=uni10.UniTensor(uni10.CTYPE,[Eright.bond(2),Eright1.bond(2),Eright.bond(2),Eright1.bond(2),Eright.bond(2),Eright1.bond(2),Eright.bond(2),Eright1.bond(2)], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([1,2,3,4,5,6,7,8])
 eig_list, eig_phase,Vec_uni_f=ED_Spect8(Eleft,Eright,Eleft1,Eright1,vec_right,Arnoldi_bond)
 #eig_list1=ED_Len4(Eleft,Eright,vec_right,Arnoldi_bond)
 maxabs=np.abs(eig_list).max()
 eig_new_list = [math.log(abs(i * (1.0/maxabs))) for i in eig_list]
 eig_new_list_abs=[abs(i * (1.0/maxabs)) for i in eig_list]

 for i in xrange(len(eig_phase)):
  fileSpectrum.write(str(eig_phase[i])+" "+str(eig_new_list[i]) + "\n")
  fileSpectrum.flush()
 #len1=nsmallest(2, abslog_ten)[-1]

 mat_m=Vec_uni_f.getBlock()
 chi=int(mat_m.col())
 #chi=2
 Chi_list=[]
 Entropy_list=[]
 print "chi", chi
 for q in xrange(20,chi,2):
  U, V, S=TruncateU.setTruncation4(Vec_uni_f,q)
  S_mat=S.getBlock(True)
  #print q
  sum_val=0
  for i in xrange(q):
    #print S_mat[i]
    if abs(S_mat[i]) > 10e-12:
     sum_val=sum_val+(-1.0)*math.log(abs(S_mat[i]),2)*abs(S_mat[i])
  Chi_list.append(q)
  Entropy_list.append(sum_val)
  

 for i in xrange(len(Chi_list)):
  fileSpectrumC.write(str(Chi_list[i])+" "+str(Entropy_list[i]) + "\n")
  fileSpectrumC.flush()

  
 print "W=8","chi", "Entropy", Chi_list , Entropy_list




###################################################################################
def ED_Spect8(Eleft,Eright,Eleft1,Eright1,Vec_uni,Arnoldi_bond):
  Vec_uni_f=1
  q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  #print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()

  for j in xrange(m):
   #print j, m
   if (j%50)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_ES28(Vec_uni,Eleft,Eright,Eleft1,Eright1,q0_even)
   #r_midle=Multi_ES2(Vec_uni,Eleft,Eright)
   #Vec_uni.putBlock(q0_even,r_midle)
   #print "hi"
   #r=Multi_full_right(Vec_uni,c, d)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace(uni10.CTYPE)
    #r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
    r_tem=Multy(q_vec[i], (-1.0+0.0j)*h[i*m+j])
    r=r+r_tem
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np, D_eig=eig_np_full(h)
  #Lambda, index, Lambda_comp =find_maxindex(D_eig[0])


  #print "D_eig", D_eig
  eig_list=[]
  eig_phase=[]

  d=np.size(e_np,0)
  #print "Hi0", d,e_np
  for q in xrange(int(d/2)+60): 
   if abs(e_np[q]) > 1.e-10:
    #print  e_np[q], abs(e_np[q]),q#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)
    eig_list.append(e_np[q])
    eigvec=return_vec(D_eig[1],q)
    Q=make_Q(q_vec)
    Q.resize(D,m)
    #Vec_F=Q*eigvec
    Vec_FL=Q*eigvec
    Vec_uni_new=uni10.UniTensor(uni10.CTYPE,list(Vec_uni.bond()))
    Vec_uni_new.putBlock(q0_even,Vec_FL)
    #print eigvec.row(), eigvec.col()#,D_eig[1] #, Vec_uni 
    Vec_uni_new.setLabel([2,-2,3,-3,4,-4,5,-5])

    if q==0:
      Vec_uni_f=copy.copy(Vec_uni_new)
      Vec_uni_f.permute([2,-2,3,-3,4,-4,5,-5],4)


    #Vec_uni_new_trans=copy.copy(Vec_uni_new)
    #Vec_uni_new_trans.cTranspose()
    #norm=Vec_uni_new_trans*Vec_uni_new
    #print norm[0] 
    #Vec_uni_new=Vec_uni_new*(1/((norm[0])**(0.5)))
    Vec_uni1=copy.copy(Vec_uni_new)
    #Vec_uni1.permute([3,-3,2,-2],4)
#    Vec_uni1.setLabel([5,-5,2,-2,3,-3,4,-4])
    Vec_uni1.setLabel([-5,2,-2,3,-3,4,-4,5])

    Vec_uni1_mat=Vec_uni1.getBlock()
    Vec_uni1_mat.conj()
    Vec_uni1.putBlock(Vec_uni1_mat)
    #Vec_uni1.cTranspose()
    Result_dat=Vec_uni_new*Vec_uni1
    #print  cmath.phase(Result_dat[0]) #Result_dat[0]
    eig_phase.append(cmath.phase(Result_dat[0]))

  return eig_list, eig_phase,Vec_uni_f



#@profile
def Multi_ES28(Vec_uni,Eleft,Eright,Eleft1,Eright1,q0_even):
  #print "hi"
  Vec_uni.setLabel([2,3,4,5,6,7,8,9])

  Eright.setLabel([1,-2,2,200])
  Vec_uni=Vec_uni*Eright

  Eright1.setLabel([200,-3,3,100])
  Vec_uni=Vec_uni*Eright1

  Eright.setLabel([100,-4,4,20])
  Vec_uni=Vec_uni*Eright

  Eright1.setLabel([20,-5,5,40])
  Vec_uni=Vec_uni*Eright1

  Eright.setLabel([40,-6,6,60])
  Vec_uni=Vec_uni*Eright

  Eright1.setLabel([60,-7,7,40])
  Vec_uni=Vec_uni*Eright1

  Eright.setLabel([40,-8,8,60])
  Vec_uni=Vec_uni*Eright

  Eright1.setLabel([60,-9,9,1])
  Vec_uni=Vec_uni*Eright1



  Eleft.setLabel([1,-2,22,200])
  Vec_uni=Vec_uni*Eleft

  Eleft1.setLabel([200,-3,33,100])
  Vec_uni=Vec_uni*Eleft1

  Eleft.setLabel([100,-4,44,20])
  Vec_uni=Vec_uni*Eleft

  Eleft1.setLabel([20,-5,55,100])
  Vec_uni=Vec_uni*Eleft1

  Eleft.setLabel([100,-6,66,20])
  Vec_uni=Vec_uni*Eleft

  Eleft1.setLabel([20,-7,77,100])
  Vec_uni=Vec_uni*Eleft1

  Eleft.setLabel([100,-8,88,20])
  Vec_uni=Vec_uni*Eleft

  Eleft1.setLabel([20,-9,99,1])
  Vec_uni=Vec_uni*Eleft1



  Vec_uni.permute([22,33,44,55,66,77,88,99],8) 
  Vec_M=Vec_uni.getBlock(q0_even)
  return Vec_M

#######################################################



























def Init_env(Env):
 c1=copy.copy(Env[0])
 c2=copy.copy(Env[1])
 c3=copy.copy(Env[2]) 
 c4=copy.copy(Env[3]) 
 Ta1=copy.copy(Env[4])
 Ta2=copy.copy(Env[5])
 Ta3=copy.copy(Env[6])
 Ta4=copy.copy(Env[7])
 Tb1=copy.copy(Env[8])
 Tb2=copy.copy(Env[9])
 Tb3=copy.copy(Env[10])
 Tb4=copy.copy(Env[11])
 return c1, c2, c3, c4, Ta1, Ta2, Ta3, Ta4, Tb1, Tb2, Tb3, Tb4



#@profile
def Spectrum_Arnoldi_right0(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,fileSpectrum,Arnoldi_bond):


 bd0=Env[4].bond(0)
 bd1=a_u.bond(1)
 bd2=a_u.bond(2)
 bd3=Env[4].bond(0)


 vec_right=uni10.UniTensor(uni10.CTYPE,[bd0,bd1,bd2,bd3], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([9,11,22,24])
 eig_list=ED_right(Env, Env4,Env8,vec_right,Arnoldi_bond,a_u,b_u)


 maxabs = np.abs(eig_list).max()
 eig_new_list = [i * (1.0/maxabs) for i in eig_list]
 #print  maxabs, np.abs(eig_new_list).max(), maxabs
 img_ten=[ i.imag     for i in eig_new_list]
 real_ten=[ i.real     for i in eig_new_list]
 abslog_ten=[ -0.5*math.log(abs(i))  for i in eig_new_list]
 phase_ten=[ cmath.phase(i)          for i in eig_new_list]

 for i in xrange(len(real_ten)):
  fileSpectrum.write(str(real_ten[i]) + " " + str(img_ten[i])+" "+str(phase_ten[i])+" "+str(abslog_ten[i]) + "\n")
  fileSpectrum.flush()

 len1=nsmallest(2, abslog_ten)[-1]


 bd0=Env[4].bond(0)
 bd1=a_u.bond(1)
 bd2=a_u.bond(2)
 bd3=Env[4].bond(0)


 vec_right=uni10.UniTensor(uni10.CTYPE,[bd0,bd1,bd2,bd3], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([9,11,22,24])
 eig_list=ED_right(Env2, Env6,Env10,vec_right,Arnoldi_bond,c_u,d_u)

 maxabs = np.abs(eig_list).max()
 eig_new_list = [i * (1.0/maxabs) for i in eig_list]
 #print  maxabs, np.abs(eig_new_list).max(), maxabs
 img_ten=[ i.imag     for i in eig_new_list]
 real_ten=[ i.real     for i in eig_new_list]
 abslog_ten=[ -0.5*math.log(abs(i))  for i in eig_new_list]
 phase_ten=[ cmath.phase(i)          for i in eig_new_list]

 len2=nsmallest(2, abslog_ten)[-1]

##################################################################
 bd0=Env[4].bond(0)
 bd1=a_u.bond(1)
 bd2=a_u.bond(2)
 bd3=Env[4].bond(0)


 vec_right=uni10.UniTensor(uni10.CTYPE,[bd0,bd1,bd2,bd3], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([0,2,4,6])
 eig_list=ED_rightup(Env2, Env1,Env,vec_right,Arnoldi_bond,c_u,a_u)

 maxabs = np.abs(eig_list).max()
 eig_new_list = [i * (1.0/maxabs) for i in eig_list]
 #print  maxabs, np.abs(eig_new_list).max(), maxabs
 img_ten=[ i.imag     for i in eig_new_list]
 real_ten=[ i.real     for i in eig_new_list]
 abslog_ten=[ -0.5*math.log(abs(i))  for i in eig_new_list]
 phase_ten=[ cmath.phase(i)          for i in eig_new_list]

 len3=nsmallest(2, abslog_ten)[-1]
#################################################################
 bd0=Env[4].bond(0)
 bd1=a_u.bond(1)
 bd2=a_u.bond(2)
 bd3=Env[4].bond(0)


 vec_right=uni10.UniTensor(uni10.CTYPE,[bd0,bd1,bd2,bd3], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([0,2,4,6])
 eig_list=ED_rightup(Env10, Env9,Env8,vec_right,Arnoldi_bond,d_u,b_u)

 maxabs = np.abs(eig_list).max()
 eig_new_list = [i * (1.0/maxabs) for i in eig_list]
 #print  maxabs, np.abs(eig_new_list).max(), maxabs
 img_ten=[ i.imag     for i in eig_new_list]
 real_ten=[ i.real     for i in eig_new_list]
 abslog_ten=[ -0.5*math.log(abs(i))  for i in eig_new_list]
 phase_ten=[ cmath.phase(i)          for i in eig_new_list]

 len4=nsmallest(2, abslog_ten)[-1]
############################################################

 list1=[len2,len1,len3,len4]
 lenf=max(list1)

 print lenf, list1
 return lenf 








def Spectrum_Arnoldi_right_Eff(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,fileSpectrum,Arnoldi_bond):


 bd0=Env[4].bond(0)
 bd3=Env[4].bond(0)


 vec_right=uni10.UniTensor(uni10.CTYPE,[bd0,bd3], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([9,24])
 eig_list=ED_right_eff(Env,Env4,Env8,Env2,Env6,Env10,vec_right,Arnoldi_bond)

 maxabs = np.abs(eig_list).max()
 eig_new_list = [i * (1.0/maxabs) for i in eig_list]
 #print  maxabs, np.abs(eig_new_list).max(), maxabs
 img_ten=[ i.imag     for i in eig_new_list]
 real_ten=[ i.real     for i in eig_new_list]
 abslog_ten=[ -0.5*math.log(abs(i))  for i in eig_new_list]
 phase_ten=[ cmath.phase(i)          for i in eig_new_list]

 for i in xrange(len(real_ten)):
  fileSpectrum.write(str(real_ten[i]) + " " + str(img_ten[i])+" "+str(phase_ten[i])+" "+str(abslog_ten[i]) + "\n")
  fileSpectrum.flush()

 len1=nsmallest(6, abslog_ten)

###################################################################
 bd0=Env[4].bond(0)
 bd1=a_u.bond(1)
 bd2=a_u.bond(2)
 bd3=Env[4].bond(0)


 vec_right=uni10.UniTensor(uni10.CTYPE,[bd0,bd3], "vec_right")
 vec_right.randomize()
 vec_right.setLabel([0,6])
 eig_list=ED_rightup_eff(Env2, Env1,Env,Env10, Env9,Env8,vec_right,Arnoldi_bond)

 maxabs = np.abs(eig_list).max()
 eig_new_list = [i * (1.0/maxabs) for i in eig_list]
 #print  maxabs, np.abs(eig_new_list).max(), maxabs
 img_ten=[ i.imag     for i in eig_new_list]
 real_ten=[ i.real     for i in eig_new_list]
 abslog_ten=[ -0.5*math.log(abs(i))  for i in eig_new_list]
 phase_ten=[ cmath.phase(i)          for i in eig_new_list]

 len3=nsmallest(6, abslog_ten)
##################################################################

 list_t=[len1[1],len3[1]]
 lenf=max(list_t)

 list_t1=[len1[2],len3[2]]
 lenf1=max(list_t)

 print "Landa_1=", len1 
 print "Landa_2=", len3 
 return lenf, lenf1 








def ED_full(a, b, c, d,Vec_uni,Arnoldi_bond):
  q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   if (j%1)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r_midle=Multi_full_right(Vec_uni,a, b)
   Vec_uni.putBlock(q0_even,r_midle)
   print "hi"
   r=Multi_full_right(Vec_uni,c, d)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace()
    r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np, D_eig=eig_np_full(h)
  #Lambda, index, Lambda_comp =find_maxindex(D_eig[0])
  for i in xrange(m):

  
	eig_list=[]
	eig_phase=[]

	d=np.size(e_np,0)
	print e_np
	for q in xrange(d): 
			if abs(e_np[q]) > 1.e-6:
				#print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
				eig_list.append(e_np[q])
				eigvec=return_vec(D_eig[1], q )
				Q=make_Q(q_vec)
				Q.resize(D,m)
				Vec_F=Q*eigvec
				Vec_FL=Q*eigvec
				Vec_uni_new=uni10.UniTensor(uni10.CTYPE,list(Vec_uni.bond()))
				Vec_uni_new.putBlock(q0_even,Vec_FL)
				#print eigvec.row(), eigvec.col()#,D_eig[1] #, Vec_uni 
				Vec_uni_new.setLabel([1,-1,2,-2,3,-3,4,-4,5,-5,6,-6,202])
				Vec_uni1=copy.copy(Vec_uni_new)
				Vec_uni1.permute([5,-5,6,-6,1,-1,2,-2,3,-3,4,-4,202],12)
				Vec_uni1.setLabel([1,-1,2,-2,3,-3,4,-4,5,-5,6,-6,202])
				Vec_uni1.ccTranspose()
				Result_dat=Vec_uni_new*Vec_uni1
				print Result_dat[0],cmath.phase(Result_dat[0])
				eig_phase.append(cmath.phase(Result_dat[0]))

  return eig_list, eig_phase



##############################################################################################
def ED_up4(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,Vec_uni,Arnoldi_bond):
  q0_even = uni10.Qnum(-2,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   if (j%20)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_u4(Vec_uni,a, b, c, d, Tb2, Ta2, Ta4, Tb4)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace()
    r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np=eig_np(h)
  #print D_eig[0]
  #Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  #eigvec=return_vec(D_eig[1], index )
  #print 'r0', Lambda, Lambda_comp
  #Q=make_Q(q_vec)
  #Q.resize(D,m)
  #Vec_F=Q*eigvec
  #Vec_FL=Q*eigvec
 
  
  eig_list=[]
  d=np.size(e_np,0)
  #print d
  for q in xrange(d): 
    if abs(e_np[q]) > 1.e-6:
     #print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
     eig_list.append(e_np[q])


  return eig_list



##############################################################################################
def ED_up2(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,Vec_uni,Arnoldi_bond):
  q0_even = uni10.Qnum(2,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   if (j%20)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_u2(Vec_uni,a, b, c, d, Tb2, Ta2, Ta4, Tb4)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace()
    r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np=eig_np(h)
  #print D_eig[0]
  #Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  #eigvec=return_vec(D_eig[1], index )
  #print 'r0', Lambda, Lambda_comp
  #Q=make_Q(q_vec)
  #Q.resize(D,m)
  #Vec_F=Q*eigvec
  #Vec_FL=Q*eigvec
 
  
  eig_list=[]
  d=np.size(e_np,0)
  #print d
  for q in xrange(d): 
    if abs(e_np[q]) > 1.e-6:
     #print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
     eig_list.append(e_np[q])


  return eig_list






##############################################################################################
def ED_up0(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,Vec_uni,Arnoldi_bond):
  q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   if (j%20)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_u0(Vec_uni,a, b, c, d, Tb2, Ta2, Ta4, Tb4)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace()
    r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np=eig_np(h)
  #print D_eig[0]
  #Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  #eigvec=return_vec(D_eig[1], index )
  #print 'r0', Lambda, Lambda_comp
  #Q=make_Q(q_vec)
  #Q.resize(D,m)
  #Vec_F=Q*eigvec
  #Vec_FL=Q*eigvec
 
  
  eig_list=[]
  d=np.size(e_np,0)
  #print d
  for q in xrange(d): 
    if abs(e_np[q]) > 1.e-6:
     #print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
     eig_list.append(e_np[q])


  return eig_list


##############################################################################################
def ED_right4(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,Vec_uni,Arnoldi_bond):
  q0_even = uni10.Qnum(-2,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   if (j%20)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_r4(Vec_uni,a,b,c,d,Tb1,Ta1,Ta3,Tb3)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace()
    r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np=eig_np(h)
  #print D_eig[0]
  #Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  #eigvec=return_vec(D_eig[1], index )
  #print 'r0', Lambda, Lambda_comp
  #Q=make_Q(q_vec)
  #Q.resize(D,m)
  #Vec_F=Q*eigvec
  #Vec_FL=Q*eigvec
 
  
  eig_list=[]
  d=np.size(e_np,0)
  #print d
  for q in xrange(d): 
    if abs(e_np[q]) > 1.e-6:
     #print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
     eig_list.append(e_np[q])


  return eig_list



##############################################################################################
def ED_right2(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,Vec_uni,Arnoldi_bond):
  q0_even = uni10.Qnum(2,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   if (j%20)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_r2(Vec_uni,a,b,c,d,Tb1,Ta1,Ta3,Tb3)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace()
    r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np=eig_np(h)
  #print D_eig[0]
  #Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  #eigvec=return_vec(D_eig[1], index )
  #print 'r0', Lambda, Lambda_comp
  #Q=make_Q(q_vec)
  #Q.resize(D,m)
  #Vec_F=Q*eigvec
  #Vec_FL=Q*eigvec
 
  
  eig_list=[]
  d=np.size(e_np,0)
  #print d
  for q in xrange(d): 
    if abs(e_np[q]) > 1.e-6:
     #print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
     eig_list.append(e_np[q])

  return eig_list


##############################################################################################
def ED_right(Env, Env4,Env8,Vec_uni,Arnoldi_bond,a_u,b_u):
  q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  #print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   #if (j%20)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_r0(Env, Env4,Env8,Vec_uni,Arnoldi_bond,a_u,b_u)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace(uni10.CTYPE)
    #print h[i*m+j],(-1.0+0.0j)*h[i*m+j], h[i*m+j],r
    r_tem=Multy(q_vec[i], (-1.0+0.0j)*h[i*m+j])
    r=r+r_tem
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np=eig_np(h)
  #print D_eig[0]
  #Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  #eigvec=return_vec(D_eig[1], index )
  #print 'r0', Lambda, Lambda_comp
  #Q=make_Q(q_vec)
  #Q.resize(D,m)
  #Vec_F=Q*eigvec
  #Vec_FL=Q*eigvec
 
  
  eig_list=[]
  d=np.size(e_np,0)
  #print d
  for q in xrange(d): 
    if abs(e_np[q]) > 1.e-6:
     #print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
     eig_list.append(e_np[q])


  return eig_list




def ED_right_eff(Env,Env4,Env8,Env2,Env6,Env10,Vec_uni,Arnoldi_bond):
  q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  #print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   #if (j%20)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_r0_eff(Env,Env4,Env8,Env2,Env6,Env10,Vec_uni,Arnoldi_bond)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace(uni10.CTYPE)
    #print h[i*m+j],(-1.0+0.0j)*h[i*m+j], h[i*m+j],r
    r_tem=Multy(q_vec[i], (-1.0+0.0j)*h[i*m+j])
    r=r+r_tem
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np=eig_np(h)
  #print D_eig[0]
  #Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  #eigvec=return_vec(D_eig[1], index )
  #print 'r0', Lambda, Lambda_comp
  #Q=make_Q(q_vec)
  #Q.resize(D,m)
  #Vec_F=Q*eigvec
  #Vec_FL=Q*eigvec
 
  
  eig_list=[]
  d=np.size(e_np,0)
  #print d
  for q in xrange(d): 
    if abs(e_np[q]) > 1.e-6:
     #print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
     eig_list.append(e_np[q])

  return eig_list





def ED_rightup(Env2, Env1,Env,Vec_uni,Arnoldi_bond,c_u,a_u):
  q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  #print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   #if (j%20)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_r0up(Env2, Env1,Env,Vec_uni,Arnoldi_bond,c_u,a_u)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace(uni10.CTYPE)
    #print h[i*m+j],(-1.0+0.0j)*h[i*m+j], h[i*m+j],r
    r_tem=Multy(q_vec[i], (-1.0+0.0j)*h[i*m+j])
    r=r+r_tem
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np=eig_np(h)
  #print D_eig[0]
  #Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  #eigvec=return_vec(D_eig[1], index )
  #print 'r0', Lambda, Lambda_comp
  #Q=make_Q(q_vec)
  #Q.resize(D,m)
  #Vec_F=Q*eigvec
  #Vec_FL=Q*eigvec
 
  
  eig_list=[]
  d=np.size(e_np,0)
  #print d
  for q in xrange(d): 
    if abs(e_np[q]) > 1.e-6:
     #print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
     eig_list.append(e_np[q])


  return eig_list





def ED_rightup_eff(Env2, Env1,Env,Env10, Env9,Env8,Vec_uni,Arnoldi_bond):
  q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
  Vec_F=Vec_uni.getBlock(q0_even)
  D=Vec_F.row()
  #print "D=",  D
  e_np=0
  m=Arnoldi_bond
  W=0
  num=0
  E1=0
  Vec_F=Vec_F*(1.00/Vec_F.norm())
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   #print j, m
   #if (j%20)==0: print j;  
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(q0_even,vec_tem)
   r=Multi_r0up_eff(Env2,Env1,Env,Env10,Env9,Env8,Vec_uni,Arnoldi_bond)
   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace(uni10.CTYPE)
    #print h[i*m+j],(-1.0+0.0j)*h[i*m+j], h[i*m+j],r
    r_tem=Multy(q_vec[i], (-1.0+0.0j)*h[i*m+j])
    r=r+r_tem
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  e_np=eig_np(h)
  #print D_eig[0]
  #Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  #eigvec=return_vec(D_eig[1], index )
  #print 'r0', Lambda, Lambda_comp
  #Q=make_Q(q_vec)
  #Q.resize(D,m)
  #Vec_F=Q*eigvec
  #Vec_FL=Q*eigvec
 
  
  eig_list=[]
  d=np.size(e_np,0)
  #print d
  for q in xrange(d): 
    if abs(e_np[q]) > 1.e-6:
     #print  e_np[q], abs(e_np[q])#,-1*math.log(abs(e[q])), cmath.phase(e[q])#, np.angle(e[q],deg=True)     
     eig_list.append(e_np[q])


  return eig_list




def a_ad_z(H, a_u):
    a_u.setLabel([51,1,2,3,4])
    H.setLabel([-51,51])
    result=(a_u*H)
    result.permute([-51,1,2,3,4],3)
    result.setLabel([0,1,2,3,4])

    return result




#@profile
def CorrelationH(a_u,b_u,Env,Env4,Env8,D,h,d_phys,chi,Corner_method,Model,distance_final,fileCorrH,Arnoldi_bond):

 c1,c2,c3,c4,Ta1,Ta2,Ta3,Ta4,Tb1,Tb2,Tb3,Tb4=Init_env(Env)

 if Model is "Heisenberg":
  bdi = uni10.Bond(uni10.BD_IN, 8)
  bdo = uni10.Bond(uni10.BD_OUT, 8)
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdo])
  sz = matSz()
  sx = matSx()
  sy = matSy()
  iden = matIden()
  H_tem=uni10.otimes(uni10.otimes(sz,sz),iden)+uni10.otimes(uni10.otimes(sx,sx),iden)+uni10.otimes(uni10.otimes(sy,sy),iden)+uni10.otimes(uni10.otimes(sz,iden),sz)+uni10.otimes(uni10.otimes(sx,iden),sx)+uni10.otimes(uni10.otimes(sy,iden),sy)+uni10.otimes(uni10.otimes(iden,sz),sz)+uni10.otimes(uni10.otimes(iden,sx),sx)+uni10.otimes(uni10.otimes(iden,sy),sy)

  #H_tem=uni10.otimes(sx,uni10.otimes(sy,sz))+(-1.0)*uni10.otimes(sx,uni10.otimes(sz,sy))+uni10.otimes(sy,uni10.otimes(sz,sx))+(-1.0)*uni10.otimes(sy,uni10.otimes(sx,sz))+uni10.otimes(sz,uni10.otimes(sx,sy))+(-1.0)*uni10.otimes(sz,uni10.otimes(sy,sx))



  H.putBlock(H_tem)
  H.setLabel([-10,10])
 if Model is "Heisenberg_Z2":
  #print d_phys
  bdi = uni10.Bond(uni10.BD_IN, d_phys)
  bdo = uni10.Bond(uni10.BD_OUT, d_phys)
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  sz = matSz()
  iden = matIden()
  szt=uni10.otimes(sz,iden)
  H.setRawElem(szt)
  szt1=uni10.otimes(iden,sz)
  H1.setRawElem(szt1)
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
 if Model is "Heisenberg_U1":
  bdi = uni10.Bond(uni10.BD_IN, d_phys)
  bdo = uni10.Bond(uni10.BD_OUT, d_phys)
  HH = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Iden = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  sz = matSz()
  sx = matSx()
  sy = matSy()
  Iden.identity()
  iden = matIden()
  HH_tem=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
  H_tem=uni10.otimes(sz,iden)#+uni10.otimes(sx,iden)#+(-1.0)*uni10.otimes(sy,iden)
  H1_tem=uni10.otimes(iden,sz)#+uni10.otimes(iden,sx)#+(-1.0)*uni10.otimes(iden,sy)
  HH.setRawElem(HH_tem)
  H.setRawElem(H_tem)
  H1.setRawElem(H1_tem)
  Iden=copy.copy(H)
  Iden.identity()
  #print HH_tem, HH, H_tem, H, H1_tem, H1
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
#########################################################################
 vec_left=make_vleft(Env)
 vec_right=make_vright(Env8)
 a_up=a_ad_z(H, a_u)
 b_up=a_ad_z(H, b_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]


 #vec_right_copy=copy.copy(vec_right)
 #Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right_copy,Arnoldi_bond)
 #print "Corr_length",Corr_length
 #fileCorrLength.write(str(Corr_length)  + "\n")
 #fileCorrLength.flush()



 #print "\n"
#######################a-a#####################################################

 vec_left_Z1=Make_first_vecleft_a(vec_left, Env,Env4,Env8, a_up,b_u,a_u,b_u)
 vec_left_norm1=Make_first_vecleft_a(vec_left, Env,Env4,Env8, a_u,b_u,a_u,b_u)

 S=vec_left_Z1*vec_right
 S1=vec_left_norm1*vec_right
 #print S,S1
 A1=S[0]/S1[0]
 

 vec_left_Z=Make_first_vecleft_a(vec_left, Env,Env4,Env8, a_u,b_up,a_u,b_u)
 S=vec_left_Z*vec_right
 S1=vec_left_norm1*vec_right
 A2=S[0]/S1[0]


 vec_left_Z=Make_first_vecleft_a(vec_left, Env,Env4,Env8, a_up,b_up,a_u,b_u)
 S=vec_left_Z*vec_right
 S1=vec_left_norm1*vec_right
 A3=S[0]/S1[0]
 dis_val_list.append(1)
 Corr_val_list.append((A3-A1*A2).real) 
 
 


##################################################################


 vec_left_Zf=Make_first_vecleft_a(vec_left_Z1, Env,Env4,Env8, a_up,b_u,a_u,b_u)
 vec_left_normf=Make_first_vecleft_a(vec_left_norm1, Env,Env4,Env8, a_u,b_u,a_u,b_u)

 S=vec_left_Zf*vec_right
 S1=vec_left_normf*vec_right
 #print S,S1
 A3=S[0]/S1[0]
 #print "hi",A1, A3,A1*A1
 dis_val_list.append(2)
 Corr_val_list.append((A3-A1*A1).real) 
 
 
 vec_left_Zf=Make_first_vecleft_a(vec_left_Z1, Env,Env4,Env8, a_u,b_up,a_u,b_u)
 vec_left_normf=Make_first_vecleft_a(vec_left_norm1, Env,Env4,Env8, a_u,b_u,a_u,b_u)

 S=vec_left_Zf*vec_right
 S1=vec_left_normf*vec_right
 #print S,S1
 A3=S[0]/S1[0]
 dis_val_list.append(3)
 Corr_val_list.append((A3-A1*A2).real) 

 print Corr_val_list


 dis_val=2

 for i in xrange(distance_final):
  dis_val+=2
  vec_left_Z1=Make_first_vecleft_a(vec_left_Z1, Env,Env4,Env8, a_u,b_u,a_u,b_u)
  vec_left_norm1=Make_first_vecleft_a(vec_left_norm1, Env,Env4,Env8, a_u,b_u,a_u,b_u)

  vec_left_Zf=Make_first_vecleft_a(vec_left_Z1, Env,Env4,Env8, a_up,b_u,a_u,b_u)
  vec_left_normf=Make_first_vecleft_a(vec_left_norm1, Env,Env4,Env8, a_u,b_u,a_u,b_u)


  S=vec_left_Zf*vec_right
  S1=vec_left_normf*vec_right
  A3=S[0]/S1[0]
  dis_val_list.append(dis_val)
  Corr_val_list.append((A3-A1*A1).real) 


  vec_left_Zf=Make_first_vecleft_a(vec_left_Z1, Env,Env4,Env8, a_u,b_up,a_u,b_u)
  vec_left_normf=Make_first_vecleft_a(vec_left_norm1, Env,Env4,Env8, a_u,b_u,a_u,b_u)


  S=vec_left_Zf*vec_right
  S1=vec_left_normf*vec_right
  A3=S[0]/S1[0]
  dis_val_list.append(dis_val+1)
  Corr_val_list.append((A3-A1*A2).real) 


  print i, Corr_val_list[i+3]



 for i in xrange(len(dis_val_list)):
  fileCorrH.write(str(dis_val_list[i])  + " " + str(Corr_val_list[i]) + "\n")
  fileCorrH.flush()






def CorrelationV(a_u,b_u,c_u,d_u,a,b,c,d,Env,D,h,d_phys,chi,Corner_method,Model,distance_final):
 c1,c2,c3,c4, Ta1, Ta2, Ta3, Ta4, Tb1, Tb2, Tb3, Tb4=Init_env(Env)


 if Model is "Heisenberg":
  bdi = uni10.Bond(uni10.BD_IN, 2)
  bdo = uni10.Bond(uni10.BD_OUT, 2)
  HH = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Hz = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1z = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Hx = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1x = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Hy = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1y = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])

  Iden = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Iden.identity()
  sz = matSz()
  sx = matSx()
  sy = matSy()
  iden = matIden()
  HH_tem=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+uni10.otimes(sy,sy)
  H_temz=uni10.otimes(sz,iden)
  H1_temz=uni10.otimes(iden,sz)
  H_temx=uni10.otimes(sx,iden)
  H1_temx=uni10.otimes(iden,sx)
  H_temy=uni10.otimes(sy,iden)
  H1_temy=uni10.otimes(iden,sy)

  HH.putBlock(HH_tem)
  Hz.putBlock(H_temz)
  H1z.putBlock(H1_temz)
  Hx.putBlock(H_temx)
  H1x.putBlock(H1_temx)
  Hy.putBlock(H_temy)
  H1y.putBlock(H1_temy)
  HH.setLabel([-10,-20,10,20])
  Hz.setLabel([-10,-20,10,20])
  H1z.setLabel([-10,-20,10,20])
  Hx.setLabel([-10,-20,10,20])
  H1x.setLabel([-10,-20,10,20])
  Hy.setLabel([-10,-20,10,20])
  H1y.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])

 if Model is "Heisenberg_Z2":
  #print d_phys
  bdi = uni10.Bond(uni10.BD_IN, d_phys)
  bdo = uni10.Bond(uni10.BD_OUT, d_phys)
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  sz = matSz()
  iden = matIden()
  szt=uni10.otimes(sz,iden)
  H.setRawElem(szt)
  szt1=uni10.otimes(iden,sz)
  H1.setRawElem(szt1)
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
 if Model is "Heisenberg_U1":
  bdi = uni10.Bond(uni10.BD_IN, d_phys)
  bdo = uni10.Bond(uni10.BD_OUT, d_phys)
  HH = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  sz = matSz()
  sx = matSx()
  sy = matSy()
  iden = matIden()
  HH_tem=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
  H_tem=uni10.otimes(sz,iden)#+uni10.otimes(sx,iden)#+(-1.0)*uni10.otimes(sy,iden)
  H1_tem=uni10.otimes(iden,sz)#+uni10.otimes(iden,sx)#+(-1.0)*uni10.otimes(iden,sy)
  HH.setRawElem(HH_tem)
  H.setRawElem(H_tem)
  H1.setRawElem(H1_tem)
  Iden=copy.copy(H)
  Iden.identity()
  #print HH_tem, HH, H_tem, H, H1_tem, H1
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
############################################################

 vec_down=make_down(c4,Ta3, Tb3,c3)
 vec_up=make_up(c1,Tb1, Ta1,c2)
 ap=make_ap_openindex(a_u)
 bp=make_ap_openindex(b_u)
 cp=make_ap_openindex(c_u)
 dp=make_ap_openindex(d_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]

# Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right)
# print "Corr_length",Corr_length
 #vec_up_copy=copy.copy(vec_up)
 #Corr_length=ED_up(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,vec_up_copy)
 #print "Corr_length",Corr_length
 #fileCorrLength.write(str(Corr_length)  + "\n")
 #fileCorrLength.flush()


 #print "\n"
#######################a-a#####################################################
 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list.append(2)
 Corr_val_list.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list.append(dis_val)
  Corr_val_list.append(Corr_val) 
###################################################################################

 #print "\n"
########################b-b#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list1.append(2)
 Corr_val_list1.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1.append(dis_val)
  Corr_val_list1.append(Corr_val) 
####################################################################################


 #print "\n"
########################c-c#####################################################

 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list2.append(2)
 Corr_val_list2.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2.append(dis_val)
  Corr_val_list2.append(Corr_val) 
####################################################################################

 #print "\n"
########################d-d#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list3.append(2)
 Corr_val_list3.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3.append(dis_val)
  Corr_val_list3.append(Corr_val) 
####################################################################################

#############################################################################################################


 dis_val_listo=[]
 Corr_val_listo=[]
 dis_val_list1o=[]
 Corr_val_list1o=[]
 dis_val_list2o=[]
 Corr_val_list2o=[]
 dis_val_list3o=[]
 Corr_val_list3o=[]

 #print "\n"

#######################a-co#####################################################

 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_listo.append(2)
# Corr_val_listo.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_listo.append(dis_val)
  Corr_val_listo.append(Corr_val) 

###################################################################################


 #print "\n"

#######################c-ao#####################################################
 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list1o.append(3)
 Corr_val_list1o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1o.append(dis_val)
  Corr_val_list1o.append(Corr_val) 
###################################################################################


 #print "\n"

########################b-do#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_list2.append(2)
# Corr_val_list2.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2o.append(dis_val)
  Corr_val_list2o.append(Corr_val) 
####################################################################################

 #print "\n"

########################d-bo#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list3o.append(3)
 Corr_val_list3o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3o.append(dis_val)
  Corr_val_list3o.append(Corr_val) 
####################################################################################


# print dis_val_list,'\n,\n'
# print Corr_val_list,'\n,\n'
# print Corr_val_list1,'\n,\n'
# print Corr_val_list2,'\n,\n'
# print Corr_val_list3,'\n,\n'

# print dis_val_listo,'\n,\n'
# print Corr_val_listo,'\n,\n'
# print Corr_val_list1o,'\n,\n'
# print Corr_val_list2o,'\n,\n'
# print Corr_val_list3o,'\n,\n'


 Corr_val_list_ave=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_list, Corr_val_list1, Corr_val_list2, Corr_val_list3)]
 #print Corr_val_list_ave,'\n,\n'


 Corr_val_list_avo=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_listo, Corr_val_list1o, Corr_val_list2o, Corr_val_list3o)]
 #print Corr_val_list_avo,'\n,\n'


 Corr_val_list_final=Corr_val_list_ave+Corr_val_list_avo
 dis_val_list_final=dis_val_list+dis_val_listo

 #print '\n,\n'
 #print dis_val_list_final
 #print Corr_val_list_final
 #print '\n,\n'

###############################################################################
 vec_down=make_down(c4,Ta3, Tb3,c3)
 vec_up=make_up(c1,Tb1, Ta1,c2)
 ap=make_ap_openindex1(a_u)
 bp=make_ap_openindex1(b_u)
 cp=make_ap_openindex1(c_u)
 dp=make_ap_openindex1(d_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]

# Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right)
# print "Corr_length",Corr_length
 #vec_up_copy=copy.copy(vec_up)
 #Corr_length=ED_up(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,vec_up_copy)
 #print "Corr_length",Corr_length
 #fileCorrLength.write(str(Corr_length)  + "\n")
 #fileCorrLength.flush()


 #print "\n"
#######################a-a#####################################################
 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list.append(2)
 Corr_val_list.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list.append(dis_val)
  Corr_val_list.append(Corr_val) 
###################################################################################

 #print "\n"
########################b-b#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list1.append(2)
 Corr_val_list1.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1.append(dis_val)
  Corr_val_list1.append(Corr_val) 
####################################################################################


 #print "\n"
########################c-c#####################################################

 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list2.append(2)
 Corr_val_list2.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2.append(dis_val)
  Corr_val_list2.append(Corr_val) 
####################################################################################

 #print "\n"
########################d-d#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list3.append(2)
 Corr_val_list3.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3.append(dis_val)
  Corr_val_list3.append(Corr_val) 
####################################################################################

#############################################################################################################


 dis_val_listo=[]
 Corr_val_listo=[]
 dis_val_list1o=[]
 Corr_val_list1o=[]
 dis_val_list2o=[]
 Corr_val_list2o=[]
 dis_val_list3o=[]
 Corr_val_list3o=[]

 #print "\n"

#######################a-co#####################################################

 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_listo.append(2)
# Corr_val_listo.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_listo.append(dis_val)
  Corr_val_listo.append(Corr_val) 

###################################################################################


 #print "\n"

#######################c-ao#####################################################
 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list1o.append(3)
 Corr_val_list1o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1o.append(dis_val)
  Corr_val_list1o.append(Corr_val) 
###################################################################################


 #print "\n"

########################b-do#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_list2.append(2)
# Corr_val_list2.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2o.append(dis_val)
  Corr_val_list2o.append(Corr_val) 
####################################################################################

 #print "\n"

########################d-bo#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list3o.append(3)
 Corr_val_list3o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3o.append(dis_val)
  Corr_val_list3o.append(Corr_val) 
####################################################################################


 #print dis_val_list,'\n,\n'
 #print Corr_val_list,'\n,\n'
 #print Corr_val_list1,'\n,\n'
 #print Corr_val_list2,'\n,\n'
 #print Corr_val_list3,'\n,\n'

 #print dis_val_listo,'\n,\n'
 #print Corr_val_listo,'\n,\n'
 #print Corr_val_list1o,'\n,\n'
 #print Corr_val_list2o,'\n,\n'
 #print Corr_val_list3o,'\n,\n'


 Corr_val_list_ave=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_list, Corr_val_list1, Corr_val_list2, Corr_val_list3)]
 #print Corr_val_list_ave,'\n,\n'


 Corr_val_list_avo=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_listo, Corr_val_list1o, Corr_val_list2o, Corr_val_list3o)]
 #print Corr_val_list_avo,'\n,\n'


 Corr_val_list_final1=Corr_val_list_ave+Corr_val_list_avo
 #dis_val_list_final=dis_val_list+dis_val_listo

 #print '\n,\n'
 #print dis_val_list_final
 #print Corr_val_list_final1
 #print '\n,\n'

######################################################

##########################################################################
 vec_down=make_down(c4,Ta3, Tb3,c3)
 vec_up=make_up(c1,Tb1, Ta1,c2)
 ap=make_ap_openindex2(a_u)
 bp=make_ap_openindex2(b_u)
 cp=make_ap_openindex2(c_u)
 dp=make_ap_openindex2(d_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]

# Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right)
# print "Corr_length",Corr_length
 #vec_up_copy=copy.copy(vec_up)
 #Corr_length=ED_up(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,vec_up_copy)
 #print "Corr_length",Corr_length
 #fileCorrLength.write(str(Corr_length)  + "\n")
 #fileCorrLength.flush()


 #print "\n"
#######################a-a#####################################################
 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list.append(2)
 Corr_val_list.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list.append(dis_val)
  Corr_val_list.append(Corr_val) 
###################################################################################

 #print "\n"
########################b-b#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list1.append(2)
 Corr_val_list1.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1.append(dis_val)
  Corr_val_list1.append(Corr_val) 
####################################################################################


 #print "\n"
########################c-c#####################################################

 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list2.append(2)
 Corr_val_list2.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2.append(dis_val)
  Corr_val_list2.append(Corr_val) 
####################################################################################

 #print "\n"
########################d-d#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list3.append(2)
 Corr_val_list3.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3.append(dis_val)
  Corr_val_list3.append(Corr_val) 
####################################################################################

#############################################################################################################


 dis_val_listo=[]
 Corr_val_listo=[]
 dis_val_list1o=[]
 Corr_val_list1o=[]
 dis_val_list2o=[]
 Corr_val_list2o=[]
 dis_val_list3o=[]
 Corr_val_list3o=[]

 #print "\n"

#######################a-co#####################################################

 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_listo.append(2)
# Corr_val_listo.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_listo.append(dis_val)
  Corr_val_listo.append(Corr_val) 

###################################################################################


 #print "\n"

#######################c-ao#####################################################
 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list1o.append(3)
 Corr_val_list1o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1o.append(dis_val)
  Corr_val_list1o.append(Corr_val) 
###################################################################################


 #print "\n"

########################b-do#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_list2.append(2)
# Corr_val_list2.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2o.append(dis_val)
  Corr_val_list2o.append(Corr_val) 
####################################################################################

 #print "\n"

########################d-bo#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list3o.append(3)
 Corr_val_list3o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3o.append(dis_val)
  Corr_val_list3o.append(Corr_val) 
####################################################################################


 #print dis_val_list,'\n,\n'
 #print Corr_val_list,'\n,\n'
 #print Corr_val_list1,'\n,\n'
 #print Corr_val_list2,'\n,\n'
 #print Corr_val_list3,'\n,\n'

 #print dis_val_listo,'\n,\n'
 #print Corr_val_listo,'\n,\n'
 #print Corr_val_list1o,'\n,\n'
 #print Corr_val_list2o,'\n,\n'
 #print Corr_val_list3o,'\n,\n'


 Corr_val_list_ave=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_list, Corr_val_list1, Corr_val_list2, Corr_val_list3)]
 #print Corr_val_list_ave,'\n,\n'


 Corr_val_list_avo=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_listo, Corr_val_list1o, Corr_val_list2o, Corr_val_list3o)]
 #print Corr_val_list_avo,'\n,\n'


 Corr_val_list_final2=Corr_val_list_ave+Corr_val_list_avo
 #dis_val_list_final=dis_val_list+dis_val_listo

 #print '\n,\n'
 #print dis_val_list_final
 #print Corr_val_list_final2
 #print '\n,\n'

######################################################

 Corr_val_list_fV=[ (sum(t)*(1.0/3.0)) for t in zip(Corr_val_list_final, Corr_val_list_final1, Corr_val_list_final2)]

 #for i in xrange(len(dis_val_list_final)):
  #fileCorr.write(str(dis_val_list_final[i])  + " " + str(Corr_val_list_f[i]) + "\n")
  #fileCorr.flush()

 return Corr_val_list_fV











def CorrelationHH(a_u,b_u,c_u,d_u,a,b,c,d,Env,D,h,d_phys,chi,Corner_method,Model,distance_final,fileCorrH,fileCorrLength,fileCorrV):

 c1,c2,c3,c4,Ta1,Ta2,Ta3,Ta4,Tb1,Tb2,Tb3,Tb4=Init_env(Env)

 if Model is "Heisenberg":
  bdi = uni10.Bond(uni10.BD_IN, 4)
  bdo = uni10.Bond(uni10.BD_OUT, 4)
  HH = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Iden = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Iden.identity()
  sz = matSz()
  sx = matSx()
  sy = matSy()
  iden = matIden()
  A=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+uni10.otimes(sy,sy)
  HH_tem=uni10.otimes(A,A)
  H_tem=uni10.otimes(A,uni10.otimes(iden,iden))
  H1_tem=uni10.otimes(uni10.otimes(iden,iden),A)
  HH.putBlock(HH_tem)
  H.putBlock(H_tem)
  H1.putBlock(H1_tem)
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
  Hz=copy.copy(H)
  H1z=copy.copy(H1)


  Hx=copy.copy(H)
  H1x=copy.copy(H1)
  Hy=copy.copy(H)
  H1y=copy.copy(H1)
  Hy.set_zero()
  Hx.set_zero()
  H1y.set_zero()
  H1x.set_zero()

 if Model is "Heisenberg_Z2":
  #print d_phys
  bdi = uni10.Bond(uni10.BD_IN, d_phys)
  bdo = uni10.Bond(uni10.BD_OUT, d_phys)
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  sz = matSz()
  iden = matIden()
  szt=uni10.otimes(sz,iden)
  H.setRawElem(szt)
  szt1=uni10.otimes(iden,sz)
  H1.setRawElem(szt1)
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
 if Model is "Heisenberg_U1":
  bdi = uni10.Bond(uni10.BD_IN, d_phys)
  bdo = uni10.Bond(uni10.BD_OUT, d_phys)
  HH = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Iden = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  sz = matSz()
  sx = matSx()
  sy = matSy()
  Iden.identity()
  iden = matIden()
  HH_tem=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
  H_tem=uni10.otimes(sz,iden)#+uni10.otimes(sx,iden)#+(-1.0)*uni10.otimes(sy,iden)
  H1_tem=uni10.otimes(iden,sz)#+uni10.otimes(iden,sx)#+(-1.0)*uni10.otimes(iden,sy)
  HH.setRawElem(HH_tem)
  H.setRawElem(H_tem)
  H1.setRawElem(H1_tem)
  Iden=copy.copy(H)
  Iden.identity()
  #print HH_tem, HH, H_tem, H, H1_tem, H1
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
#########################################################################
 vec_left=make_vleft(Tb4,Ta4,c1,c4)
 vec_right=make_vright(Ta2,Tb2,c2,c3)
 ap=make_ap_openindexx(a_u)
 bp=make_ap_openindexx(b_u)
 cp=make_ap_openindexx(c_u)
 dp=make_ap_openindexx(d_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]


# vec_right_copy=copy.copy(vec_right)
# Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right_copy)
# print "Corr_length",Corr_length
# fileCorrLength.write(str(Corr_length)  + "\n")
# fileCorrLength.flush()



 #print "\n"
#######################a-a#####################################################

 vec_left_1=Make_first_vecleft_a(vec_left, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)
 vec_right_1=Make_first_vecright_a(vec_right, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list.append(2)
 Corr_val_list.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list.append(dis_val)
  Corr_val_list.append(Corr_val) 
###################################################################################

 #print "\n"
#######################b-b#####################################################

 vec_left_1=Make_first_vecleft_b(vec_left, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)
 vec_right_1=Make_first_vecright_b(vec_right, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list1.append(2)
 Corr_val_list1.append(Corr_val)

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1.append(dis_val)
  Corr_val_list1.append(Corr_val) 
###################################################################################


 #print "\n"
#######################c-c#####################################################

 vec_left_1=Make_first_vecleft_c(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)
 vec_right_1=Make_first_vecright_c(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list2.append(2)
 Corr_val_list2.append(Corr_val)

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2.append(dis_val)
  Corr_val_list2.append(Corr_val) 
###################################################################################

 #print "\n"
#######################d-d#####################################################

 vec_left_1=Make_first_vecleft_d(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)
 vec_right_1=Make_first_vecright_d(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list3.append(2)
 Corr_val_list3.append(Corr_val)

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3.append(dis_val)
  Corr_val_list3.append(Corr_val) 
###################################################################################

############################################################################################################


 dis_val_listo=[]
 Corr_val_listo=[]
 dis_val_list1o=[]
 Corr_val_list1o=[]
 dis_val_list2o=[]
 Corr_val_list2o=[]
 dis_val_list3o=[]
 Corr_val_list3o=[]

 #print "\n"

######################a-bo#####################################################

 vec_left_1=Make_first_vecleft_a(vec_left, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)
 vec_right_1=Make_first_vecright_b(vec_right, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,3)
 dis_val_listo.append(3)
 Corr_val_listo.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_listo.append(dis_val)
  Corr_val_listo.append(Corr_val) 
##################################################################################


 #print "\n"

######################b-ao#####################################################

 vec_left_1=Make_first_vecleft_b(vec_left, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)
 vec_right_1=Make_first_vecright_a(vec_right, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)

 #Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,1)
 #dis_val_list1.append(2)
 #Corr_val_list1.append(Corr_val)

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1o.append(dis_val)
  Corr_val_list1o.append(Corr_val) 
##################################################################################


 #print "\n"

#######################c-do#####################################################

 vec_left_1=Make_first_vecleft_c(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)
 vec_right_1=Make_first_vecright_d(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,3)
 dis_val_list2o.append(3)
 Corr_val_list2o.append(Corr_val)

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2o.append(dis_val)
  Corr_val_list2o.append(Corr_val) 
###################################################################################

 #print "\n"

#######################d-co#####################################################

 vec_left_1=Make_first_vecleft_d(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)
 vec_right_1=Make_first_vecright_c(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
# dis_val_list3.append(2)
# Corr_val_list3.append(Corr_val)

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3o.append(dis_val)
  Corr_val_list3o.append(Corr_val) 
###################################################################################


 #print dis_val_list,'\n,\n'
 #print Corr_val_list,'\n,\n'
# #print Corr_val_list1,'\n,\n'
# print Corr_val_list2,'\n,\n'
# print Corr_val_list3,'\n,\n'

# print dis_val_listo,'\n,\n'
# print Corr_val_listo,'\n,\n'
# print Corr_val_list1o,'\n,\n'
# print Corr_val_list2o,'\n,\n'
# print Corr_val_list3o,'\n,\n'


 Corr_val_list_ave=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_list, Corr_val_list1, Corr_val_list2, Corr_val_list3)]
 #print Corr_val_list_ave,'\n,\n'


 Corr_val_list_avo=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_listo, Corr_val_list1o, Corr_val_list2o, Corr_val_list3o)]
 #print Corr_val_list_avo,'\n,\n'


 Corr_val_list_final=Corr_val_list_ave+Corr_val_list_avo
 dis_val_list_final=dis_val_list+dis_val_listo

 #print '\n,\n'
 #print dis_val_list_final
 #print Corr_val_list_final
 #print '\n,\n'



#############################################################################

 vec_left=make_vleft(Tb4,Ta4,c1,c4)
 vec_right=make_vright(Ta2,Tb2,c2,c3)
 ap=make_ap_openindexx1(a_u)
 bp=make_ap_openindexx1(b_u)
 cp=make_ap_openindexx1(c_u)
 dp=make_ap_openindexx1(d_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]


# vec_right_copy=copy.copy(vec_right)
# Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right_copy)
# print "Corr_length",Corr_length
# fileCorrLength.write(str(Corr_length)  + "\n")
# fileCorrLength.flush()



 #print "\n"
#######################a-a#####################################################

 vec_left_1=Make_first_vecleft_a(vec_left, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)
 vec_right_1=Make_first_vecright_a(vec_right, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list.append(2)
 Corr_val_list.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list.append(dis_val)
  Corr_val_list.append(Corr_val) 
###################################################################################

 #print "\n"
#######################b-b#####################################################

 vec_left_1=Make_first_vecleft_b(vec_left, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)
 vec_right_1=Make_first_vecright_b(vec_right, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list1.append(2)
 Corr_val_list1.append(Corr_val)

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1.append(dis_val)
  Corr_val_list1.append(Corr_val) 
###################################################################################


 #print "\n"
#######################c-c#####################################################

 vec_left_1=Make_first_vecleft_c(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)
 vec_right_1=Make_first_vecright_c(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list2.append(2)
 Corr_val_list2.append(Corr_val)

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2.append(dis_val)
  Corr_val_list2.append(Corr_val) 
###################################################################################

 #print "\n"
#######################d-d#####################################################

 vec_left_1=Make_first_vecleft_d(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)
 vec_right_1=Make_first_vecright_d(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list3.append(2)
 Corr_val_list3.append(Corr_val)

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3.append(dis_val)
  Corr_val_list3.append(Corr_val) 
###################################################################################

############################################################################################################


 dis_val_listo=[]
 Corr_val_listo=[]
 dis_val_list1o=[]
 Corr_val_list1o=[]
 dis_val_list2o=[]
 Corr_val_list2o=[]
 dis_val_list3o=[]
 Corr_val_list3o=[]

 #print "\n"

######################a-bo#####################################################

 vec_left_1=Make_first_vecleft_a(vec_left, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)
 vec_right_1=Make_first_vecright_b(vec_right, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,3)
 dis_val_listo.append(3)
 Corr_val_listo.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_listo.append(dis_val)
  Corr_val_listo.append(Corr_val) 
##################################################################################


 #print "\n"

######################b-ao#####################################################

 vec_left_1=Make_first_vecleft_b(vec_left, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)
 vec_right_1=Make_first_vecright_a(vec_right, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)

 #Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,1)
 #dis_val_list1.append(2)
 #Corr_val_list1.append(Corr_val)

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1o.append(dis_val)
  Corr_val_list1o.append(Corr_val) 
##################################################################################


 #print "\n"

#######################c-do#####################################################

 vec_left_1=Make_first_vecleft_c(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)
 vec_right_1=Make_first_vecright_d(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,3)
 dis_val_list2o.append(3)
 Corr_val_list2o.append(Corr_val)

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2o.append(dis_val)
  Corr_val_list2o.append(Corr_val) 
###################################################################################

 #print "\n"

#######################d-co#####################################################

 vec_left_1=Make_first_vecleft_d(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)
 vec_right_1=Make_first_vecright_c(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
# dis_val_list3.append(2)
# Corr_val_list3.append(Corr_val)

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3o.append(dis_val)
  Corr_val_list3o.append(Corr_val) 
###################################################################################


 #print dis_val_list,'\n,\n'
 #print Corr_val_list,'\n,\n'
# #print Corr_val_list1,'\n,\n'
# print Corr_val_list2,'\n,\n'
# print Corr_val_list3,'\n,\n'

# print dis_val_listo,'\n,\n'
# print Corr_val_listo,'\n,\n'
# print Corr_val_list1o,'\n,\n'
# print Corr_val_list2o,'\n,\n'
# print Corr_val_list3o,'\n,\n'


 Corr_val_list_ave=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_list, Corr_val_list1, Corr_val_list2, Corr_val_list3)]
 #print Corr_val_list_ave,'\n,\n'


 Corr_val_list_avo=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_listo, Corr_val_list1o, Corr_val_list2o, Corr_val_list3o)]
 #print Corr_val_list_avo,'\n,\n'


 Corr_val_list_final1=Corr_val_list_ave+Corr_val_list_avo
 #dis_val_list_final=dis_val_list+dis_val_listo

 #print '\n,\n'
 #print dis_val_list_final
 #print Corr_val_list_final1
 #print '\n,\n'

#####################################################################

 vec_left=make_vleft(Tb4,Ta4,c1,c4)
 vec_right=make_vright(Ta2,Tb2,c2,c3)
 ap=make_ap_openindexx2(a_u)
 bp=make_ap_openindexx2(b_u)
 cp=make_ap_openindexx2(c_u)
 dp=make_ap_openindexx2(d_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]


# vec_right_copy=copy.copy(vec_right)
# Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right_copy)
# print "Corr_length",Corr_length
# fileCorrLength.write(str(Corr_length)  + "\n")
# fileCorrLength.flush()



 #print "\n"
#######################a-a#####################################################
 #print ap.printDiagram()
 vec_left_1=Make_first_vecleft_a(vec_left, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)
 vec_right_1=Make_first_vecright_a(vec_right, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list.append(2)
 Corr_val_list.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list.append(dis_val)
  Corr_val_list.append(Corr_val) 
###################################################################################

 #print "\n"
#######################b-b#####################################################

 vec_left_1=Make_first_vecleft_b(vec_left, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)
 vec_right_1=Make_first_vecright_b(vec_right, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list1.append(2)
 Corr_val_list1.append(Corr_val)

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1.append(dis_val)
  Corr_val_list1.append(Corr_val) 
###################################################################################


 #print "\n"
#######################c-c#####################################################

 vec_left_1=Make_first_vecleft_c(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)
 vec_right_1=Make_first_vecright_c(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list2.append(2)
 Corr_val_list2.append(Corr_val)

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2.append(dis_val)
  Corr_val_list2.append(Corr_val) 
###################################################################################

 #print "\n"
#######################d-d#####################################################

 vec_left_1=Make_first_vecleft_d(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)
 vec_right_1=Make_first_vecright_d(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
 dis_val_list3.append(2)
 Corr_val_list3.append(Corr_val)

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3.append(dis_val)
  Corr_val_list3.append(Corr_val) 
###################################################################################

############################################################################################################


 dis_val_listo=[]
 Corr_val_listo=[]
 dis_val_list1o=[]
 Corr_val_list1o=[]
 dis_val_list2o=[]
 Corr_val_list2o=[]
 dis_val_list3o=[]
 Corr_val_list3o=[]

 #print "\n"

######################a-bo#####################################################

 vec_left_1=Make_first_vecleft_a(vec_left, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)
 vec_right_1=Make_first_vecright_b(vec_right, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,3)
 dis_val_listo.append(3)
 Corr_val_listo.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_listo.append(dis_val)
  Corr_val_listo.append(Corr_val) 
##################################################################################


 #print "\n"

######################b-ao#####################################################

 vec_left_1=Make_first_vecleft_b(vec_left, Ta1, Ta3,Tb1,Tb3,a,bp,c,d)
 vec_right_1=Make_first_vecright_a(vec_right, Ta1, Ta3,Tb1,Tb3,ap,b,c,d)

 #Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,1)
 #dis_val_list1.append(2)
 #Corr_val_list1.append(Corr_val)

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1o.append(dis_val)
  Corr_val_list1o.append(Corr_val) 
##################################################################################


 #print "\n"

#######################c-do#####################################################

 vec_left_1=Make_first_vecleft_c(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)
 vec_right_1=Make_first_vecright_d(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,3)
 dis_val_list2o.append(3)
 Corr_val_list2o.append(Corr_val)

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2o.append(dis_val)
  Corr_val_list2o.append(Corr_val) 
###################################################################################

 #print "\n"

#######################d-co#####################################################

 vec_left_1=Make_first_vecleft_d(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,c,dp)
 vec_right_1=Make_first_vecright_c(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,cp,d)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,2)
# dis_val_list3.append(2)
# Corr_val_list3.append(Corr_val)

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_left_1=Make_midle_vecleft(vec_left_1, Ta1, Ta3,Tb1,Tb3,a,b,c,d)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right_1, vec_left_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3o.append(dis_val)
  Corr_val_list3o.append(Corr_val) 
###################################################################################


 #print dis_val_list,'\n,\n'
 #print Corr_val_list,'\n,\n'
# #print Corr_val_list1,'\n,\n'
# print Corr_val_list2,'\n,\n'
# print Corr_val_list3,'\n,\n'

# print dis_val_listo,'\n,\n'
# print Corr_val_listo,'\n,\n'
# print Corr_val_list1o,'\n,\n'
# print Corr_val_list2o,'\n,\n'
# print Corr_val_list3o,'\n,\n'


 Corr_val_list_ave=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_list, Corr_val_list1, Corr_val_list2, Corr_val_list3)]
 #print Corr_val_list_ave,'\n,\n'


 Corr_val_list_avo=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_listo, Corr_val_list1o, Corr_val_list2o, Corr_val_list3o)]
 #print Corr_val_list_avo,'\n,\n'


 Corr_val_list_final2=Corr_val_list_ave+Corr_val_list_avo
 #dis_val_list_final=dis_val_list+dis_val_listo

 #print '\n,\n'
 #print dis_val_list_final
 #print Corr_val_list_final2
 #print '\n,\n'
#########################################################

 Corr_val_list_fH=[ (sum(t)*(1.0/3.0)) for t in zip(Corr_val_list_final, Corr_val_list_final1, Corr_val_list_final2)]


########################################################
 for i in xrange(len(dis_val_list_final)):
  fileCorrV.write(str(dis_val_list_final[i])  + " " + str(Corr_val_list_fH[i]) + "\n")
  fileCorrV.flush()
#####################################################


 Corr_val_list_fV=CorrelationVV(a_u,b_u,c_u,d_u,a,b,c,d,Env,D,h,d_phys,chi,Corner_method,Model,distance_final)

 Corr_val_list_f=[ (sum(t)*(1.0/2.0)) for t in zip(Corr_val_list_fH, Corr_val_list_fV)]

 print '\n,\n'
 print dis_val_list_final
 print Corr_val_list_f
 print '\n,\n'

########################################################
 for i in xrange(len(dis_val_list_final)):
  fileCorrH.write(str(dis_val_list_final[i])  + " " + str(Corr_val_list_f[i]) + "\n")
  fileCorrH.flush()
#####################################################







def CorrelationVV(a_u,b_u,c_u,d_u,a,b,c,d,Env,D,h,d_phys,chi,Corner_method,Model,distance_final):
 c1,c2,c3,c4, Ta1, Ta2, Ta3, Ta4, Tb1, Tb2, Tb3, Tb4=Init_env(Env)


 if Model is "Heisenberg":
  bdi = uni10.Bond(uni10.BD_IN, 4)
  bdo = uni10.Bond(uni10.BD_OUT, 4)
  HH = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Iden = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  Iden.identity()
  sz = matSz()
  sx = matSx()
  sy = matSy()
  iden = matIden()
  A=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+uni10.otimes(sy,sy)
  HH_tem=uni10.otimes(A,A)
  H_tem=uni10.otimes(A,uni10.otimes(iden,iden))
  H1_tem=uni10.otimes(uni10.otimes(iden,iden),A)
  HH.putBlock(HH_tem)
  H.putBlock(H_tem)
  H1.putBlock(H1_tem)
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
  Hz=copy.copy(H)
  H1z=copy.copy(H1)


  Hx=copy.copy(H)
  H1x=copy.copy(H1)
  Hy=copy.copy(H)
  H1y=copy.copy(H1)
  Hy.set_zero()
  Hx.set_zero()
  H1y.set_zero()
  H1x.set_zero()

 if Model is "Heisenberg_Z2":
  #print d_phys
  bdi = uni10.Bond(uni10.BD_IN, d_phys)
  bdo = uni10.Bond(uni10.BD_OUT, d_phys)
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  sz = matSz()
  iden = matIden()
  szt=uni10.otimes(sz,iden)
  H.setRawElem(szt)
  szt1=uni10.otimes(iden,sz)
  H1.setRawElem(szt1)
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
 if Model is "Heisenberg_U1":
  bdi = uni10.Bond(uni10.BD_IN, d_phys)
  bdo = uni10.Bond(uni10.BD_OUT, d_phys)
  HH = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  H1 = uni10.UniTensor(uni10.CTYPE,[bdi, bdi, bdo, bdo])
  sz = matSz()
  sx = matSx()
  sy = matSy()
  iden = matIden()
  HH_tem=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
  H_tem=uni10.otimes(sz,iden)#+uni10.otimes(sx,iden)#+(-1.0)*uni10.otimes(sy,iden)
  H1_tem=uni10.otimes(iden,sz)#+uni10.otimes(iden,sx)#+(-1.0)*uni10.otimes(iden,sy)
  HH.setRawElem(HH_tem)
  H.setRawElem(H_tem)
  H1.setRawElem(H1_tem)
  Iden=copy.copy(H)
  Iden.identity()
  #print HH_tem, HH, H_tem, H, H1_tem, H1
  HH.setLabel([-10,-20,10,20])
  H.setLabel([-10,-20,10,20])
  H1.setLabel([-10,-20,10,20])
  Iden.setLabel([-10,-20,10,20])
############################################################

 vec_down=make_down(c4,Ta3, Tb3,c3)
 vec_up=make_up(c1,Tb1, Ta1,c2)
 ap=make_ap_openindexx(a_u)
 bp=make_ap_openindexx(b_u)
 cp=make_ap_openindexx(c_u)
 dp=make_ap_openindexx(d_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]

# Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right)
# print "Corr_length",Corr_length
 #vec_up_copy=copy.copy(vec_up)
 #Corr_length=ED_up(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,vec_up_copy)
 #print "Corr_length",Corr_length
 #fileCorrLength.write(str(Corr_length)  + "\n")
 #fileCorrLength.flush()


 #print "\n"
#######################a-a#####################################################
 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list.append(2)
 Corr_val_list.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list.append(dis_val)
  Corr_val_list.append(Corr_val) 
###################################################################################

 #print "\n"
########################b-b#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list1.append(2)
 Corr_val_list1.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1.append(dis_val)
  Corr_val_list1.append(Corr_val) 
####################################################################################


 #print "\n"
########################c-c#####################################################

 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list2.append(2)
 Corr_val_list2.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2.append(dis_val)
  Corr_val_list2.append(Corr_val) 
####################################################################################

 #print "\n"
########################d-d#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list3.append(2)
 Corr_val_list3.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3.append(dis_val)
  Corr_val_list3.append(Corr_val) 
####################################################################################

#############################################################################################################


 dis_val_listo=[]
 Corr_val_listo=[]
 dis_val_list1o=[]
 Corr_val_list1o=[]
 dis_val_list2o=[]
 Corr_val_list2o=[]
 dis_val_list3o=[]
 Corr_val_list3o=[]

 #print "\n"

#######################a-co#####################################################

 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_listo.append(2)
# Corr_val_listo.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_listo.append(dis_val)
  Corr_val_listo.append(Corr_val) 

###################################################################################


 #print "\n"

#######################c-ao#####################################################
 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list1o.append(3)
 Corr_val_list1o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1o.append(dis_val)
  Corr_val_list1o.append(Corr_val) 
###################################################################################


 #print "\n"

########################b-do#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_list2.append(2)
# Corr_val_list2.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2o.append(dis_val)
  Corr_val_list2o.append(Corr_val) 
####################################################################################

 #print "\n"

########################d-bo#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list3o.append(3)
 Corr_val_list3o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3o.append(dis_val)
  Corr_val_list3o.append(Corr_val) 
####################################################################################


# print dis_val_list,'\n,\n'
# print Corr_val_list,'\n,\n'
# print Corr_val_list1,'\n,\n'
# print Corr_val_list2,'\n,\n'
# print Corr_val_list3,'\n,\n'

# print dis_val_listo,'\n,\n'
# print Corr_val_listo,'\n,\n'
# print Corr_val_list1o,'\n,\n'
# print Corr_val_list2o,'\n,\n'
# print Corr_val_list3o,'\n,\n'


 Corr_val_list_ave=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_list, Corr_val_list1, Corr_val_list2, Corr_val_list3)]
 #print Corr_val_list_ave,'\n,\n'


 Corr_val_list_avo=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_listo, Corr_val_list1o, Corr_val_list2o, Corr_val_list3o)]
 #print Corr_val_list_avo,'\n,\n'


 Corr_val_list_final=Corr_val_list_ave+Corr_val_list_avo
 dis_val_list_final=dis_val_list+dis_val_listo

 #print '\n,\n'
 #print dis_val_list_final
 #print Corr_val_list_final
 #print '\n,\n'

###############################################################################
 vec_down=make_down(c4,Ta3, Tb3,c3)
 vec_up=make_up(c1,Tb1, Ta1,c2)
 ap=make_ap_openindexx1(a_u)
 bp=make_ap_openindexx1(b_u)
 cp=make_ap_openindexx1(c_u)
 dp=make_ap_openindexx1(d_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]

# Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right)
# print "Corr_length",Corr_length
 #vec_up_copy=copy.copy(vec_up)
 #Corr_length=ED_up(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,vec_up_copy)
 #print "Corr_length",Corr_length
 #fileCorrLength.write(str(Corr_length)  + "\n")
 #fileCorrLength.flush()


 #print "\n"
#######################a-a#####################################################
 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list.append(2)
 Corr_val_list.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list.append(dis_val)
  Corr_val_list.append(Corr_val) 
###################################################################################

 #print "\n"
########################b-b#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list1.append(2)
 Corr_val_list1.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1.append(dis_val)
  Corr_val_list1.append(Corr_val) 
####################################################################################


 #print "\n"
########################c-c#####################################################

 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list2.append(2)
 Corr_val_list2.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2.append(dis_val)
  Corr_val_list2.append(Corr_val) 
####################################################################################

 #print "\n"
########################d-d#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list3.append(2)
 Corr_val_list3.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3.append(dis_val)
  Corr_val_list3.append(Corr_val) 
####################################################################################

#############################################################################################################


 dis_val_listo=[]
 Corr_val_listo=[]
 dis_val_list1o=[]
 Corr_val_list1o=[]
 dis_val_list2o=[]
 Corr_val_list2o=[]
 dis_val_list3o=[]
 Corr_val_list3o=[]

 #print "\n"

#######################a-co#####################################################

 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_listo.append(2)
# Corr_val_listo.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_listo.append(dis_val)
  Corr_val_listo.append(Corr_val) 

###################################################################################


 #print "\n"

#######################c-ao#####################################################
 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list1o.append(3)
 Corr_val_list1o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1o.append(dis_val)
  Corr_val_list1o.append(Corr_val) 
###################################################################################


 #print "\n"

########################b-do#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_list2.append(2)
# Corr_val_list2.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2o.append(dis_val)
  Corr_val_list2o.append(Corr_val) 
####################################################################################

 #print "\n"

########################d-bo#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list3o.append(3)
 Corr_val_list3o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3o.append(dis_val)
  Corr_val_list3o.append(Corr_val) 
####################################################################################


 #print dis_val_list,'\n,\n'
 #print Corr_val_list,'\n,\n'
 #print Corr_val_list1,'\n,\n'
 #print Corr_val_list2,'\n,\n'
 #print Corr_val_list3,'\n,\n'

 #print dis_val_listo,'\n,\n'
 #print Corr_val_listo,'\n,\n'
 #print Corr_val_list1o,'\n,\n'
 #print Corr_val_list2o,'\n,\n'
 #print Corr_val_list3o,'\n,\n'


 Corr_val_list_ave=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_list, Corr_val_list1, Corr_val_list2, Corr_val_list3)]
 #print Corr_val_list_ave,'\n,\n'


 Corr_val_list_avo=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_listo, Corr_val_list1o, Corr_val_list2o, Corr_val_list3o)]
 #print Corr_val_list_avo,'\n,\n'


 Corr_val_list_final1=Corr_val_list_ave+Corr_val_list_avo
 #dis_val_list_final=dis_val_list+dis_val_listo

 #print '\n,\n'
 #print dis_val_list_final
 #print Corr_val_list_final1
 #print '\n,\n'

######################################################

##########################################################################
 vec_down=make_down(c4,Ta3, Tb3,c3)
 vec_up=make_up(c1,Tb1, Ta1,c2)
 ap=make_ap_openindexx2(a_u)
 bp=make_ap_openindexx2(b_u)
 cp=make_ap_openindexx2(c_u)
 dp=make_ap_openindexx2(d_u)
 dis_val_list=[]
 Corr_val_list=[]
 dis_val_list1=[]
 Corr_val_list1=[]
 dis_val_list2=[]
 Corr_val_list2=[]
 dis_val_list3=[]
 Corr_val_list3=[]

# Corr_length=ED_right(c2, Ta2, Tb2, c3, a, b, c, d, Tb1, Ta1, Ta3, Tb3,vec_right)
# print "Corr_length",Corr_length
 #vec_up_copy=copy.copy(vec_up)
 #Corr_length=ED_up(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,vec_up_copy)
 #print "Corr_length",Corr_length
 #fileCorrLength.write(str(Corr_length)  + "\n")
 #fileCorrLength.flush()


 #print "\n"
#######################a-a#####################################################
 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list.append(2)
 Corr_val_list.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list.append(dis_val)
  Corr_val_list.append(Corr_val) 
###################################################################################

 #print "\n"
########################b-b#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list1.append(2)
 Corr_val_list1.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1.append(dis_val)
  Corr_val_list1.append(Corr_val) 
####################################################################################


 #print "\n"
########################c-c#####################################################

 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list2.append(2)
 Corr_val_list2.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2.append(dis_val)
  Corr_val_list2.append(Corr_val) 
####################################################################################

 #print "\n"
########################d-d#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
 dis_val_list3.append(2)
 Corr_val_list3.append(Corr_val) 

 dis_val=2
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3.append(dis_val)
  Corr_val_list3.append(Corr_val) 
####################################################################################

#############################################################################################################


 dis_val_listo=[]
 Corr_val_listo=[]
 dis_val_list1o=[]
 Corr_val_list1o=[]
 dis_val_list2o=[]
 Corr_val_list2o=[]
 dis_val_list3o=[]
 Corr_val_list3o=[]

 #print "\n"

#######################a-co#####################################################

 vec_down_1=Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_listo.append(2)
# Corr_val_listo.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_listo.append(dis_val)
  Corr_val_listo.append(Corr_val) 

###################################################################################


 #print "\n"

#######################c-ao#####################################################
 vec_down_1=Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list1o.append(3)
 Corr_val_list1o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list1o.append(dis_val)
  Corr_val_list1o.append(Corr_val) 
###################################################################################


 #print "\n"

########################b-do#####################################################

 vec_down_1=Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)

# Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,2)
# dis_val_list2.append(2)
# Corr_val_list2.append(Corr_val) 

 dis_val=1
 for i in xrange(distance_final+1):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list2o.append(dis_val)
  Corr_val_list2o.append(Corr_val) 
####################################################################################

 #print "\n"

########################d-bo#####################################################

 vec_down_1=Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4)
 vec_up_1=Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4)

 Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,3)
 dis_val_list3o.append(3)
 Corr_val_list3o.append(Corr_val) 

 dis_val=3
 for i in xrange(distance_final):
  dis_val+=2
  vec_down_1=Make_midle_vecdown(vec_down_1,a,b,c,d,Tb2, Ta2, Ta4, Tb4)
  Corr_val=Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_down_1, vec_up_1,dis_val)
  #print dis_val, Corr_val
  dis_val_list3o.append(dis_val)
  Corr_val_list3o.append(Corr_val) 
####################################################################################


 #print dis_val_list,'\n,\n'
 #print Corr_val_list,'\n,\n'
 #print Corr_val_list1,'\n,\n'
 #print Corr_val_list2,'\n,\n'
 #print Corr_val_list3,'\n,\n'

 #print dis_val_listo,'\n,\n'
 #print Corr_val_listo,'\n,\n'
 #print Corr_val_list1o,'\n,\n'
 #print Corr_val_list2o,'\n,\n'
 #print Corr_val_list3o,'\n,\n'


 Corr_val_list_ave=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_list, Corr_val_list1, Corr_val_list2, Corr_val_list3)]
 #print Corr_val_list_ave,'\n,\n'


 Corr_val_list_avo=[ (sum(t)*(1.0/4.0)) for t in zip(Corr_val_listo, Corr_val_list1o, Corr_val_list2o, Corr_val_list3o)]
 #print Corr_val_list_avo,'\n,\n'


 Corr_val_list_final2=Corr_val_list_ave+Corr_val_list_avo
 #dis_val_list_final=dis_val_list+dis_val_listo

 #print '\n,\n'
 #print dis_val_list_final
 #print Corr_val_list_final2
 #print '\n,\n'

######################################################

 Corr_val_list_fV=[ (sum(t)*(1.0/3.0)) for t in zip(Corr_val_list_final, Corr_val_list_final1, Corr_val_list_final2)]

 #for i in xrange(len(dis_val_list_final)):
  #fileCorr.write(str(dis_val_list_final[i])  + " " + str(Corr_val_list_f[i]) + "\n")
  #fileCorr.flush()

 return Corr_val_list_fV













def Mat_np_to_Uni(Mat_np):
 d0=np.size(Mat_np,0)
 d1=np.size(Mat_np,1)
 Mat_uni=uni10.CMatrix(d0,d1)
 for i in xrange(d0):
  for j in xrange(d1):
   Mat_uni[i*d1+j]=Mat_np[i,j]
   #print "reza", Mat_np[i,j], Mat_uni[i*d1+j] 
 return  Mat_uni


def Mat_nptoUni(Mat_np):
 d0=np.size(Mat_np,0)
 Mat_uni=uni10.CMatrix(d0,d0, True)
 for i in xrange(d0):
   Mat_uni[i]=Mat_np[i]
 return  Mat_uni

 
def Mat_uni_to_np(Mat_uni):
 dim0=int(Mat_uni.row())
 dim1=int(Mat_uni.col())
 Mat_np=np.zeros((dim0,dim1),dtype = complex)
 for i in xrange(dim0):
  for j in xrange(dim1):
   Mat_np[i,j]=Mat_uni[i*dim1+j]
 return  Mat_np


def Multy(Mat_uni, val):
 dim0=int(Mat_uni.row())
 dim1=int(Mat_uni.col())
 r_uni=uni10.CMatrix(dim0,dim1)
 for i in xrange(dim0):
  for j in xrange(dim1):
   r_uni[i*dim1+j]=Mat_uni[i*dim1+j]*val
 return  r_uni



def eig_np(A):
 A_np=Mat_uni_to_np(A)
 w= LA.eigvals(A_np)
 return w



def eig_np_full(A):
 D_eig=[A]*2
 A_np=Mat_uni_to_np(A)
 w, v = LA.eig(A_np)
 #print w,"\n,\n", v
 D_eig[0]=Mat_nptoUni(w)
 D_eig[1]=Mat_np_to_Uni(v)
 return w, D_eig



def  make_Q(q_vec): 
 D=int(q_vec[0].row())
 m=len(q_vec)
 Q=uni10.CMatrix(D, m)
 for i in xrange(m):
  for j in xrange(D):
    Q[j*m+i]=q_vec[i][j]
 return Q

def return_vec(A, index ):
 D=int(A.row())
 vec_tem=uni10.CMatrix(D,1)
 for i in xrange(D): 
  vec_tem[i]=A[i*D+index]
  #print "A", A[i*D+index], vec_tem[i]
 return vec_tem


def find_maxindex(A):
 D=int(A.row())
 max_val=0
 index=0
 #print A
 for i in xrange(D):
  if (i == 0) or ( max_val < abs(A[i]) ):
   max_val = abs(A[i])
   index=i
 return max_val, index, A[index] 




def ED_up(c1,Tb1, Ta1,c2, a, b, c, d, Tb2, Ta2, Ta4, Tb4,Vec_uni):


 Vec_F=Vec_uni.getBlock()
 D=Vec_F.row()
 #print "D=",  D

 m=10
 W=2
 num=0
 E1=0
 p=0

 while p  <  (W+1):
  r=copy.copy(Vec_F)
  #r = r* (1.00/r.norm()) 
  
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()

  for j in xrange(m):
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(vec_tem)
   r=Multi_u(Vec_uni,a, b, c, d, Tb2, Ta2, Ta4, Tb4)
   #r.resize(D,1)
   for i in xrange(j+1):

    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace(uni10.CTYPE)
    
    r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 

  D_eig=eig_np(h)
  Lambda, index, Lambda_comp=find_maxindex(D_eig[0])
  eigvec=return_vec(D_eig[1], index )
  print 'u0', Lambda,Lambda_comp

  Q=make_Q(q_vec)
  Q.resize(D,m)
  Vec_F=Q*eigvec
  Vec_FL=Q*eigvec
  if p==W and num==0:
   p=-1
   m+=5
   E1=copy.copy(Lambda)
   num+=1
  elif p==W:
   num+=1
   if abs(Lambda) > 1.e-9: 
    if  (((abs(Lambda-E1))/(abs(Lambda)))< 1.e-9): num+=1
    elif m<=20:
     p=-1
     m=m+5
     E1=Lambda
   else:
    if  (abs(Lambda-E1))< 1.e-9:
     num+=1
    elif m<=20: 
     p=-1
     m=m+5
     E1=Lambda
  p+=1

 E1L=copy.copy(E1)
 Vec_FL=Vec_FL*(1.00/Vec_FL.norm())
 m=10
 W=2
 num=0
 E1=0
 p=0
 Vec_F.randomize()
 Vec_F=Vec_F*(1.00/Vec_F.norm())
 while p  <  (W+1):
  #print "norm", p, Vec_F.norm(),Vec_F[0], Vec_F[1], Vec_F[2], Vec_F[3] 
  r=copy.copy(Vec_F)
 
  Vec_FL_trans=copy.copy(Vec_FL)
  Vec_FL_trans.cTranspose()
  dot_vec=Vec_FL_trans*r
  dot_val=dot_vec.trace()
  r=r+(-1.00*dot_val*Vec_FL)
  
  
  #r = r* (1.00/r.norm()) 
  q_vec=[]
  q_vec.append(copy.copy(r))
  h=uni10.CMatrix(m,m)
  h.set_zero()
  for j in xrange(m):
   vec_tem=copy.copy(q_vec[j])
   Vec_uni.putBlock(vec_tem)
   r=Multi_u(Vec_uni,a, b, c, d, Tb2, Ta2, Ta4, Tb4)

   Vec_FL_trans=copy.copy(Vec_FL)
   Vec_FL_trans.cTranspose()
   dot_vec=Vec_FL_trans*r
   dot_val=dot_vec.trace()
   r=r+(-1.00*dot_val*Vec_FL)



   for i in xrange(j+1):
    q_vec_trans=copy.copy(q_vec[i])
    q_vec_trans.cTranspose()
    dot_vec=q_vec_trans*r
    h[i*m+j]=dot_vec.trace()
    r=r+((-1.00)*(h[i*m+j]*q_vec[i]))
   if j<(m-1):
    h[((j+1)*m)+j]=r.norm()
    if r.norm() > 1.0e-8:
     q_vec.append(r*(1.00/r.norm()))
    else:  break; 
  D_eig=eig_np(h)
  Lambda, index,Lambda_comp=find_maxindex(D_eig[0])
  eigvec=return_vec(D_eig[1], index )
  print 'u1', Lambda,Lambda_comp
  Q=make_Q(q_vec)
  Q.resize(D,m)
  Vec_F=Q*eigvec
  if p==W and num==0:
   p=-1
   m+=5
   E1=copy.copy(Lambda)
   num+=1
  elif p==W:
   num+=1
   if abs(Lambda) > 1.e-9: 
    if  (((abs(Lambda-E1))/(abs(Lambda)))< 1.e-9): num+=1
    elif m<=20:
     p=-1
     m+=5
     E1=Lambda
   else:
    if  (abs(Lambda-E1))< 1.e-9:
     num+=1
    elif m<=20: 
     p=-1
     m+=5
     E1=Lambda
  p+=1


 Length=abs(E1/E1L)
 Length_val=-2.0*(1.00/math.log(Length))
 print "Length", Length,Length_val 
 return Length_val

def Multi_r(Vec_uni,a,b,c,d,Tb1,Ta1,Ta3,Tb3):
 CTM_1 = uni10.Network("Network1/Right.net")
 CTM_1.putTensor('Vec_uni',Vec_uni)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 vec.permute([21,14,-14 , 7,-7,0],6)
 #print vec.printDiagram() 
 Vec_M=vec.getBlock()
 return Vec_M


def Multi_full_right(Vec_uni,a,b):
 q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
 CTM_1 = uni10.Network("Network1/Full.net")
 CTM_1.putTensor('Vec_uni',Vec_uni)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('a1',a)
 CTM_1.putTensor('b1',b)
 CTM_1.putTensor('a2',a)
 CTM_1.putTensor('b2',b)
 vec=CTM_1.launch()
 #print CTM_1
 vec.setLabel([3,-3,6,-6,9,-9,12,-12,15,-15,18,-18,202]) 
 vec.permute([3,-3,6,-6,9,-9,12,-12,15,-15,18,-18,202],12)
 #print vec.printDiagram() 
 Vec_M=vec.getBlock(q0_even)
 return Vec_M


#@profile
def Multi_r0(Env, Env4,Env8,vec_right,Arnoldi_bond,a_u,b_u):

 vec_right.setLabel([9,11,22,24])
 a_u.setLabel([53,16,15,13,4])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([20,2,53,18,19])

 b_u.setLabel([54,13,14,11,8])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([22,6,54,20,21])

 Env[0].setLabel([0,1])
 Env[1].setLabel([1,2,3])
 Env[2].setLabel([3,4,5])
 Env[11].setLabel([17,16,0])
 Env[10].setLabel([29,18,17])
 Env[9].setLabel([28,29])
 Env[8].setLabel([27,19,28])
 Env[7].setLabel([26,15,27])

 Env4[2].setLabel([5,6,7])
 Env4[7].setLabel([25,21,26])

 Env8[2].setLabel([7,8,9])
 Env8[7].setLabel([24,14,25])
 Env8[3].setLabel([9,10])
 Env8[4].setLabel([10,11,12])
 Env8[5].setLabel([12,22,23])
 Env8[6].setLabel([23,24])

 #print vec_right.printDiagram(), Env8[2].printDiagram() 
 vec=(((((((((((((vec_right)*Env8[2])*b_u)*Env8[7])*Env4[7])*b_d)*Env4[2])*Env[2])*a_u)*Env[7])*Env[8])*a_d)*Env[1])
 vec.permute([1,16,18 , 28],4)


 #vec.permute([21,14,-14 , 7,-7,0,202],6)
 #print vec.printDiagram() 
 Vec_M=vec.getBlock()
 return Vec_M






#@profile
def Multi_r0_eff(Env,Env4,Env8,Env2,Env6,Env10,vec_right,Arnoldi_bond):

 vec_right.setLabel([9,14])

 Env[1].setLabel([1,2,3])
 Env[2].setLabel([3,4,5])

 Env4[2].setLabel([5,6,7])

 Env8[2].setLabel([7,8,9])

 Env2[8].setLabel([11,2,10])
 Env2[7].setLabel([12,4,11])
 Env6[7].setLabel([13,6,12])
 Env10[7].setLabel([14,8,13])

 #print vec_right.printDiagram(), Env8[2].printDiagram() 
 vec=((((((((vec_right*Env8[2])*(Env10[7]))*Env4[2])*Env6[7])*Env[2])*Env2[7])*Env[1])*Env2[8])
 vec.permute([1, 10],2)


 #vec.permute([21,14,-14 , 7,-7,0,202],6)
 #print vec.printDiagram() 
 Vec_M=vec.getBlock()
 return Vec_M





def Multi_r0up(Env2, Env1,Env,vec_right,Arnoldi_bond,c_u,a_u):

 vec_right.setLabel([0,2,4,6])
 a_u.setLabel([54,9,8,7,4])
 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([13,2,54,11,12])

 c_u.setLabel([53,18,17,16,8])
 c_d=copy.copy(c_u)
 c_d.cTranspose()
 c_d.setLabel([23,12,53,21,22])

 Env[0].setLabel([0,1])
 Env[1].setLabel([1,2,3])
 Env[2].setLabel([3,4,5])
 Env[11].setLabel([10,9,0])
 Env[10].setLabel([19,11,10])
 Env[3].setLabel([5,6])
 Env[4].setLabel([6,7,14])
 Env[5].setLabel([14,13,15])

 Env1[5].setLabel([15,16,24])
 Env1[10].setLabel([20,18,19])

 Env2[5].setLabel([24,23,25])
 Env2[10].setLabel([29,21,20])
 Env2[6].setLabel([25,26])
 Env2[7].setLabel([26,17,27])
 Env2[8].setLabel([27,22,28])
 Env2[9].setLabel([28,29])



 vec=(((((((((((((vec_right)*Env[11])*a_u)*Env[4])*Env[5])*a_d)*Env[10])*Env1[10])*c_u)*Env1[5])*Env2[10])*c_d)*Env2[5])
 vec.permute([29,22,17,25],4)


 #vec.permute([21,14,-14 , 7,-7,0,202],6)
 #print vec.printDiagram()
 Vec_M=vec.getBlock()
 return Vec_M


def Multi_r0up_eff(Env2, Env1,Env,Env10, Env9,Env8,vec_right,Arnoldi_bond):

 vec_right.setLabel([9,10])

 Env[11].setLabel([7,8,9])
 Env[10].setLabel([5,6,7])
 Env1[10].setLabel([3,4,5])
 Env2[10].setLabel([1,2,3])

 Env8[4].setLabel([10,8,12])
 Env8[5].setLabel([12,6,14])
 Env9[5].setLabel([14,4,16])
 Env10[5].setLabel([16,2,18])

 vec=((((((((Env[11]*vec_right)*Env8[4])*Env[10])*Env8[5])*Env1[10])*Env9[5])*Env2[10])*Env10[5])

 vec.permute([1,18],2)

 #vec.permute([21,14,-14 , 7,-7,0,202],6)
 #print vec.printDiagram() 
 Vec_M=vec.getBlock()
 return Vec_M



def Multi_r2(Vec_uni,a,b,c,d,Tb1,Ta1,Ta3,Tb3):
 q0_even = uni10.Qnum(2,uni10.PRT_EVEN);
 CTM_1 = uni10.Network("Network1/Right2.net")
 CTM_1.putTensor('Vec_uni',Vec_uni)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 vec.permute([21,14,-14 , 7,-7,0,202],6)
 #print vec.printDiagram() 
 Vec_M=vec.getBlock(q0_even)
 return Vec_M

def Multi_r4(Vec_uni,a,b,c,d,Tb1,Ta1,Ta3,Tb3):
 q0_even = uni10.Qnum(-2,uni10.PRT_EVEN);
 CTM_1 = uni10.Network("Network1/Right2.net")
 CTM_1.putTensor('Vec_uni',Vec_uni)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 vec.permute([21,14,-14 , 7,-7,0,202],6)
 #print vec.printDiagram() 
 Vec_M=vec.getBlock(q0_even)
 return Vec_M


def Multi_u(Vec_uni,a, b, c, d, Tb2, Ta2, Ta4, Tb4):
 CTM_1 = uni10.Network("Network1/Up.net")
 CTM_1.putTensor('Vec_uni',Vec_uni)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 vec.permute([17, 18, -18, 19, -19,  20],6)
 Vec_M=vec.getBlock()
 return Vec_M


def Multi_u0(Vec_uni,a, b, c, d, Tb2, Ta2, Ta4, Tb4):
 q0_even = uni10.Qnum(0,uni10.PRT_EVEN);
 CTM_1 = uni10.Network("Network1/Up2.net")
 CTM_1.putTensor('Vec_uni',Vec_uni)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 vec.permute([17, 18, -18, 19, -19,  20,202],6)
 Vec_M=vec.getBlock(q0_even)
 return Vec_M

def Multi_u2(Vec_uni,a, b, c, d, Tb2, Ta2, Ta4, Tb4):
 q0_even = uni10.Qnum(2,uni10.PRT_EVEN);
 CTM_1 = uni10.Network("Network1/Up2.net")
 CTM_1.putTensor('Vec_uni',Vec_uni)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 vec.permute([17, 18, -18, 19, -19,  20,202],6)
 Vec_M=vec.getBlock(q0_even)
 return Vec_M

def Multi_u4(Vec_uni,a, b, c, d, Tb2, Ta2, Ta4, Tb4):
 q0_even = uni10.Qnum(-2,uni10.PRT_EVEN);
 CTM_1 = uni10.Network("Network1/Up2.net")
 CTM_1.putTensor('Vec_uni',Vec_uni)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 vec.permute([17, 18, -18, 19, -19,  20,202],6)
 Vec_M=vec.getBlock(q0_even)
 return Vec_M


#################################################################################


def  Corr_val_function(Iden,HH,Hz,H1z,Hx,H1x,Hy,H1y,vec_right, vec_left,dis_val):

 vec_left.setLabel([10,23,16,-16,9,-9,2,-10])
 vec_right.setLabel([20,23,16,-16,9,-9,2,-20])
 #print vec_right.printDiagram(), vec_left.printDiagram(), Iden.printDiagram() 
 Cor_norm=(vec_left*Iden)*vec_right
 Corr_val=(vec_left*HH)*vec_right
 Corr_val1z=(vec_left*Hz)*vec_right
 Corr_val2z=(vec_left*H1z)*vec_right
 Corr_val1x=(vec_left*Hy)*vec_right
 Corr_val2x=(vec_left*H1y)*vec_right
 Corr_val1y=(vec_left*Hx)*vec_right
 Corr_val2y=(vec_left*H1x)*vec_right

 #print  dis_val.real, Corr_val[0].real/Cor_norm[0].real,Corr_val1[0].real/Cor_norm[0].real,Corr_val2[0].real/Cor_norm[0].real

 #print dis_val.real, (Corr_val[0].real/Cor_norm[0].real)-((Corr_val1z[0].real*Corr_val2z[0].real)/(Cor_norm[0].real*Cor_norm[0].real))-((Corr_val1y[0].real*Corr_val2y[0].real)/(Cor_norm[0].real*Cor_norm[0].real))-((Corr_val1x[0].real*Corr_val2x[0].real)/(Cor_norm[0].real*Cor_norm[0].real))  

 val=(Corr_val[0].real/Cor_norm[0].real)-((Corr_val1z[0].real*Corr_val2z[0].real)/(Cor_norm[0].real*Cor_norm[0].real))-((Corr_val1y[0].real*Corr_val2y[0].real)/(Cor_norm[0].real*Cor_norm[0].real))-((Corr_val1x[0].real*Corr_val2x[0].real)/(Cor_norm[0].real*Cor_norm[0].real))  

 return val.real


def  make_ap_openindex(a_up):
 bdi2 = uni10.Bond(uni10.BD_IN, 2)
 bdi4 = uni10.Bond(uni10.BD_IN, 4)

 bdo2 = uni10.Bond(uni10.BD_OUT, 2)
 bdo4 = uni10.Bond(uni10.BD_OUT, 4)
 a_u=uni10.UniTensor(uni10.CTYPE,[bdi2,bdi4,a_up.bond(1),a_up.bond(2),a_up.bond(3),a_up.bond(4)])
 a_u.putBlock(a_up.getBlock())

 a_u.setLabel([10,20,1,2,3,4])
 a_uc=copy.copy(a_u)
 a_uc.cTranspose()
 a_uc.setLabel([-3,-4,-10,20,-1,-2])
 result=a_uc*a_u
 result.permute([10,1,-1,2,-2,3,-3,4,-4,-10], 5)
 #print "results", result.printDiagram()
 return result


def  make_ap_openindex1(a_up):
 bdi2 = uni10.Bond(uni10.BD_IN, 2)
 bdi4 = uni10.Bond(uni10.BD_IN, 4)

 bdo2 = uni10.Bond(uni10.BD_OUT, 2)
 bdo4 = uni10.Bond(uni10.BD_OUT, 4)

 a_u=uni10.UniTensor(uni10.CTYPE,[bdi4,bdi2,a_up.bond(1),a_up.bond(2),a_up.bond(3),a_up.bond(4)])
 a_u.putBlock(a_up.getBlock())


 a_u.setLabel([20,10,1,2,3,4])
 a_uc=copy.copy(a_u)
 a_uc.cTranspose()
 a_uc.setLabel([-3,-4,20,-10,-1,-2])
 result=a_uc*a_u
 result.permute([10,1,-1,2,-2,3,-3,4,-4,-10], 5)
 return result


def  make_ap_openindex2(a_up):
 bdi2 = uni10.Bond(uni10.BD_IN, 2)
 bdi4 = uni10.Bond(uni10.BD_IN, 4)

 bdo2 = uni10.Bond(uni10.BD_OUT, 2)
 bdo4 = uni10.Bond(uni10.BD_OUT, 4)

 a_u=uni10.UniTensor(uni10.CTYPE,[bdi2,bdi2,bdi2,a_up.bond(1),a_up.bond(2),a_up.bond(3),a_up.bond(4)])
 a_u.putBlock(a_up.getBlock())


 a_u.setLabel([20,10,25,1,2,3,4])
 a_uc=copy.copy(a_u)
 a_uc.cTranspose()
 a_uc.setLabel([-3,-4,20,-10,25,-1,-2])
 result=a_uc*a_u
 result.permute([10,1,-1,2,-2,3,-3,4,-4,-10], 5)
 return result




def  make_ap_openindexx(a_up):
 bdi2 = uni10.Bond(uni10.BD_IN, 2)
 bdi4 = uni10.Bond(uni10.BD_IN, 4)

 bdo2 = uni10.Bond(uni10.BD_OUT, 2)
 bdo4 = uni10.Bond(uni10.BD_OUT, 4)

 a_u=uni10.UniTensor(uni10.CTYPE,[bdi4,bdi2,a_up.bond(1),a_up.bond(2),a_up.bond(3),a_up.bond(4)])
 a_u.putBlock(a_up.getBlock())


 a_u.setLabel([10,20,1,2,3,4])
 a_uc=copy.copy(a_u)
 a_uc.cTranspose()
 a_uc.setLabel([-3,-4,-10,20,-1,-2])
 result=a_uc*a_u
 result.permute([10,1,-1,2,-2,3,-3,4,-4,-10], 5)
 #print "results", result.printDiagram()
 return result


def  make_ap_openindexx1(a_up):
 bdi2 = uni10.Bond(uni10.BD_IN, 2)
 bdi4 = uni10.Bond(uni10.BD_IN, 4)

 bdo2 = uni10.Bond(uni10.BD_OUT, 2)
 bdo4 = uni10.Bond(uni10.BD_OUT, 4)

 a_u=uni10.UniTensor(uni10.CTYPE,[bdi2,bdi4,a_up.bond(1),a_up.bond(2),a_up.bond(3),a_up.bond(4)])
 a_u.putBlock(a_up.getBlock())


 a_u.setLabel([20,10,1,2,3,4])
 a_uc=copy.copy(a_u)
 a_uc.cTranspose()
 a_uc.setLabel([-3,-4,20,-10,-1,-2])
 result=a_uc*a_u
 result.permute([10,1,-1,2,-2,3,-3,4,-4,-10], 5)
 return result


def  make_ap_openindexx2(a_up):
 bdi2 = uni10.Bond(uni10.BD_IN, 2)
 bdi4 = uni10.Bond(uni10.BD_IN, 4)

 bdo2 = uni10.Bond(uni10.BD_OUT, 2)
 bdo4 = uni10.Bond(uni10.BD_OUT, 4)

 a_u=uni10.UniTensor(uni10.CTYPE,[bdi2,bdi2,bdi2,a_up.bond(1),a_up.bond(2),a_up.bond(3),a_up.bond(4)])
 a_u.putBlock(a_up.getBlock())

 a_u.setLabel([20,10,25,1,2,3,4])
 a_uc=copy.copy(a_u)
 a_uc.cTranspose()
 a_uc.setLabel([-3,-4,-20,10,-25,-1,-2])
 result=a_uc*a_u
 result.permute([20,25,1,-1,2,-2,3,-3,4,-4,-20,-25], 5)

 resultp=uni10.UniTensor(uni10.CTYPE,[bdi4,result.bond(2),result.bond(3),result.bond(4),result.bond(5),result.bond(6), result.bond(7), result.bond(8),result.bond(9),bdo4])
 resultp.putBlock(result.getBlock())
 resultp.setLabel([10,1,-1,2,-2,3,-3,4,-4,-10])
 resultp.permute([10,1,-1,2,-2,3,-3,4,-4,-10], 5)
 
 return resultp



def make_vleft(Env):
 Env[0].setLabel([0,1])
 Env[11].setLabel([17,16,0])
 Env[10].setLabel([29,18,17])
 Env[9].setLabel([28,29])
 vec_left=(Env[0]*Env[11])*(Env[10]*Env[9])

 vec_left.permute([1,16,18,28],0)
 return vec_left

def  make_vright(Env8):

 Env8[3].setLabel([9,10])
 Env8[4].setLabel([10,11,12])
 Env8[5].setLabel([12,22,23])
 Env8[6].setLabel([23,24])
 vec_right=(Env8[3]*Env8[4])*(Env8[5]*Env8[6])
 vec_right.permute([9,11,22,24],4)
 return vec_right


def  Make_first_vecleft_a(vec_left, Env,Env4,Env8, a_up,b_up,a_u,b_u):

 vec_left.setLabel([1,16,18,28])
 
 a_up.setLabel([53,16,15,13,4])

 a_d=copy.copy(a_u)
 a_d.cTranspose()
 a_d.setLabel([20,2,53,18,19])

 b_up.setLabel([54,13,14,11,8])
 b_d=copy.copy(b_u)
 b_d.cTranspose()
 b_d.setLabel([22,6,54,20,21])

 Env[0].setLabel([0,1])
 Env[1].setLabel([1,2,3])
 Env[2].setLabel([3,4,5])
 Env[11].setLabel([17,16,0])
 Env[10].setLabel([29,18,17])
 Env[9].setLabel([28,29])
 Env[8].setLabel([27,19,28])
 Env[7].setLabel([26,15,27])

 Env4[2].setLabel([5,6,7])
 Env4[7].setLabel([25,21,26])

 Env8[2].setLabel([7,8,9])
 Env8[7].setLabel([24,14,25])
 Env8[3].setLabel([9,10])
 Env8[4].setLabel([10,11,12])
 Env8[5].setLabel([12,22,23])
 Env8[6].setLabel([23,24])



 vec=(((((((((((((vec_left)*Env[8])*a_d)*Env[1])*Env[2])*a_up)*Env[7])*Env4[7])*b_d)*Env4[2])*Env8[2])*b_up)*Env8[7])


 vec.permute([9,11, 22, 24],0)
 #vec=max_ten(vec)
 return vec


def Make_first_vecright_a(vec_right, Ta1, Ta3,Tb1,Tb3,ap,b,c,d):
 CTM_1 = uni10.Network("Network1/RightCorra.net")
 CTM_1.putTensor('vec_right',vec_right)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',ap)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 vec.permute([10,21,14,-14 , 7,-7,0,-10],6)
 vec=max_ten(vec)
 return vec

def  Make_first_vecleft_b(vec_left, Ta1, Ta3,Tb1,Tb3,a,bp,c,d):
 CTM_1 = uni10.Network("Network1/LeftCorrb.net")
 CTM_1.putTensor('vec_left',vec_left)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',bp)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 vec.permute([10,23, 16, -16 , 9,-9, 2,-10],0)
 vec=max_ten(vec)
 return vec


def Make_first_vecright_b(vec_right, Ta1, Ta3,Tb1,Tb3,a,bp,c,d):
 CTM_1 = uni10.Network("Network1/RightCorrb.net")
 CTM_1.putTensor('vec_right',vec_right)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',bp)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 vec.permute([10,21,14,-14 , 7,-7,0,-10],6)
 vec=max_ten(vec)
 return vec


def  Make_first_vecleft_c(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,cp,d):
 CTM_1 = uni10.Network("Network1/LeftCorrc.net")
 CTM_1.putTensor('vec_left',vec_left)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',cp)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 vec.permute([10,23, 16, -16 , 9,-9, 2,-10],0)
 vec=max_ten(vec)
 return vec


def Make_first_vecright_c(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,cp,d):
 CTM_1 = uni10.Network("Network1/RightCorrc.net")
 CTM_1.putTensor('vec_right',vec_right)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',cp)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 vec.permute([10,21,14,-14 , 7,-7,0,-10],6)
 vec=max_ten(vec)
 return vec


def  Make_first_vecleft_d(vec_left, Ta1, Ta3,Tb1,Tb3,a,b,c,dp):
 CTM_1 = uni10.Network("Network1/LeftCorrd.net")
 CTM_1.putTensor('vec_left',vec_left)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',dp)
 vec=CTM_1.launch()
 vec.permute([10,23, 16, -16 , 9,-9, 2,-10],0)
 vec=max_ten(vec)
 return vec


def Make_first_vecright_d(vec_right, Ta1, Ta3,Tb1,Tb3,a,b,c,dp):
 CTM_1 = uni10.Network("Network1/RightCorrd.net")
 CTM_1.putTensor('vec_right',vec_right)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',dp)
 vec=CTM_1.launch()
 vec.permute([10,21,14,-14 , 7,-7,0,-10],6)
 vec=max_ten(vec)
 return vec

def  Make_midle_vecleft(vec_left, Ta1, Ta3,Tb1,Tb3,ap,b,c,d):

 CTM_1 = uni10.Network("Network1/LeftCorr1.net")
 CTM_1.putTensor('vec_left',vec_left)
 CTM_1.putTensor('Ta1',Ta1)
 CTM_1.putTensor('Ta3',Ta3)
 CTM_1.putTensor('Tb1',Tb1)
 CTM_1.putTensor('Tb3',Tb3)
 CTM_1.putTensor('a',ap)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 vec.permute([10,23, 16, -16 , 9,-9, 2,-10],0)
 vec=max_ten(vec)
 return vec



#############################################################################

def make_down(c4,Ta3, Tb3,c3):
 Tb3.setLabel([-5,3,-3,4])
 Ta3.setLabel([1,2,-2,-5])
 c3.setLabel([4,6])
 c4.setLabel([5,1])
 vec_down=(Tb3*Ta3)*(c3*c4)
 vec_down.permute([5, 2, -2 , 3, -3 , 6],0)
 return vec_down

def  make_up(c1,Tb1, Ta1,c2):
 Ta1.setLabel([-5,3,-3,4])
 Tb1.setLabel([1,2,-2,-5])
 c2.setLabel([4,6])
 c1.setLabel([5,1])
 vec_up=(Ta1*Tb1)*(c1*c2)
 vec_up.permute([5,2,-2,3,-3,6],6)
 return vec_up




def  Make_first_vecdown_a(vec_down, ap, b, c, d, Tb2, Ta2, Ta4, Tb4):

 CTM_1 = uni10.Network("Network1/DownCorra.net")
 CTM_1.putTensor('vec_down',vec_down)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',ap)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 #vec.permute([10,3, 4, -4 , 5, -5 , 6,-10],8)
 return vec




def Make_first_vecup_a(vec_up, ap, b, c, d, Tb2, Ta2, Ta4, Tb4):

 CTM_1 = uni10.Network("Network1/UpCorra.net")
 CTM_1.putTensor('vec_up',vec_up)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',ap)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 return vec


def  Make_midle_vecdown(vec_down, a,b,c,d,Tb2, Ta2, Ta4, Tb4):

 CTM_1 = uni10.Network("Network1/DownCorr1.net")
 CTM_1.putTensor('vec_down',vec_down)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 vec=max_ten(vec)
 return vec



def  Make_first_vecdown_b(vec_down, a, bp, c, d, Tb2, Ta2, Ta4, Tb4):

 CTM_1 = uni10.Network("Network1/DownCorrb.net")
 CTM_1.putTensor('vec_down',vec_down)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',bp)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 #vec.permute([10,3, 4, -4 , 5, -5 , 6,-10],8)
 return vec




def Make_first_vecup_b(vec_up, a, bp, c, d, Tb2, Ta2, Ta4, Tb4):

 CTM_1 = uni10.Network("Network1/UpCorrb.net")
 CTM_1.putTensor('vec_up',vec_up)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',bp)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 return vec



def  Make_first_vecdown_c(vec_down, a, b, cp, d, Tb2, Ta2, Ta4, Tb4):

 CTM_1 = uni10.Network("Network1/DownCorrc.net")
 CTM_1.putTensor('vec_down',vec_down)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',cp)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 #print CTM_1
 #vec.permute([10,3, 4, -4 , 5, -5 , 6,-10],8)
 return vec

def Make_first_vecup_c(vec_up, a, b, cp, d, Tb2, Ta2, Ta4, Tb4):

 CTM_1 = uni10.Network("Network1/UpCorrc.net")
 CTM_1.putTensor('vec_up',vec_up)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',cp)
 CTM_1.putTensor('d',d)
 vec=CTM_1.launch()
 return vec




def  Make_first_vecdown_d(vec_down, a, b, c, dp, Tb2, Ta2, Ta4, Tb4):

 CTM_1 = uni10.Network("Network1/DownCorrd.net")
 CTM_1.putTensor('vec_down',vec_down)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',dp)
 vec=CTM_1.launch()
 #print CTM_1
 #vec.permute([10,3, 4, -4 , 5, -5 , 6,-10],8)
 return vec

def Make_first_vecup_d(vec_up, a, b, c, dp, Tb2, Ta2, Ta4, Tb4):

 CTM_1 = uni10.Network("Network1/UpCorrd.net")
 CTM_1.putTensor('vec_up',vec_up)
 CTM_1.putTensor('Ta2',Ta2)
 CTM_1.putTensor('Ta4',Ta4)
 CTM_1.putTensor('Tb2',Tb2)
 CTM_1.putTensor('Tb4',Tb4)
 CTM_1.putTensor('a',a)
 CTM_1.putTensor('b',b)
 CTM_1.putTensor('c',c)
 CTM_1.putTensor('d',dp)
 vec=CTM_1.launch()
 return vec















#################################################################################################



















