import pyUni10 as uni10
#import sys
#import numpy as np
#import matplotlib.pyplot as plt
#import matplotlib
#import pylab
#import random
import copy
#import line_profiler
import TruncateU

def norm_comp(complex_mat):
 D=int(complex_mat.row())
 D1=int(complex_mat.col())
 list_z=[]
 for i in xrange(D):
  for j in xrange(D1):
    list_z.append(abs(complex_mat[i*D1+j]))
 return max(list_z)

###@profile
def MaxAbs(c):
 blk_qnums = c.blockQnum()
 max_list=[]
 for qnum in blk_qnums:
    c_mat=c.getBlock(qnum)
    max_list.append(norm_comp(c_mat))
 #sv_mat = uni10.Matrix( len(max_list), len(max_list), max_list, True)
 #return sv_mat.absMax()
 max_list_f=[abs(x) for x in max_list]
 #print max_list_f, max(max_list_f)
 return max(max_list_f)

###@profile
def norm_comp1(complex_mat):
 D=int(complex_mat.row())
 list_z=[]
 for i in xrange(D):
    list_z.append(abs(complex_mat[i]))
 return max(list_z)

###@profile
def MaxAbs1(c):
 blk_qnums = c.blockQnum()
 max_list=[]
 for qnum in blk_qnums:
    c_mat=c.getBlock(qnum)
    max_list.append(norm_comp1(c_mat))
 #sv_mat = uni10.Matrix( len(max_list), len(max_list), max_list, True)
 #return sv_mat.absMax()
 max_list_f=[abs(x) for x in max_list]
 #print max_list_f, max(max_list_f)
 return max(max_list_f)

###@profile
def norm_CTM(c):
 Max_val=abs(MaxAbs(c))
 if (( Max_val < 0.50e-1) or (Max_val > 0.50e+1)) and abs(Max_val)>1.0e-12:
  #print  "Max_val", Max_val
  c*=(1.00/Max_val)
 return c

#@profile 
def produce_projectives(theta,theta1,chi_dim):
 theta=copy.copy(theta)
 theta1=copy.copy(theta1)
 
 theta.setLabel([1,2,20,3,4,40])
 theta.permute([1,2,20,3,4,40],3) 

 
 U, s, V=TruncateU.svd_parity1(theta)
 
 if MaxAbs(s) > 1.0e+7 or MaxAbs(s) < 1.0e-1:
  s=s*(1.00/MaxAbs(s))

 U.setLabel([1,2,20,-1,-2,-3])
 s.setLabel([-1,-2,-3,3,4,5])
 R=U*s
 R.permute([1,2,20,3,4,5],3)


 theta1.setLabel([1,2,20,3,4,40])
 theta1.permute([1,2,20,3,4,40],3) 

 
 U, s, V=TruncateU.svd_parity1(theta1)

 if MaxAbs(s) > 1.0e+7 or MaxAbs(s) < 1.0e-1:
  s=s*(1.00/MaxAbs(s))

 U.setLabel([1,2,20,-1,-2,-3])
 s.setLabel([-1,-2,-3,6,7,8])
 Rb=U*s
 Rb.permute([1,2,20,6,7,8],3)
 Rb.permute([6,7,8,1,2,20],3)
 
 
 A=R*Rb
 A.permute([6,7,8,3,4,5],3)


 V, U, s=TruncateU.setTruncation(A, chi_dim) 

 if MaxAbs(s) > 1.0e+7 or MaxAbs(s) < 1.0e-1:
  s=s*(1.00/MaxAbs(s))


 U.setLabel([-1,3,4,5])
 V.setLabel([6,7,8,-1])

# if MaxAbs(s) > 1.0e-8:
#  s=s*(1.00/MaxAbs(s)) 

 s=TruncateU.inverse(s)
 s=TruncateU.Sqrt(s)
 
 s.setLabel([6,-1])
 U=s*U
 U.permute([6,3,4,5],1)

 s.setLabel([-1,9])
 V=V*s
 V.permute([6,7,8,9],3) 


 R.permute([1,2,20,3,4,5],3)
 U.cTranspose()
 U1x=R*U
 U1x.permute([1,2,20,6],3)

 Rb.permute([6,7,8,1,2,20],3)
 V.cTranspose()
 U1x_trans=Rb*V
 U1x_trans.permute([9,1,2,20],1)

 return U1x, U1x_trans 





def produce_projectives2(theta,theta1,chi_dim):
 theta=copy.copy(theta)
 theta1=copy.copy(theta1)
 
 theta.setLabel([1,2,3,4])
 theta.permute([1,2,3,4],2) 

 print theta.printDiagram()
 U, s, V=TruncateU.svd_parity111(theta)
 
 if MaxAbs(s) > 1.0e+7 or MaxAbs(s) < 1.0e-1:
  s=s*(1.00/MaxAbs(s))

 U.setLabel([1,2,-1,-2])
 s.setLabel([-1,-2,3,4])
 R=U*s
 R.permute([1,2,3,4],2)


 theta1.setLabel([1,2,3,4])
 theta1.permute([1,2,3,4],2) 

 print theta1.printDiagram() 
 U, s, V=TruncateU.svd_parity111(theta1)

 if MaxAbs(s) > 1.0e+7 or MaxAbs(s) < 1.0e-1:
  s=s*(1.00/MaxAbs(s))

 U.setLabel([1,2,-1,-2])
 s.setLabel([-1,-2,6,7])
 Rb=U*s
 Rb.permute([1,2,6,7],2)
 Rb.permute([6,7,1,2],2)
 
 
 A=R*Rb
 A.permute([6,7,3,4],2)

 print A.printDiagram() 
 V, U, s=TruncateU.setTruncation3(A, chi_dim) 

 if MaxAbs(s) > 1.0e+7 or MaxAbs(s) < 1.0e-1:
  s=s*(1.00/MaxAbs(s))


 U.setLabel([-1,3,4])
 V.setLabel([6,7,-1])

# if MaxAbs(s) > 1.0e-8:
#  s=s*(1.00/MaxAbs(s)) 

 s=TruncateU.inverse(s)
 s=TruncateU.Sqrt(s)
 
 s.setLabel([6,-1])
 U=s*U
 U.permute([6,3,4],1)

 s.setLabel([-1,9])
 V=V*s
 V.permute([6,7,9],2) 


 R.permute([1,2,3,4],2)
 U.transpose()
 U1x=R*U
 U1x.permute([1,2,6],2)

 Rb.permute([6,7,1,2],2)
 V.transpose()
 U1x_trans=Rb*V
 U1x_trans.permute([9,1,2],1)

 return U1x, U1x_trans 


##@profile
def produce_U_up(a,b,c,d,Env,chi_dim,D):
#############################Env#####################################
# t0=time.time()
 Env[0].setLabel([1,2])
 Env[1].setLabel([2,3,-3,4])
 Env[2].setLabel([4,5,-5,6])
 Env[3].setLabel([6,7])
 Env[4].setLabel([7,9,-9,8])
 Env[11].setLabel([14,13,-13,1])
 a.setLabel([13,-13,12,-12,11,-11,3,-3])
 b.setLabel([11,-11,10,-10,9,-9,5,-5])


 theta=(((Env[0]*Env[1])*(Env[11]))*a)*(((Env[2]*Env[3])*(Env[4]))*b)
 theta.permute([14, 12,-12, 8 , 10,-10],3)

 Env[10].setLabel([1,13,-13,14])
 Env[9].setLabel([2,1])
 Env[8].setLabel([4,3,-3,2])
 Env[7].setLabel([6,5,-5,4])
 Env[6].setLabel([7,6])
 Env[5].setLabel([8,9,-9,7])
 c.setLabel([13,-13,3,-3,11,-11,12,-12])
 d.setLabel([11,-11,5,-5,9,-9,10,-10])


 theta1=(((Env[5]*Env[6])*(Env[7]))*d)*(((Env[9]*Env[8])*(Env[10]))*c)
 theta1.permute([14, 12,-12, 8 , 10,-10],3)
 U1x, U1x_trans=produce_projectives( theta, theta1, chi_dim)

 theta.permute([ 8 , 10, -10, 14, 12,-12], 3)
 theta1.permute([ 8 , 10, -10, 14, 12,-12], 3)

 U2x, U2x_trans=produce_projectives(theta,theta1, chi_dim)

 return  U1x, U1x_trans, U2x, U2x_trans


##@profile
def update_env(a,Env_list,U1x,U1x_trans,U2x,U2x_trans,i_x,j_y, N_x, N_y):
###################################1###############################################
 Env_list[i_x][j_y][0].setLabel([0,1])
 Env_list[i_x][j_y][1].setLabel([1,2,-2,3])
 U1x_trans[(j_y+1)%N_y].setLabel([4,0,2,-2]) 
 Env_list[(i_x+1)%N_x][j_y][0]=(Env_list[i_x][j_y][0]*Env_list[i_x][j_y][1])*U1x_trans[(j_y+1)%N_y]
 Env_list[(i_x+1)%N_x][j_y][0].permute([4,3],1)


 Env_list[i_x][j_y][9].setLabel([0,1])
 Env_list[i_x][j_y][8].setLabel([2,3,-3,0])
 U1x[(j_y-1)%N_y].setLabel([1,3,-3,4]) 
 Env_list[(i_x+1)%N_x][j_y][9]=(Env_list[i_x][j_y][8]*Env_list[i_x][j_y][9])*U1x[(j_y-1)%N_y]
 Env_list[(i_x+1)%N_x][j_y][9].permute([2,4],1)


 Env_list[i_x][j_y][10].setLabel([0,1,-1,2])
 a[i_x][j_y].setLabel([1,-1,3,-3,4,-4,5,-5])
 U1x_trans[(j_y-1)%N_y].setLabel([8,0,3,-3]) 
 U1x[j_y].setLabel([2,5,-5,7])
 Env_list[(i_x+1)%N_x][j_y][10]=((Env_list[i_x][j_y][10]*U1x_trans[(j_y-1)%N_y])*a[i_x][j_y])*U1x[j_y]
 Env_list[(i_x+1)%N_x][j_y][10].permute([8,4,-4,7],3)


 Env_list[i_x][j_y][11].setLabel([0,1,-1,2])
 U1x[(j_y+1)%N_y].setLabel([2,7,-7,4]) 
 U1x_trans[j_y].setLabel([5,0,3,-3])
 a[i_x][(j_y+1)%N_y].setLabel([1,-1,3,-3,6,-6,7,-7])
 Env_list[(i_x+1)%N_x][j_y][11]=(((Env_list[i_x][j_y][11]*U1x_trans[j_y]))*a[i_x][(j_y+1)%N_y])*U1x[(j_y+1)%N_y]
 Env_list[(i_x+1)%N_x][j_y][11].permute([5,6,-6,4],3)

##########################################################################
 Env_list[i_x][j_y][3].setLabel([0,1])
 Env_list[i_x][j_y][2].setLabel([3,2,-2,0])
 U2x_trans[(j_y+1)%N_y].setLabel([4,1,2,-2])
 Env_list[(i_x-1)%N_x][j_y][3]=(Env_list[i_x][j_y][3]*Env_list[i_x][j_y][2])*U2x_trans[(j_y+1)%N_y]
 Env_list[(i_x-1)%N_x][j_y][3].permute([3,4],1)


 Env_list[i_x][j_y][6].setLabel([1,3])
 Env_list[i_x][j_y][7].setLabel([3,2,-2,0])
 U2x[(j_y-1)%N_y].setLabel([1,2,-2,4])
 Env_list[(i_x-1)%N_x][j_y][6]=(Env_list[i_x][j_y][6]*Env_list[i_x][j_y][7])*U2x[(j_y-1)%N_y]
 Env_list[(i_x-1)%N_x][j_y][6].permute([4,0],1)


 Env_list[i_x][j_y][4].setLabel([0,1,-1,2])
 a[(i_x+1)%N_x][(j_y+1)%N_y].setLabel([5,-5,3,-3,1,-1,4,-4])
 U2x_trans[j_y].setLabel([8,2,3,-3])
 U2x[(j_y+1)%N_y].setLabel([0,4,-4,7])
 Env_list[(i_x-1)%N_x][j_y][4]=((Env_list[i_x][j_y][4]*U2x_trans[j_y])*a[(i_x+1)%N_x][(j_y+1)%N_y])*U2x[(j_y+1)%N_y]
 Env_list[(i_x-1)%N_x][j_y][4].permute([7,5,-5,8],3)


 Env_list[i_x][j_y][5].setLabel([0,1,-1,2])
 U2x[j_y].setLabel([0,4,-4,5])
 U2x_trans[(j_y-1)%N_y].setLabel([7,2,3,-3])
 a[(i_x+1)%N_x][j_y].setLabel([6,-6,3,-3,1,-1,4,-4])
 Env_list[(i_x-1)%N_x][j_y][5]=((Env_list[i_x][j_y][5]*U2x_trans[(j_y-1)%N_y])*a[(i_x+1)%N_x][j_y])*U2x[j_y]
 Env_list[(i_x-1)%N_x][j_y][5].permute([5,6,-6,7],3)
########################################################################
 return    Env_list


#@profile 
def  Do_1st_column(a_u,a,Env_list,chi_dim,D,i_x,N_x,N_y):

 U1x=[None]*N_y
 U1x_trans=[None]*N_y

 U2x=[None]*N_y
 U2x_trans=[None]*N_y

 for j_y in xrange(N_y):
  #print "j_y", i_x, j_y
  U1x[j_y], U1x_trans[j_y], U2x[j_y], U2x_trans[j_y]=produce_U_up(a[(i_x+1)%N_x][j_y],a[(i_x+1)%N_x][(j_y+1)%N_y],a[i_x][j_y],a[i_x][(j_y+1)%N_y],Env_list[i_x][j_y],chi_dim,D)

 for j_y in xrange(N_y):
  #print "j_y, update", i_x, j_y
  Env_list=update_env(a,Env_list,U1x,U1x_trans,U2x,U2x_trans,i_x,j_y,N_x, N_y)

 return Env_list 



#@profile 
def  add_left1(a_u,a,Env_list,chi,D,N_x,N_y):

 chi_dim=0
 for i in xrange(len(chi)):
  chi_dim+=chi[i]


 for i_x in xrange(N_x):
   Env_list=Do_1st_column(a_u,a,Env_list,chi_dim,D,i_x,N_x,N_y)


 for i_x in xrange(N_x):
  for j_y in xrange(N_y):
   Env_list[i_x][j_y]=normalize_Env(Env_list[i_x][j_y])



 return Env_list
 
 


 
def equall_norm(a,b,c,d,Env):

 Norm=magnetization_value(a,b,c,d,Env)
 if abs(Norm) < 0: Env[0]=-1.0*Env[0];

 while abs(Norm) <1.0e-1: 
  Env=checking_norm(a,b,c,d,Env)
  Norm=magnetization_value(a,b,c,d,Env)

 while abs(Norm)>1.0e+5: 
  Env=checking_norm(a,b,c,d,Env)
  Norm=magnetization_value(a,b,c,d,Env)

 return Env


def checking_norm(a,b,c,d,Env):
 Norm=magnetization_value(a,b,c,d,Env)
 if abs(Norm) < 0: Env[0]=-1.0*Env[0];
 if abs(Norm) < 1.0e-1:
  Env[0]*=1.4
  Env[1]*=1.4
  Env[2]*=1.4
  Env[3]*=1.4
  Env[4]*=1.4
  Env[5]*=1.4
  Env[6]*=1.4
  Env[7]*=1.4
  Env[8]*=1.4
  Env[9]*=1.4
  Env[10]*=1.4
  Env[11]*=1.4

 elif abs(Norm) > 1.e+5:
  Env[0]*=0.92
  Env[1]*=0.92
  Env[2]*=0.92
  Env[3]*=0.92
  Env[4]*=0.92
  Env[5]*=0.92
  Env[6]*=0.92
  Env[7]*=0.92
  Env[8]*=0.92
  Env[9]*=0.92
  Env[10]*=0.92
  Env[11]*=0.92
 return Env
 

###@profile 

def  magnetization_value(a,b,c,d,Env):

 #norm=norm(a,b,c,d,Env)
 #print  
 CTM_net = uni10.Network("Network/CTM.net")
 CTM_net.putTensor('Env[0]',Env[0])
 CTM_net.putTensor('Env[1]',Env[1])
 CTM_net.putTensor('Env[2]',Env[2])
 CTM_net.putTensor('Env[3]',Env[3])
 CTM_net.putTensor('Env[4]',Env[4])
 CTM_net.putTensor('Env[5]',Env[5])
 CTM_net.putTensor('Env[6]',Env[6])
 CTM_net.putTensor('Env[7]',Env[7])
 CTM_net.putTensor('Env[8]',Env[8])
 CTM_net.putTensor('Env[9]',Env[9])
 CTM_net.putTensor('Env[10]',Env[10])
 CTM_net.putTensor('Env[11]',Env[11])
 CTM_net.putTensor('a',a)
 CTM_net.putTensor('b',b)
 CTM_net.putTensor('c',c)
 CTM_net.putTensor('d',d)
 norm=CTM_net.launch()

 return norm[0]

 

def  Env_energy_h(c1,c2,c3,c4,Ta1,Ta2,Ta3,Ta4,Tb1,Tb2,Tb3,Tb4,a,b,c,d,a_u,b_u,H0,D,d_phys):

 E1, E2, E3, E4, E5, E6, E7, E8=basicB.produce_Env(a,b,c,d,c1,c2,c3,c4,Ta1,Tb1,Ta2,Tb2,Ta3,Tb3,Ta4, Tb4,D,d_phys)
 
 E_ab=basicB.Energy_ab(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, H0, a_u, b_u)

 return E_ab

def  Env_energy_v(c1,c2,c3,c4,Ta1,Ta2,Ta3,Ta4,Tb1,Tb2,Tb3,Tb4,a,b,c,d,c_u,a_u,H0,D,d_phys):

 E1, E2, E3, E4, E5, E6, E7, E8=basicB.produce_Env(a,b,c,d,c1, c2,c3,c4,Ta1, Tb1,Ta2, Tb2,Ta3, Tb3,Ta4, Tb4,D,d_phys)

 E_ca=basicB.Energy_ca(E1, E2, E3, E4, E5, E6, E7, E8, a, b, c,d, H0,c_u,a_u)

 return E_ca



def permuteN(a_u,a,Env_list,N_x,N_y):

 a_t=[None]*N_y
 for i in xrange(N_y):
  a_t[i]=[None]*N_x

 for i in xrange(N_x):
  for j in xrange(N_y):
   ten=a[i][j]*1.0
   ten.setLabel([10,-10,1,-1,2,-2,3,-3])
   ten.permute([1,-1,10,-10,3,-3,2,-2],4)
   a_t[j][i]=ten*1.0

 Env_t=[None]*N_y
 for i in xrange(N_y):
  Env_t[i]=[None]*N_x

 for i_x in xrange(N_x):
  for j_y in xrange(N_y):
   Env_t[j_y][i_x]=rewrite_reorder(Env_list[i_x][j_y])

 return a_t, Env_t


def rewrite_reorder(Env):

 Env[0].setLabel([0,1])
 Env[0].permute([1,0],1)
 Env[0].setLabel([0,1])

 Env[3].setLabel([0,1])
 Env[3].permute([1,0],1)
 Env[3].setLabel([0,1])
 
 Env[6].setLabel([0,1])
 Env[6].permute([1,0],1)
 Env[6].setLabel([0,1])

 Env[9].setLabel([0,1])
 Env[9].permute([1,0],1)
 Env[9].setLabel([0,1])

 Env[1].setLabel([0,1,-1,2])
 Env[1].permute([2,1,-1,0],3)
 Env[1].setLabel([0,1,-1,2])
 
 Env[2].setLabel([0,1,-1,2])
 Env[2].permute([2,1,-1,0],3)
 Env[2].setLabel([0,1,-1,2])
 
 Env[4].setLabel([0,1,-1,2])
 Env[4].permute([2,1,-1,0],3)
 Env[4].setLabel([0,1,-1,2])

 Env[5].setLabel([0,1,-1,2])
 Env[5].permute([2,1,-1,0],3)
 Env[5].setLabel([0,1,-1,2])
 
 Env[7].setLabel([0,1,-1,2])
 Env[7].permute([2,1,-1,0],3)
 Env[7].setLabel([0,1,-1,2])

 Env[8].setLabel([0,1,-1,2])
 Env[8].permute([2,1,-1,0],3)
 Env[8].setLabel([0,1,-1,2])

 Env[10].setLabel([0,1,-1,2])
 Env[10].permute([2,1,-1,0],3)
 Env[10].setLabel([0,1,-1,2])

 Env[11].setLabel([0,1,-1,2])
 Env[11].permute([2,1,-1,0],3)
 Env[11].setLabel([0,1,-1,2])

 Env1=[Env[0],Env[11],Env[10],Env[9],Env[8],Env[7],Env[6],Env[5],Env[4],Env[3],Env[2],Env[1]]
 return Env1








 
 
def qr_parity2(theta):

    bd1=uni10.Bond(uni10.BD_IN,theta.bond(2).Qlist())

    GA=uni10.UniTensor([theta.bond(0),theta.bond(1),theta.bond(2)])
    LA=uni10.UniTensor([bd1, theta.bond(2)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).qr()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])

    return GA, LA
 
def lq_parity2(theta):
    bd1=uni10.Bond(uni10.BD_OUT,theta.bond(0).Qlist())

    
    LA=uni10.UniTensor([theta.bond(0),bd1])
    GA=uni10.UniTensor([theta.bond(0),theta.bond(1),theta.bond(2)])
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).lq()
        GA.putBlock(qnum, svds[qnum][1])
        LA.putBlock(qnum, svds[qnum][0])

    return  LA, GA
 
def svd_parity2(theta):

    LA=uni10.UniTensor([theta.bond(0), theta.bond(1)])
    GA=uni10.UniTensor([theta.bond(0), theta.bond(1)])
    GB=uni10.UniTensor([theta.bond(0), theta.bond(1)])
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
    for qnum in blk_qnums:
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0])
        GB.putBlock(qnum, svd[2])
        LA.putBlock(qnum, svd[1])
#    print LA
    return GA, LA,GB
 
 
 


def normalize_Env(Env):
 
 for q in xrange(len(Env)):
  Env[q]=norm_CTM(Env[q])

 return Env





