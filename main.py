import pyUni10 as uni10
import copy
import numpy as np
from numpy import linalg as LA
import MPSclass 
import UniformDisentangler as UD
from scipy.linalg import block_diag

N_x=5
N_y=3
#No-symmetry
D=[2]
chi_boundry=[12]
chi_single=[6]
chi_try=[2]
d_phys=[2]

#Z2-symmetry
#D=[2,2]
#chi_boundry=[8,8]
#chi_single=[20,20]
#chi_try=[20]
#d_phys=[1,1]


#U1-symmetry
#D=[1,2,1,1]
#chi_boundry=[200]
#chi_single=[200]
#chi_try=[20]
#d_phys=[1,1]

interval=+1.0e-2
threshold=+1.0e+1
accuracy=+1.0e-8
Model="Heis"               #ITF,ITF_Z2, Heis, Heis_Z2, Heis_U1, Fer_Z2, Fer_U1#
N_tebd=60
z_coupling=1.0
j_coupling=-2.0
h_coupling=[z_coupling, j_coupling]
start_itebd=1.0
division_itebd=5.0
N_iter_SF=20
Corner_method='CTMFull'   #CTMRG, CTMFull
N_env=[30,1.00e-8]



PEPS_listten=[None]*N_x


q_D, q_chi_boundry, q_chi_single, q_chi_try, q_d=UD.full_make_bond( Model, D, chi_boundry, chi_single, chi_try, d_phys)


#print q_D#, q_chi_boundry, q_chi_single, q_chi_try, q_d

for i in xrange(N_x):
 PEPS_listten[i]=UD.Init_PEPS( N_y, q_D, q_d, i)


Landa_col=UD.Landa_f_col( q_D, N_x, N_y)
Landa_row=UD.Landa_f_row( q_D, N_x, N_y)


UD.Reload_Landa_row(Landa_row, N_x,N_y)
UD.Reload_Landa_col(Landa_col, N_x,N_y)
UD.Reload_Gamma(PEPS_listten, N_x,N_y)
#UD.Reload_f(PEPS_listten, N_x)

#PEPS_listten=UD.make_PEPS_tensors(PEPS_listten, Landa_row, Landa_col,N_x)

#PEPS_listten, norm_val, count_val=UD.Normalize_PEPS( PEPS_listten, N_x, q_D, q_chi_try, q_d, threshold, interval)

#Landa_col_inv,Landa_row_inv=UD.inv_landa_col_row( Landa_col, Landa_row, N_x)
#PEPS_listten=UD.make_PEPS_tensors(PEPS_listten, Landa_row_inv, Landa_col_inv,N_x)

#PEPS_listten, Landa_col, Landa_row=UD.simple_update( PEPS_listten, Landa_col, Landa_row, start_itebd, division_itebd, N_iter_SF, h_coupling, q_d, Model, N_x, N_y, q_D,q_chi_try, threshold, interval)

UD.Store_Gamma( PEPS_listten, N_x,N_y)
UD.Store_Landa_row( Landa_row, N_x,N_y)
UD.Store_Landa_col( Landa_col, N_x,N_y)

# for i in xrange(N_x):
#  for j in xrange(N_y):
#   print i, j , Landa_row[i][j]
# 
# for i in xrange(N_x):
#  for j in xrange(N_y):
#   print i, j , Landa_col[i][j]

PEPS_listten=UD.make_PEPS_tensors(PEPS_listten, Landa_row, Landa_col,N_x,N_y)

#UD.Reload_f(PEPS_listten, N_x,N_y)
#UD.Store_f(PEPS_listten, N_x,N_y)

Env_list=UD.Env_f_list(q_chi_boundry,q_D, N_x, N_y)
PEPS_listtenU=UD.Make_Double_Ten(PEPS_listten, N_x, N_y, q_D, q_d)

E_value=UD.E_total(PEPS_listten,PEPS_listtenU,Env_list,D,h_coupling,q_d,chi_boundry,Corner_method,Model,N_env,N_x,N_y)
print 'E_toal=', E_value

Mag_f_list=[]
E_f_list=[]
h_list=[]
E_iter_list=[]
E_iter_list1=[]
count_list=[]
Cond_list=[]
Norm_list_boundry=[]


file = open("EnergyIter1.txt", "w")
for index in range(len(E_iter_list1)):
  file.write(str(index) + " " + str(E_iter_list1[index])+" "+ "\n")
file.close()


#for i in xrange(N_x):
# for j in xrange(N_x+1):
#   print "col", i,j, Landa_col[i][j]

#for i in xrange(N_x+1):
#  for j in xrange(N_x):
#   print "row", i,j, Landa_row[i][j]


#for i in xrange(N_x):
# for j in xrange(N_x):
#  print i, j , PEPS_listten[i][j].printDiagram()
