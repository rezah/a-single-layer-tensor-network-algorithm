import pyUni10 as uni10
import cmath
import numpy as np
#from scipy.linalg import expm
from numpy import linalg as LA
#import matplotlib.pyplot as plt
#import matplotlib
#import pylab
#import random
import copy
import time
import basic
#import basicA
import basicB
import basicC
def expm_f(delta,Temporary):

 d, Y = np.linalg.eig(Temporary)
 Yinv = np.linalg.pinv(Y)
 D = np.diag(np.exp((-delta)*d))
 
 Y = np.asmatrix(Y)
 D = np.asmatrix(D)
 Yinv = np.asmatrix(Yinv)
 B = Y*D*Yinv
 #print B
 return B

def Full_Update(a_u,b_u,c_u,d_u,chi,d_phys,D,h,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,Gauge,Corner_method,N_iterF,Acc_E,Model,N_grad, Opt_method,Inv_method,N_svd,N_env,method,check_step,iteration_per_step):

 H0, H00, H1, H2=basic.choose_model(Model, h, d_phys)
 U = uni10.UniTensor(uni10.CTYPE,H0.bond(), "U");
 U0 = uni10.UniTensor(uni10.CTYPE,H00.bond(), "U0");
 U1 = uni10.UniTensor(uni10.CTYPE,H1.bond(), "U1");
 U2 = uni10.UniTensor(uni10.CTYPE,H2.bond(), "U2");
 

 E_0=1.0
 E_1=2.0
 E_min=1.e+14
 Env_1,Env1_1,Env2_1,Env3_1,Env4_1,Env5_1,Env6_1,Env7_1,Env8_1,Env9_1,Env10_1,Env11_1,Env12_1,Env13_1,Env14_1,Env15_1=make_env(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15)

 Env_2,Env1_2,Env2_2,Env3_2,Env4_2,Env5_2,Env6_2,Env7_2,Env8_2,Env9_2,Env10_2,Env11_2,Env12_2,Env13_2,Env14_2,Env15_2=make_env(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15)

 Env_3,Env1_3,Env2_3,Env3_3,Env4_3,Env5_3,Env6_3,Env7_3,Env8_3,Env9_3,Env10_3,Env11_3,Env12_3,Env13_3,Env14_3,Env15_3=make_env(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15)


#  MPO_list=basic.Decomposition(H2)
#  plist=basicC.initialize_plist(a_u, b_u, c_u, MPO_list)
#  #basicC.Reload_plist(plist)
#  plist1=basicC.initialize_plist(b_u, a_u, d_u, MPO_list)
#  plist2=basicC.initialize_plist(c_u, d_u, a_u, MPO_list)
#  plist3=basicC.initialize_plist(d_u, c_u, b_u, MPO_list)
# 
#  plist00=basicC.initialize_plist1(a_u, b_u, d_u, MPO_list)
#  plist11=basicC.initialize_plist1(b_u, a_u, c_u, MPO_list)
#  plist22=basicC.initialize_plist1(c_u, d_u, b_u, MPO_list)
#  plist33=basicC.initialize_plist1(d_u, c_u, a_u, MPO_list)
 
 List_delN=basic.Short_TrotterSteps(N_iterF)
 #List_delN=basic.Short_TrotterSteps1(N_iterF)
 #List_delN=basic.Long_TrotterSteps(N_iterF)
 #List_delN=basic.Long_TrotterSteps1(N_iterF)
 print List_delN
 for delta, N_iter in List_delN:
  print delta, N_iter

  blk_qnums = H0.blockQnum()
  for qnum in blk_qnums:
        Temporary=Mat_uni_to_np(H0.getBlock(qnum))
        #delta=-0.5j
        #R_poly=expm(-delta*Temporary)
        R_poly=expm_f(delta,Temporary)
        U.putBlock(qnum, Mat_np_to_Uni(R_poly))

  blk_qnums = H00.blockQnum()
  for qnum in blk_qnums:
        Temporary=Mat_uni_to_np(H00.getBlock(qnum))
        #delta=-0.5j
        #R_poly=expm(-delta*Temporary)
        R_poly=expm_f(delta,Temporary)
        U0.putBlock(qnum, Mat_np_to_Uni(R_poly))


  blk_qnums = H2.blockQnum()
  for qnum in blk_qnums:
        Temporary=Mat_uni_to_np(H2.getBlock(qnum))
        #delta=-0.5j
        #R_poly=expm(-delta*Temporary)
        R_poly=expm_f(delta,Temporary)
        U2.putBlock(qnum, Mat_np_to_Uni(R_poly))

#  MPO_list=basic.Decomposition(U2)

  blk_qnums = H1.blockQnum()
  for qnum in blk_qnums:
        Temporary=Mat_uni_to_np(H1.getBlock(qnum))
        #delta=-0.5j
        #R_poly=expm(-delta*Temporary)
        R_poly=expm_f(delta,Temporary)
        U1.putBlock(qnum, Mat_np_to_Uni(R_poly))


  for q in xrange(N_iter):
   t0=time.time()

#################################################################################################

   Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,H0,d_phys,N_env)

   a_u, b_u=basicB.Var_ab(a_u,b_u,Env,Env4,Env8,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

   #Remove
   #Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,H0,d_phys,N_env)
   Env_1,Env1_1,Env2_1,Env3_1,Env4_1,Env5_1,Env6_1,Env7_1,Env8_1,Env9_1,Env10_1,Env11_1,Env12_1,Env13_1,Env14_1,Env15_1=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env_1,Env1_1,Env2_1,Env3_1,Env4_1,Env5_1,Env6_1,Env7_1,Env8_1,Env9_1,Env10_1,Env11_1,Env12_1,Env13_1,Env14_1,Env15_1,D,H0,d_phys,N_env)

   c_u, d_u=basicB.Var_ab(c_u,d_u,Env2_1,Env6_1,Env10_1,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

   #Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,H0,d_phys,N_env)
   Env_2,Env1_2,Env2_2,Env3_2,Env4_2,Env5_2,Env6_2,Env7_2,Env8_2,Env9_2,Env10_2,Env11_2,Env12_2,Env13_2,Env14_2,Env15_2=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env_2,Env1_2,Env2_2,Env3_2,Env4_2,Env5_2,Env6_2,Env7_2,Env8_2,Env9_2,Env10_2,Env11_2,Env12_2,Env13_2,Env14_2,Env15_2,D,H0,d_phys,N_env)

   b_u, a_u=basicB.Var_ab(b_u,a_u,Env8_2,Env12_2,Env_2,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)



   #Remove
   #Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,H0,d_phys,N_env)
   Env_3,Env1_3,Env2_3,Env3_3,Env4_3,Env5_3,Env6_3,Env7_3,Env8_3,Env9_3,Env10_3,Env11_3,Env12_3,Env13_3,Env14_3,Env15_3=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env_3,Env1_3,Env2_3,Env3_3,Env4_3,Env5_3,Env6_3,Env7_3,Env8_3,Env9_3,Env10_3,Env11_3,Env12_3,Env13_3,Env14_3,Env15_3,D,H0,d_phys,N_env)

   d_u, c_u=basicB.Var_ab(d_u,c_u,Env10_3,Env14_3,Env2_3,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

   Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,H0,d_phys,N_env)

##################################################################

   c_u, a_u=basicB.Var_ca(c_u,a_u,Env2,Env1,Env,D,U0,d_phys,chi,Gauge,Corner_method,H00,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)
#Remove
   #Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,H0,d_phys,N_env)
   Env_1,Env1_1,Env2_1,Env3_1,Env4_1,Env5_1,Env6_1,Env7_1,Env8_1,Env9_1,Env10_1,Env11_1,Env12_1,Env13_1,Env14_1,Env15_1=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env_1,Env1_1,Env2_1,Env3_1,Env4_1,Env5_1,Env6_1,Env7_1,Env8_1,Env9_1,Env10_1,Env11_1,Env12_1,Env13_1,Env14_1,Env15_1,D,H0,d_phys,N_env)

   d_u, b_u=basicB.Var_ca(d_u,b_u,Env10_1,Env9_1,Env8_1,D,U0,d_phys,chi,Gauge,Corner_method,H00,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

   #Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,H0,d_phys,N_env)
   Env_2,Env1_2,Env2_2,Env3_2,Env4_2,Env5_2,Env6_2,Env7_2,Env8_2,Env9_2,Env10_2,Env11_2,Env12_2,Env13_2,Env14_2,Env15_2=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env_2,Env1_2,Env2_2,Env3_2,Env4_2,Env5_2,Env6_2,Env7_2,Env8_2,Env9_2,Env10_2,Env11_2,Env12_2,Env13_2,Env14_2,Env15_2,D,H0,d_phys,N_env)

   a_u, c_u=basicB.Var_ca(a_u,c_u,Env_2,Env3_2,Env2_2,D,U0,d_phys,chi,Gauge,Corner_method,H00,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

#Remove
   #Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,H0,d_phys,N_env)
   Env_3,Env1_3,Env2_3,Env3_3,Env4_3,Env5_3,Env6_3,Env7_3,Env8_3,Env9_3,Env10_3,Env11_3,Env12_3,Env13_3,Env14_3,Env15_3=basic.corner_transfer_matrix_twosite_CTMFull(a_u,b_u,c_u,d_u,chi, Env_3,Env1_3,Env2_3,Env3_3,Env4_3,Env5_3,Env6_3,Env7_3,Env8_3,Env9_3,Env10_3,Env11_3,Env12_3,Env13_3,Env14_3,Env15_3,D,H0,d_phys,N_env)

   b_u, d_u=basicB.Var_ca(b_u,d_u,Env8_3,Env11_3,Env10_3,D,U0,d_phys,chi,Gauge,Corner_method,H00,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)
#####################################################################################
#####################################################################################
   E_value,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15=basic.E_total_conv(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,h,d_phys,chi,Corner_method,Model,N_env)
   print 'E_s=', E_value, time.time() - t0
   M_value=basic.M_total(a_u,b_u,c_u,d_u,Env,Env2,Env8,Env10,D,h,d_phys,chi,Corner_method,Model)
   print 'M_s=', M_value
   basic.Translational_sym(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,h,d_phys,chi,Corner_method,Model,N_env)
   chiral_val=basic.chiral(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,d_phys,chi,Corner_method,Model,N_env)
   print 'chiral_val=', chiral_val

   #break
   E_0=E_1
   E_1=E_value
   if E_1 < E_min:
    E_min=E_1
    print 'delta=', delta    
    print "E_m=", E_min
    basic.Store_Full(a_u,b_u,c_u,d_u)
    basic.Store_EnvEnv(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15)
#     basicC.Store_plist(plist)
#     basicC.Store_plist1(plist00)
   elif q > iteration_per_step:
    a_u,b_u,c_u,d_u=basic.Reload_Full()
    basic.Reload_EnvEnv(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15)
#     basicC.Reload_plist(plist)
#     basicC.Reload_plist1(plist00)
    E_value=basic.E_total(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,h,d_phys,chi,Corner_method,Model,N_env)
    print 'delta=', delta
    print "E_min=", E_value
    M_value=basic.M_total(a_u,b_u,c_u,d_u,Env,Env2,Env8,Env10,D,h,d_phys,chi,Corner_method,Model)
    print 'M_s=', M_value
    basic.Translational_sym(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,h,d_phys,chi,Corner_method,Model,N_env)
    chiral_val=basic.chiral(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,d_phys,chi,Corner_method,Model,N_env)
    print 'chiral_val=', chiral_val
    break; 

   print 'E_d=', abs((E_0-E_1) / E_0) , 'Num_iter=', q

   if (( abs((E_0-E_1) / E_0) ) < Acc_E) or ( q is int(N_iter-1)): 
    print 'break', E_0, E_1, abs((E_0-E_1) / E_0)
    a_u,b_u,c_u,d_u=basic.Reload_Full()
    basic.Reload_EnvEnv(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15)
    #basicC.Reload_plist(plist)
    #basicC.Reload_plist1(plist00)
    E_value=basic.E_total(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,h,d_phys,chi,Corner_method,Model,N_env)
    print 'E_t=', E_value    
    M_value=basic.M_total(a_u,b_u,c_u,d_u,Env,Env2,Env8,Env10,D,h,d_phys,chi,Corner_method,Model)
    print 'M_s=', M_value
    basic.Translational_sym(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,h,d_phys,chi,Corner_method,Model,N_env)
    #basic.Store_Full(a_u,b_u,c_u,d_u)
    chiral_val=basic.chiral(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15,D,d_phys,chi,Corner_method,Model,N_env)
    print 'chiral_val=', chiral_val

    E_0=10.0
    E_1=20.0
    break;
 return a_u, b_u, c_u, d_u, Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15



def  make_env(Env,Env1,Env2,Env3,Env4,Env5,Env6,Env7,Env8,Env9,Env10,Env11,Env12,Env13,Env14,Env15):
 Env_1=copy_copy(Env)
 Env1_1=copy_copy(Env1)
 Env2_1=copy_copy(Env2)
 Env3_1=copy_copy(Env3)
 Env4_1=copy_copy(Env4)
 Env5_1=copy_copy(Env5)
 Env6_1=copy_copy(Env6)
 Env7_1=copy_copy(Env7)
 Env8_1=copy_copy(Env8)
 Env9_1=copy_copy(Env9)
 Env10_1=copy_copy(Env10)
 Env11_1=copy_copy(Env11)
 Env12_1=copy_copy(Env12)
 Env13_1=copy_copy(Env13)
 Env14_1=copy_copy(Env14)
 Env15_1=copy_copy(Env15) 
 #print Env[0], Env_1[0]
 return Env_1, Env1_1,Env2_1,Env3_1,Env4_1,Env5_1,Env6_1,Env7_1,Env8_1,Env9_1,Env10_1,Env11_1,Env12_1,Env13_1,Env14_1,Env15_1


def copy_copy(Env):
 Env_1=[ copy.copy(Env[i])  for i in xrange(len(Env)) ]
 for i in xrange(len(Env)):
  Env_1[i].randomize()
 #print Env_1
 return  Env_1


def Full_Update_time(a_u,b_u,c_u,d_u,a,b,c,d,chi,d_phys,D,h,Env,Env1,Env2,Env3,Gauge,Corner_method,N_iterF,Acc_E,Model,N_grad, Opt_method,Inv_method,N_svd,N_env,method,check_step,iteration_per_step):

 M_s_list=[]
 a_u_f=copy.copy(a_u)
 b_u_f=copy.copy(b_u)
 c_u_f=copy.copy(c_u)
 d_u_f=copy.copy(d_u)
 #a_u=basic.update_site_a_az(d_phys,a_u)

 time=0+0j
 #M_value=basic.Corr_time(a_u,b_u,c_u,d_u,a_u_f,b_u_f,c_u_f,d_u_f,Env,Env1,Env2,Env3,D,h,d_phys,chi,Corner_method,Model,N_env)
 #print 'M_s=', M_value*(cmath.exp((-1.0j*(time*(-0.669))))), M_value


 #a_u_f=basic.update_site_a_az(d_phys,a_u_f)

 H0, H00, H1, H2=basic.choose_model(Model, h, d_phys)

 U = uni10.UniTensor(uni10.CTYPE,H0.bond(), "U");
 U0 = uni10.UniTensor(uni10.CTYPE,H00.bond(), "U0");
 U1 = uni10.UniTensor(uni10.CTYPE,H1.bond(), "U1");
 U2 = uni10.UniTensor(uni10.CTYPE,H2.bond(), "U2");
 

 E_0=1.0
 E_1=2.0
 E_min=1.e+14


 MPO_list=basic.Decomposition(H2)
 plist=basicC.initialize_plist(a_u, b_u, c_u, MPO_list)
 #basicC.Reload_plist(plist)
 plist1=basicC.initialize_plist(b_u, a_u, d_u, MPO_list)
 plist2=basicC.initialize_plist(c_u, d_u, a_u, MPO_list)
 plist3=basicC.initialize_plist(d_u, c_u, b_u, MPO_list)

 plist00=basicC.initialize_plist1(a_u, b_u, d_u, MPO_list)
 plist11=basicC.initialize_plist1(b_u, a_u, c_u, MPO_list)
 plist22=basicC.initialize_plist1(c_u, d_u, b_u, MPO_list)
 plist33=basicC.initialize_plist1(d_u, c_u, a_u, MPO_list)
 
 #List_delN=basic.Short_TrotterSteps(N_iterF)
 #List_delN=basic.Short_TrotterSteps1(N_iterF)
 #List_delN=basic.Long_TrotterSteps(N_iterF)
 #List_delN=basic.Long_TrotterSteps1(N_iterF)
 #print List_delN
 time=0
 print "\n","\n" 

 for delta in xrange(1,100):
  time=0+0.005*delta
  print "time",time 

  blk_qnums = H0.blockQnum()
  for qnum in blk_qnums:
        Temporary=Mat_uni_to_np(H0.getBlock(qnum))
        #delta=-0.5j
        R_poly=expm((1.0j)*time*Temporary)
        U.putBlock(qnum, Mat_np_to_Uni(R_poly))

  blk_qnums = H00.blockQnum()
  for qnum in blk_qnums:
        Temporary=Mat_uni_to_np(H00.getBlock(qnum))
        #delta=-0.5j
        R_poly=expm((1.0j)*time*Temporary)
        U0.putBlock(qnum, Mat_np_to_Uni(R_poly))


  blk_qnums = H2.blockQnum()
  for qnum in blk_qnums:
        Temporary=Mat_uni_to_np(H2.getBlock(qnum))
        #delta=-0.5j
        R_poly=expm(-delta*Temporary)
        U2.putBlock(qnum, Mat_np_to_Uni(R_poly))

  MPO_list=basic.Decomposition(U2)

  blk_qnums = H1.blockQnum()
  for qnum in blk_qnums:
        Temporary=Mat_uni_to_np(H1.getBlock(qnum))
        #delta=-0.5j
        R_poly=expm(-delta*Temporary)
        U1.putBlock(qnum, Mat_np_to_Uni(R_poly))


  #print U0, U
  #t0=time.time()

#################################################################################################
#   
  a_u, b_u, a, b=basicB.Var_ab(a_u,b_u,a,b,c,d,Env,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)
   #print time.time() - t0, "Seconds, Left"

  c_u, d_u, c, d=basicB.Var_ab(c_u,d_u,c,d,a,b,Env1,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)
#######

  b_u, a_u, b, a=basicB.Var_ab(b_u,a_u,b,a,d,c,Env2,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

  d_u, c_u, d, c=basicB.Var_ab(d_u,c_u,d,c,b,a,Env3,D,U,d_phys,chi,Gauge,Corner_method,H0,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

#########

  c_u, a_u, c, a=basicB.Var_ca(c_u,a_u,a,b,c,d,Env,D,U0,d_phys,chi,Gauge,Corner_method,H00,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

  d_u, b_u, d, b=basicB.Var_ca(d_u,b_u,b,a,d,c,Env2,D,U0,d_phys,chi,Gauge,Corner_method,H00,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

########
  a_u, c_u, a, c=basicB.Var_ca(a_u,c_u,c,d,a,b,Env1,D,U0,d_phys,chi,Gauge,Corner_method,H00,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)

  b_u, d_u, b, d=basicB.Var_ca(b_u,d_u,d,c,b,a,Env3,D,U0,d_phys,chi,Gauge,Corner_method,H00,N_env,N_svd,method,N_grad,Opt_method,Inv_method,check_step)
####################################################################################################

######################################################################################

#   print "\n"
#   a_u, b_u,c_u,d_u, a,b,c,d=basicC.Var_cab(a_u, b_u,c_u,d_u,a,b,c,d,Env,D,U2,d_phys,chi,Gauge,Corner_method,H2,N_grad, Opt_method,plist,MPO_list,Inv_method,N_svd,N_env,method,check_step)
#   print "\n"
#   b_u, a_u,d_u,c_u, b,a,d,c=basicC.Var_cab(b_u, a_u,d_u,c_u,b,a,d,c,Env1,D,U2,d_phys,chi,Gauge,Corner_method,H2,N_grad, Opt_method,plist1,MPO_list,Inv_method,N_svd,N_env,method,check_step)
#   print "\n"
#   c_u, d_u,a_u,b_u, c,d,a,b=basicC.Var_cab(c_u, d_u,a_u,b_u,c,d,a,b,Env2,D,U2,d_phys,chi,Gauge,Corner_method,H2,N_grad, Opt_method,plist2,MPO_list,Inv_method,N_svd,N_env,method,check_step)
#   print "\n"
#   d_u, c_u,b_u,a_u, d,c,b,a=basicC.Var_cab(d_u, c_u,b_u,a_u,d,c,b,a,Env3,D,U2,d_phys,chi,Gauge,Corner_method,H2,N_grad, Opt_method,plist3,MPO_list,Inv_method,N_svd,N_env,method,check_step)

############################

#   print "\n"
#   a_u, b_u,c_u,d_u, a,b,c,d=basicC.Var_abd(a_u, b_u,c_u,d_u,a,b,c,d,Env,D,U2,d_phys,chi,Gauge,Corner_method,H2,N_grad, Opt_method,plist00,MPO_list,Inv_method,N_svd,N_env,method,check_step)

#   print "\n"
#   b_u, a_u,d_u,c_u, b,a,d,c=basicC.Var_abd(b_u, a_u,d_u,c_u,b,a,d,c,Env1,D,U2,d_phys,chi,Gauge,Corner_method,H2,N_grad, Opt_method,plist11,MPO_list,Inv_method,N_svd,N_env,method,check_step)

#   print "\n"
#   c_u, d_u,a_u,b_u, c,d,a,b=basicC.Var_abd(c_u, d_u,a_u,b_u,c,d,a,b,Env2,D,U2,d_phys,chi,Gauge,Corner_method,H2,N_grad, Opt_method,plist22,MPO_list,Inv_method,N_svd,N_env,method,check_step)

#   print "\n"
#   d_u, c_u,b_u,a_u, d,c,b,a=basicC.Var_abd(d_u, c_u,b_u,a_u,d,c,b,a,Env3,D,U2,d_phys,chi,Gauge,Corner_method,H2,N_grad, Opt_method,plist33,MPO_list,Inv_method,N_svd,N_env,method,check_step)

#############################################################################################
   
  E_value=basic.E_total_conv(a_u,b_u,c_u,d_u,a,b,c,d,Env,Env1,Env2,Env3,D,h,d_phys,chi,Corner_method,Model,N_env)
  print 'E_s=', E_value
  M_value=basic.M_total(a_u,b_u,c_u,d_u,a,b,c,d,Env,Env1,Env2,Env3,D,h,d_phys,chi,Corner_method,Model)
  print 'M_s=', M_value, '\n'
  M_s_list.append(M_value)
  print M_s_list
#basic.Translational_sym(a_u,b_u,c_u,d_u,Env,Env1,Env2,Env3,D,h,d_phys,chi,Corner_method,Model,N_env)











 
def Mat_np_to_Uni(Mat_np):
 d0=np.size(Mat_np,0)
 d1=np.size(Mat_np,1)
 Mat_uni=uni10.CMatrix(d0,d1)
 for i in xrange(d0):
  for j in xrange(d1):
   Mat_uni[i*d1+j]=Mat_np[i,j]
 return  Mat_uni
 
def Mat_uni_to_np(Mat_uni):
 dim0=int(Mat_uni.row())
 dim1=int(Mat_uni.col())
 Mat_np=np.zeros((dim0,dim1),dtype=np.complex_)
 for i in xrange(dim0):
  for j in xrange(dim1):
   Mat_np[i,j]=Mat_uni[i*dim1+j]
 return  Mat_np



